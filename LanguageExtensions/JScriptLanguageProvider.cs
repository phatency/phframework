﻿using System.Collections.Generic;
using Microsoft.JScript;
using System.CodeDom.Compiler;


namespace Ph.Scripting.Extensions
{
  class JScriptLanguageProvider : ScriptLanguageProvider
  {
    CompilerParameters options = new CompilerParameters();

    public JScriptLanguageProvider()
    {
       provider = new JScriptCodeProvider();
    }

    public override void AddReferences(string[] references)
    {

      options.ReferencedAssemblies.AddRange(references);
    }

    public override bool Debug
    {
      get
      {
        return options.IncludeDebugInformation;
      }
      set
      {
        options.IncludeDebugInformation = value;
      }
    }

    public override bool GenerateInMemory
    {
      get
      {
        return options.GenerateInMemory;
      }
      set
      {
        options.GenerateInMemory = value;
      }
    }

    public override ScriptLanguage Language
    {
      get { return ScriptLanguage.JScript; }
    }


    public override CompilerResults Compile(string[] sources)
    {
      options.OutputAssembly = outputAssembly;
      
      options.CompilerOptions += " /target:" + target.ToString().ToLower();
      CompilerResults result = provider.CompileAssemblyFromFile(options, sources);

      options.ReferencedAssemblies.Clear();
      options.CompilerOptions = "";
      options.ReferencedAssemblies.Clear();
      options.TempFiles.Delete();
      return result;
    }
  }
}
