﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ph.Extensions.Char;

namespace Ph.Scripting.Extensions
{
  class SettingNode
  {
    protected string node;
    protected Settings parent;

    public SettingNode(Settings parent, string node)
    {
      this.node = node;
      this.parent = parent;
    }

    public string Node
    {
      get { return node; }
      set { node = value; }
    }

    public string this[string index]
    {
      get { return parent[node + index]; }
      set { parent[node + index] = value; }
    }

    public int GetInt(string index)
    {
      return parent.GetInt(node + index);
    }

    public float GetFloat(string index)
    {
      return parent.GetFloat(node + index);
    }

    public string GetString(string index)
    {
      return parent.GetString(node + index);
    }

    public void SetInt(string index, int value)
    {
      parent.SetInt(index, value);
    }

    public void SetFloat(string index, float value)
    {
      parent.SetFloat(index, value);
    }

    public void SetString(string index, string value)
    {
      parent.SetString(index, value);
    }


  }

  class SettingNodeInt : SettingNode
  {

    public SettingNodeInt(Settings parent, string node)
      : base(parent, node)
    {

    }

    public new int this[string index]
    {
      get { return parent.Int[node + index]; }
      set { parent.Int[node + index] = value; }
    }
  }



  class Settings
  {

    SortedDictionary<string, string> stringData = new SortedDictionary<string, string>();
    SortedDictionary<string, int> intData = new SortedDictionary<string, int>();
    SortedDictionary<string, float> floatData = new SortedDictionary<string, float>();
    List<string> files = new List<string>();
    bool loaded = false;

    public bool Loaded
    {
      get { return loaded; }
    }

    public SettingNode GetNode(string name)
    {
      return new SettingNode(this, name + '.');
    }

    public SettingNodeInt GetIntNode(string name)
    {
      return new SettingNodeInt(this, name + '.');
    }

    public string this[string index]
    {
      get { return stringData[index]; }
      set { stringData[index] = value; }
    }

    public SortedDictionary<string, int> Int
    {
      get { return intData; }
    }

    public SortedDictionary<string, string> String
    {
      get { return stringData; }
    }

    public SortedDictionary<string, float> Float
    {
      get { return floatData; }
    }

    public int GetInt(string name)
    {
      return intData[name];
    }

    public void SetInt(string name, int value)
    {
      intData[name] = value;
    }

    public float GetFloat(string name)
    {
      return floatData[name];
    }

    public void SetFloat(string name, float value)
    {
      floatData[name] = value;
    }

    public string GetString(string name)
    {
      return stringData[name];
    }

    public void SetString(string name, string value)
    {
      stringData[name] = value;
    }


    public void Load(System.IO.TextReader reader)
    {
      string buffer = reader.ReadToEnd();
      StringBuilder curNode = new StringBuilder();

      int offset = 0;
      int a, b, c, d;
      b = c = 0;
      while (offset < buffer.Length - 3) // minimum amount of characters required for a full line
      {
        a = -1 + offset; d = offset;
        while (buffer[++a] == ' ' || buffer[a] == '\t') ;
        if (buffer[a] == '<')
        {
          if (buffer[a + 1] == '/')
            ++a;
          b = ++a;
          while (buffer[++b] != '>') ;

          if (buffer[a - 1] == '/')
            curNode.Remove(curNode.Length - (b - a) - 1, b - a + 1);
          else
            curNode.Append(buffer.Substring(a, b - a)).Append('.');
          d = b;
          while (++d < buffer.Length && buffer[d] != '\r') ;
        }
        else if (buffer[a] != '\r')
        {

          b = a;
          while (buffer[++b] != ' ' && buffer[b] != '\t') ;
          c = b;
          while (buffer[++c] == ' ' || buffer[c] == '\t') ;
          d = c;
          while (++d < buffer.Length && buffer[d] != '\r') ;

          string name = curNode.Length != 0 ? curNode.ToString() + buffer.Substring(a, b - a) : buffer.Substring(a, b - a);
          string value = buffer.Substring(c, d - c);

          if (value[0].IsNumeric())
            if (value[value.Length - 1] == 'f') // float val
              floatData.Add(name, float.Parse(value.Remove(value.Length - 1)));
            //else if (value[value.Length - 1] == 'd') // double val
            //  doubleData.Add(name, double.Parse(value);
            else
              intData.Add(name, int.Parse(value));
          else
            stringData.Add(name, value);
        }
        else
        {
          d = a;
        }

        offset = d + 2;

        continue;

      }
      loaded = true;
    }

    public void Load(string fileName)
    {
      if (!files.Contains(fileName))
        files.Add(fileName);
      using (System.IO.TextReader reader = new System.IO.StreamReader(fileName))
        this.Load(reader);

    }

    public void Reload()
    {
      Unload();
      foreach (string fileName in files)
        Load(fileName);
    }

    public void Unload()
    {
      loaded = false;
      stringData.Clear();
      intData.Clear();
      floatData.Clear();
    }
  }
}
