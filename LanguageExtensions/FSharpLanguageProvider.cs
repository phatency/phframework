﻿
using System.CodeDom.Compiler;


namespace Ph.Scripting.Extensions
{
  public class FSharpLanguageProvider : ScriptLanguageProvider
  {
    protected CompilerParameters options = new CompilerParameters();
    bool debug = false;
    public FSharpLanguageProvider() { provider = new FSharpCodeProvider(); }

    public override ScriptLanguage Language
    {
      get { return ScriptLanguage.FSharp; }
    }


    public override void AddReferences(string[] references)
    {
      options.ReferencedAssemblies.AddRange(references);
    }

    public override bool Debug
    {
      get
      {
        return debug;
      }
      set
      {
        debug = value;
      }
    }

    public override bool GenerateInMemory
    {
      get
      {
        return options.GenerateInMemory;
      }
      set
      {
        options.GenerateInMemory = value;
      }
    }

    public override CompilerResults Compile(string[] sources)
    {
      if (debug)
        options.CompilerOptions += " --debug pdbonly";

      //options.CompilerOptions += " --noframework";

      options.CompilerOptions += " --target " + target.ToString().ToLower();
      options.OutputAssembly = outputAssembly;
      CompilerResults results = provider.CompileAssemblyFromFile(options, sources);
      options.CompilerOptions = "";
      options.ReferencedAssemblies.Clear();
      options.TempFiles.Delete();
      return results;
    }
  }
}

