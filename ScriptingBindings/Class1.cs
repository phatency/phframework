﻿using System;
using System.Xml.Serialization;
using Ph.Scripting;
using Ph;
using System.Collections.Generic;


namespace ScriptBindings
{
  public enum ParameterCount
  {
    SingleFullString = -2,
    Variable = -1,
    None = 0,
    One = 1,
    Two,
    Three,
    Four,
    Five,
    Six,
    Seven,
    Eight,
    Nine,
    Ten
  }


  public abstract class ConsoleCommand : ScriptObject
  {
    protected const int VARIABLE = -1;
    public override void Init() { Console.WriteLine("ConsoleCommand:Init()"); }

    public abstract void Execute(params string[] parameters);

    public virtual ParameterCount ExpectedParameterNum
    {
      get { return ParameterCount.None; }
    }
  }

  public interface ICommandLine
  {
     void Execute(string line);
  }


  [XmlRoot("ConsoleCommand")]
  public class ConsoleCommandDefinition : ScriptDefinition
  {
    string synopsis, usage, help;

    public string Synopsis
    {
      get
      {
        return synopsis;
      }
    }

    public string Usage
    {
      get
      {
        return usage;
      }
    }

    public string Help
    {
      get
      {
        return help;
      }
    }

    protected override void AddParameter(string name, string value)
    {
      switch (name)
      {
        case "Synopsis":
          synopsis = value;
          break;
        case "Usage":
          usage = value;
          break;
        case "Help":
          help = value;
          break;
        default:
          break;
      }
    }

    protected override IEnumerable<KeyValuePair<string, string>> GetParameters()
    {
      yield return Utility.MakePair("Synopsis", synopsis);
      yield return Utility.MakePair("Usage", usage);
      yield return Utility.MakePair("Help", help);
    }


  }
}
