using System;
using System.CodeDom;
using System.Drawing;
using System.Xml;
using SlimDX;

namespace UI
{
  class UIColor : UIAttribute
  {
    private float? _opacity;
    private Color? _color;
    private string _debugString;
    public float? Opacity
    {
      get { return _opacity ?? 1.0f; }
      set { _opacity = value ?? _opacity; }
    }

    public CodeExpression GetOpacityExpression(CodeExpression relativeTo)
    {
      CodeExpression opacityExpression;
      if (relativeTo != null)
        opacityExpression = Multiply(Opacity.Value, relativeTo);
      else
        opacityExpression = MakeExpression(Opacity.Value);
      return opacityExpression;
    }

    public override void GenerateCode(SortedStatementCache code)
    {
      UIWidget widget = _parent as UIWidget;
      
      if (widget != null)
      {
        CodeExpression relativeTo = null;
        if (widget.HasParent)
          relativeTo = Property(Variable("parent"), "Opacity");
        AssignToField("_opacity", GetOpacityExpression(relativeTo));
        return;
      }

    }

    public void GenerateColorField(SortedStatementCache code, string fieldName, CodeExpression opacityRelativeTo)
    {
      code.Member(NewField(typeof(int), fieldName));
      var colorExpression = ToColor4Expression(opacityRelativeTo);
      var assignColor = AssignToField(fieldName, Invoke(colorExpression, "ToArgb"));
      code.ResetGraphics(assignColor);
    }

    public CodeStatement GenerateColorVariable(string name, CodeFieldReferenceExpression opacityRelativeTo)
    {
      var colorExpression = ToColor4Expression(opacityRelativeTo);
      var assignColor = new CodeVariableDeclarationStatement(typeof(int), name) { InitExpression = Invoke(colorExpression, "ToArgb") };
      return assignColor;
    }

    public Color? Color
    {
      get { return _color ?? System.Drawing.Color.Violet; }
      set { _color = value ?? _color; }
    }

    public CodeObjectCreateExpression ToColor4Expression(CodeExpression relativeTo)
    {
      Color color = Color.Value;
      return New(typeof(Color4), GetOpacityExpression(relativeTo), color.R, color.G, color.B );
    }

    public override void CopyFrom(UIAttribute attribute)
    {
      var other = attribute as UIColor;
      if (other == null)
        return;
     
      Opacity = other._opacity;
      Color = other._color;
    }

    public override bool Empty
    {
      get {
        return (!_opacity.HasValue && !_color.HasValue);
      }
    }

    public UIColor(UIComponent parent) : base(parent)
    {
    }

    public override string Type
    {
      get { return "Color"; }
    }

    public override void Read(XmlReader reader)
    {
      string opacityString = reader["opacity"];
      _debugString = opacityString;
      if (opacityString != null)
        Opacity = UIHelper.GetSpatialValue(opacityString, 100) * 0.01f;
      string colorString = reader["color"] ?? reader["colour"];

      Color color;
      if (UIHelper.TryGetColor(colorString, out color))
        _color = color;
      
    }


  }
}