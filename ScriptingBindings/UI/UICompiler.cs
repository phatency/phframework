﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Xml;
using Ph;
using Ph.Scripting;

namespace UI
{
  public class UICompiler
  {
    private ScriptCompiler _compiler;

    public UICompiler(ScriptCompiler compiler)
    {
      _compiler = compiler;
    }

    public void Compile()
    {
      var uiDir = new DirectoryInfo("scripting\\UI\\");
      List<string> sources = new List<string>();
      List<string> rootWidgets = new List<string>();
      foreach (FileInfo file in uiDir.GetFiles("*.ui", SearchOption.AllDirectories))
      {
        //try
        //{
          var uiParser = new UIParser();
          string path = GenerateCode(uiParser, file);
          sources.Add(path);
          rootWidgets.AddRange(uiParser.Widgets);
        //}
        //catch (Exception ex)
        //{
        //  Log.Error("UICompiler: failed to generate code for {0}", file.Name);
        //}
      }

      ScriptCompiler compiler = Services.ScriptManager.Compiler;
      CompileParameters p = new CompileParameters
      {
        AssemblyName = "UI.dll",
        Debug = false,
        References = new[] { "mscorlib.dll", "System.Drawing.dll", "ScriptingBindings.dll", @"C:\Program Files (x86)\SlimDX SDK (March 2011)\Bin\net40\x86\SlimDX.dll", "System.Windows.Forms.dll" },
        
        SourceCode = sources.ToArray()
      };
      Assembly assembly;
      compiler.Compile(p, out assembly);
      if (assembly != null)
        UIManager.Top = (Widget)assembly.CreateInstance("UserUI.Top", false, BindingFlags.CreateInstance, null, new object[] { null }, null, null);
        //foreach (string widgetTypeName in rootWidgets)
        //{
        //  Widget widget = (Widget)assembly.CreateInstance("UserUI." + widgetTypeName, false,
        //    BindingFlags.CreateInstance, null, new[] { UIManager.Top }, null, null);
        //  if (widget == null)
        //    Log.Error("UIParser: Something went wrong when constructing widget {0}", widgetTypeName);
        //  UIManager.Top.Add(widget);
        //}
    }

    private string GenerateCode(UIParser uiParser, FileInfo file)
    {
      var xmlSettings = new XmlReaderSettings { IgnoreWhitespace = true, IgnoreComments = true };
      
      uiParser.GenerateCodeDomGraph(file);
      ScriptLanguage lang = GetScriptLanguage(uiParser.ScriptLanguage);
      var provider = _compiler.Providers.GetProvider(lang);
      string path = GetSourcePath(file, lang);
      CodeGeneratorOptions options = new CodeGeneratorOptions();
      using (TextWriter writer = new StreamWriter(path))
      {
        provider.CodeDomProvider.GenerateCodeFromNamespace(uiParser.RootNamespace, writer, options);
      }
      return path;
    }

    private string GetSourcePath(FileInfo file, ScriptLanguage lang)
    {
      string extension = _compiler.Providers.GetExtension(lang);
      //return file.Name + extension;
      return new[] {"cache", "gui", file.Name + extension}.RelativePath();
    }


    private static ScriptLanguage GetScriptLanguage(string scriptLanguage)
    {
      scriptLanguage = scriptLanguage.Replace("-", "");
      ScriptLanguage lang = (ScriptLanguage)Enum.Parse(typeof(ScriptLanguage), scriptLanguage, true);
      return lang;
    }
  }
}
