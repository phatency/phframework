﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Forms;
using Ph;
using Ph.Scripting;
using System.Runtime.InteropServices;
using System.Drawing;
using SlimDX;
using SlimDX.Direct3D9;
using UI;


 public class Gui : ScriptEntryPoint
{
  static GraphicsManager graphics;
  static Graphics2D graphics2D; 
  private static List<Widget> _widgets = new List<Widget>();
  
  public override void Init()
  {
    Log.Debug("Gui is starting!");

    Thread graphicsThread = new Thread(StartGraphicsThread);

    //var widget = new Widget();
    //widget.Rectangle = new Rectangle(7, 7, 266, 366); 
    //var widget2 = new Widget();
    //widget2.Rectangle = new Rectangle(7,7,76, 76);
    //widget.Add(widget2);
    //_widgets.Add(widget);
    
    graphicsThread.Start();
  }


  private void StartGraphicsThread()
  {
    InputManager input;
    Application.EnableVisualStyles();
    Application.SetCompatibleTextRenderingDefault(false);
    graphics = new GraphicsManager();
    input = new InputManager(graphics.Form);

    input.RegisterMouseHandler(UIManager.Top.DistributeMouseEvents, true);
    Application.Idle += new EventHandler(Application_Idle);
    Application.ApplicationExit += new EventHandler(Application_Exit);
    graphics2D = new Graphics2D(graphics.Device, new Rectangle(0, 0, 800, 600));
    graphics.Form.SetDesktopLocation(1920, 0);
    graphics.Form.TopMost = true;
    Application.Run(graphics.Form);
    
    
  }

  static void Application_Idle(object sender, EventArgs e)
  {
    graphics.Device.Clear(ClearFlags.Target | ClearFlags.ZBuffer, Color.Gray, 1.0f, 0);
    graphics.Device.BeginScene();
    graphics2D.BeginDraw();
    //graphics2D.ClipStackDebug();
    //foreach (Widget widget in _widgets)
    //  widget.Draw(graphics2D);
    UIManager.Top._Draw(graphics2D) ;
    graphics2D.EndDraw();
    graphics.Device.EndScene();
    graphics.Device.Present();

  }

  static void Application_Exit(object sender, EventArgs e)
  {
    graphics.Dispose();
  }


}



namespace UI
{


  [StructLayout(LayoutKind.Sequential)]
  public struct Vertex
  {
    public Vector4 PositionRhw;
    public int Color;

    public Vertex(float x, float y, float z, int color)
    {
      this.PositionRhw = new Vector4(x, y, z, 1.0f);
      this.Color = color;
    }

    public Vertex(Vector4 position, int color)
    {
      this.PositionRhw = position;
      this.Color = color;
    }
    public const int SizeInBytes = 16;
  }

  [StructLayout(LayoutKind.Sequential)]
  struct VertexColoredTextured
  {
    public Vector3 Position;
    public int Color;
    public Vector2 Texture;

    public static VertexFormat Format
    {
      get { return VertexFormat.Position | VertexFormat.Diffuse | VertexFormat.Texture1; }
    }

    public static int SizeInBytes
    {
      get { return 24; }
    }
  }




}