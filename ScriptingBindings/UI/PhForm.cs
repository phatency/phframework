﻿using System.Windows.Forms;
namespace UI
{
  [System.ComponentModel.DesignerCategory("")]
  public class PhForm : Form
  {
    private const int WM_KEYDOWN = 256;
    private const int WM_SYSKEYDOWN = 260;
    private const int LPARAM_KEYREPEAT = 0x40000000;
    private bool keyRepeat = false;


    public bool KeyRepeat
    {
      get { return keyRepeat; }
      set { keyRepeat = value; }
    }

    protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
    {
      if (msg.Msg == WM_KEYDOWN)
      {
        if (keyRepeat == false && ((int)msg.LParam & LPARAM_KEYREPEAT) != 0)
          return true;
      }
      return base.ProcessKeyMessage(ref msg);
    }

    protected override bool ProcessKeyEventArgs(ref Message m)
    {
      if (m.Msg == WM_SYSKEYDOWN)
      {
        if (keyRepeat == false && ((int)m.LParam & LPARAM_KEYREPEAT) != 0)
          return true;
      }
      return base.ProcessKeyEventArgs(ref m);

    }

    internal void Debug(MouseEventArgs e)
    {
    }
  }
} 