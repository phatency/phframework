﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Ph;
using Ph.Extensions.ICollection;

namespace UI
{
  public class Widget
  {

    protected ClipRectangle _rectangle;
    public ClipRectangle Rectangle { get { return _rectangle; } set { _rectangle = value; } }
    protected List<Widget> _children = null;
    protected Widget _parent = null;
    protected bool _visible = true;
    protected float _opacity = 1.0f;
    protected MouseEvent _mouseCallbacks;
    public float Opacity { get { return _opacity; } } 
    public bool Visible { get { return _visible; } } 

    public virtual bool Movable { get { return false; } }

      public bool HasChildren
    {
      get { return _children == null ? false : !_children.IsEmpty<Widget>(); }
    }

    public Widget(Widget parent)
    {
      _parent = parent;
    }  

    public void Add(Widget widget)
    {
      if (_children == null)
        _children = new List<Widget>();
      _children.Add(widget);
    } 


    public bool Remove(Widget widget)
    {
      return _children.Remove(widget);
    } 

    public void MoveToBottom(Widget widget)
    {
      _children.Remove(widget);
      _children.Insert(0, widget);
    }

    internal void _Draw(Graphics2D graphics2D)
    {
      graphics2D.PushClipArea(_rectangle);
      //graphics2D.ClipStackDebug();
      Draw(graphics2D);

      if (this.HasChildren)
        foreach (Widget child in _children)
          if (child._visible)
            child._Draw(graphics2D);
      graphics2D.PopClipArea();
    }

    protected void MoveWidget(int offsetX, int offsetY)
    {
      _rectangle.AddOffset(offsetX, offsetY);
      if (HasChildren)
      foreach (Widget child in _children)
        child.MoveWidget(offsetX, offsetY);
      AddOffset(offsetX, offsetY);
    }

    protected virtual void AddOffset(int offsetX, int offsetY)
    {
      
    }

    protected virtual void Draw(Graphics2D graphics2D)
    {

      graphics2D.DrawRectangle(new Rectangle(0, 0, _rectangle.Width, _rectangle.Height));
    }

    #region Mouse input
    internal bool DistributeMouseEvents(InputManager input, MouseEvent type, MouseEventArgs e)
    {
      if (this.HasChildren && e != null)
        foreach (Widget child in _children)
          if (( child.Rectangle.Contains(e.Location)) )
            if (child.DistributeMouseEvents(input,type, e))
              return true;

      bool retVal = false;
      if (type == MouseEvent.MouseMove)
        if (_mouseCallbacks.IsSet(MouseEvent.MouseEnter) && !_mouseCallbacks.IsSet(MouseEvent.HasEntered))
        {
          input.RegisterMouseLeaveEvent(DistributeMouseEvents, _rectangle);
          retVal = ExecuteMouseCallback(input, MouseEvent.MouseEnter, e);
        }

      if (_mouseCallbacks.IsSet(type))
        retVal |= ExecuteMouseCallback(input, type, e);
      return retVal;
    }

    private bool ExecuteMouseCallback(InputManager input, MouseEvent type, MouseEventArgs e)
    {
      switch (type)
      {
        case MouseEvent.MouseClick:
          return MouseClick(e);
        case MouseEvent.MouseDown:
          return MouseDown(input, e);
        case MouseEvent.MouseUp:
          return MouseUp(e);
        case MouseEvent.MouseEnter:
          _mouseCallbacks = _mouseCallbacks.Set(MouseEvent.HasEntered, true);
          return MouseEnter(e);
        case MouseEvent.MouseLeave:
          _mouseCallbacks = _mouseCallbacks.Set(MouseEvent.HasEntered, false);
          return MouseLeave(e);
        case MouseEvent.MouseMove:
          return MouseMove(e);
        default:
          return false;
      }
    }
   
    protected virtual bool MouseClick(MouseEventArgs e) { return false; }
    protected virtual bool MouseMove(MouseEventArgs e) { return false; }
    protected virtual bool MouseDown(InputManager sender, MouseEventArgs e) { return false; }
    protected virtual bool MouseUp(MouseEventArgs e) { return false; }
    protected virtual bool MouseEnter(MouseEventArgs e) { return false; }
    protected virtual bool MouseLeave(MouseEventArgs e) { return false; }
    protected void RegisterMouseEvent(MouseEvent mouseEvent)
    {
      _mouseCallbacks = _mouseCallbacks.Set(mouseEvent, true);
    }
    protected void UnregisterMouseEvent(MouseEvent mouseEvent)
    {
      _mouseCallbacks = _mouseCallbacks.Set(mouseEvent, false);
    }
    #endregion
  }

  public class Window : Widget/*, IMovable*/
  {
    

    protected Window(Widget parent)
      : base(parent)
    {
      //ResetGraphics();
      RegisterMouseEvent(MouseEvent.MouseDown | MouseEvent.MouseEnterLeave | MouseEvent.MouseUp | MouseEvent.MouseMove);
    }

    public void Move(int x, int y)
    {
      
    }

   // abstract protected void ResetGraphics();

    public override bool Movable { get { return true; } }

    protected override bool MouseDown(InputManager input, MouseEventArgs e)
    {
        input.BeginDrag( (sender, drag) =>
                            {
                              MoveWidget(drag.Offset.X, drag.Offset.Y);
                            });
        Log.Debug("Dragging started");
      
      return true;
    }


  }

  //public interface IMovable
  //{
  //  void Move(int x, int y);
  //  bool Movable { get; }
  //}


}