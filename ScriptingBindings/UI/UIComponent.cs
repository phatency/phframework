using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Linq;
using Ph.Collections;

namespace UI
{
  class UIComponent : CodeDomHelper, IXmlAttributeReader
  {
    private string _name;
    protected UIComponent _parent;
    protected OverwritingDictionary<string, UIAttribute> _attributes = new OverwritingDictionary<string, UIAttribute>();
    protected bool _hasName = true;
    
    public string Name
    {
      get { return _name; }
    }

    public bool HasParent
    {
      get { return _parent != null; }
    }

    public virtual UIComponent Parent
    {
      get { return _parent; }
      set { _parent = value; }
    }

    public virtual string Type
    {
      get { return "UIComponent"; }
    }

    public UIColor Color
    {
      get { return _attributes["Color"] as UIColor; }
    }

    public UISize Size
    {
      get { return _attributes["Size"] as UISize; }
    }

    public UIComponent(UIComponent parent)
    {
      _parent = parent;
    }

    public void Add(UIAttribute attribute)
    {
      UIAttribute oldAttribute;
      if (_attributes.TryGetValue(attribute.Type, out oldAttribute))
        if (!oldAttribute.Empty)
          attribute.CopyFrom(oldAttribute);
      _attributes.Add(attribute.Type, attribute);
        
    }

    public virtual void Read(XmlReader reader)
    {
      _name = reader["name"];
      if (_name == null)
      {
        _name = HasParent ? GetVariableName("$parent_" + Type) : GetVariableName(Type);
        _hasName = false;
      }
      if (HasParent)
        _name = _name.Replace("$parent", _parent.Name);
      var commonAttributes = new UIAttribute[] { new UISize(this), new UIColor(this) };
      foreach (var attr in commonAttributes)
      {
        attr.Read(reader);

      }
    }

    public virtual void GenerateCode(SortedStatementCache code)
    {
      
    }

    protected void GenerateAttributes(SortedStatementCache code)
    {
      foreach (UIAttribute attribute in _attributes.Values)
        attribute.GenerateCode(code);
    }

    public override string ToString()
    {
      StringBuilder sb = new StringBuilder();
      ToString(sb);
      return sb.ToString();
    }

    protected virtual void ToString(StringBuilder sb)
    {
      sb.AppendFormat("<{0} type=\"{1}\">\n", this.Name, this.Type);
      sb.AppendFormat("</{0}>\n", this.Name);
    }
  }
}