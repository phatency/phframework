using System.CodeDom;
using System.Xml;

namespace UI
{
  class UIText : UIGraphicComponent
  {
    string _text;

    public override string Type
    {
      get { return "Text"; }
    }

    public UIText(UIComponent parent)
      : base(parent)
    {
    }

    public override void Read(XmlReader reader)
    {
      base.Read(reader);
      _text = reader.ReadString();
    }

    public override void GenerateCode(SortedStatementCache code)
    {
      base.GenerateCode(code);
      
      CodeExpression color;
      UIColor uiColor = Color;
      if (!uiColor.Color.HasValue)
        uiColor.Color = System.Drawing.Color.Black;
      
      string fieldName = GetVariableName("_textColor");
      uiColor.GenerateColorField(code, fieldName, ThisField("_opacity"));
      color = ThisField(fieldName);
      
      var rect = Size.GetExpressionRectangle(ThisField("_rectangle"));

      var drawStatement = Invoke(Variable("graphics2D"), "DrawString", null, _text, rect.XOffset, rect.YOffset, color);
      code.Draw(drawStatement);
    }

  }
}