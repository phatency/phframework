﻿using System;
using System.Drawing;

using Ph;
using Ph.Extensions.String;
using SlimDX;

namespace UI
{
  public static class UIHelper
  {
    public static ClipRectangle CreateWidgetClipRectangle(string x, string y, string width, string height, ClipRectangle parent)
    {
      int _x = GetSpatialValue(x, parent.Width);
      int _y = GetSpatialValue(y, parent.Height);
      int _w = GetSpatialValue(width, parent.Width);
      int _h = GetSpatialValue(height, parent.Height);
      ClipRectangle rect = new ClipRectangle(new Rectangle(_x, _y, _w, _h), _x + parent.XOffset, _y + parent.YOffset);
      return rect;
    }

    public static int GetSpatialValue(string value, int relativeValue)
    {
      if (value == null)
        return 0;
      int percentIndex = value.IndexOf('%');
      if (percentIndex != -1)
        value = value.Substring(0, percentIndex);
      if (percentIndex != -1 || value.Contains("."))
      {
        float f;
        if (float.TryParse(value, out f))
          if (percentIndex != -1)
            return (int)(f * relativeValue * 0.01);
          else
            return (int)(f * relativeValue);
        Log.Error("Error parsing {0}", value);
      }

      int i;
      if (!int.TryParse(value, out i))
        return 0;
      return i;
    }

    public static bool TryGetPercentValue(string value, out float result)
    {
      result = 0.0f;
      if (value == null)
        return false;
      int percentIndex = value.IndexOf('%');
      if (percentIndex != -1)
        value = value.Substring(0, percentIndex);
      if (percentIndex == -1 && !value.Contains("."))
        return false;
      float f;
      if (float.TryParse(value, out f))
        if (percentIndex != -1)
          result = f * 0.01f;
        else
          result = f;
      else
        Log.Error("Error parsing {0}", value);
      return true;
    }

    public static Color GetColor(string colorString)
    {
      Color color;
      TryGetColor(colorString, out color);
      return color;
    }

    public static bool TryGetColor(string colorString, out Color color)
    {
      if (colorString != null)
      {
        colorString = colorString.Trim();
        if (!colorString.IsEmpty() || colorString.StartsWith("#") || Char.IsLetter(colorString[0]))
        {
          color = ColorTranslator.FromHtml(colorString);
          return true;
        }
      }
      color = Color.Violet;
      return false;
    }

    public static Color4 GetColor4(Color color, float opacity)
    {
      var c = new Color4(color);
      c.Alpha = opacity;
      return c;
    }

    internal static int GetColorArgb(string colorString, float opacity, string opacityString)
    {
      var c = GetColor4(GetColor(colorString), opacity);
      float opacity2 = GetSpatialValue(opacityString, 100) * 0.01f;
      c.Alpha *= opacity2;
      return c.ToArgb();
    }
  }
}
