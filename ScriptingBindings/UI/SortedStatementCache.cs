using System;
using System.CodeDom;
using System.Collections.Generic;

namespace UI
{
  class SortedStatementCache : CodeDomHelper
  {
    private Dictionary<StatementPosition, CodeStatementCollection> _constructor = new Dictionary<StatementPosition, CodeStatementCollection>();
    private Dictionary<StatementPosition, CodeStatementCollection> _reset = new Dictionary<StatementPosition, CodeStatementCollection>();
    private Dictionary<StatementPosition, CodeStatementCollection> _draw = new Dictionary<StatementPosition, CodeStatementCollection>();
    private Dictionary<StatementPosition, CodeStatementCollection> _addOffset = new Dictionary<StatementPosition, CodeStatementCollection>();

    private CodeTypeMemberCollection _members = new CodeTypeMemberCollection();

    private IEnumerable<Dictionary<StatementPosition, CodeStatementCollection>> Collections
    {
      get { 
        yield return _constructor;
        yield return _reset;
        yield return _draw;
      }
    }

    public void Clear()
    {
      foreach (var col in Collections)
        col.Clear();
    }

    public CodeStatementCollection GetConstructorStatements()
    {
      return Squash(_constructor);
    }

    public CodeStatementCollection GetAddOffsetStatements()
    {
      return Squash(_addOffset);
    }

    public CodeStatementCollection GetResetGraphicsStatements()
    {
      return Squash(_reset);
    }

    public CodeStatementCollection GetDrawStatements()
    {
      return Squash(_draw);
    }

    private CodeStatementCollection Squash(Dictionary<StatementPosition, CodeStatementCollection> collection)
    {
      var newCollection = new CodeStatementCollection();
      foreach (var col in collection)
        newCollection.AddRange(col.Value);
      return newCollection;
    }

    public SortedStatementCache()
    {
      foreach (var col in Collections)
        foreach (StatementPosition pos in Enum.GetValues(typeof(StatementPosition)))
          col.Add(pos, new CodeStatementCollection());
    }

    public void Constructor(CodeStatement statement) {
      Add(_constructor,  statement, StatementPosition.Last);
    }

    public void Constructor(CodeStatement statement, StatementPosition position) {
      Add(_constructor, statement, position);
    }

    public void Constructor(CodeExpression statement) {
      Add(_constructor, statement, StatementPosition.Last);
    }

    public void Constructor(CodeExpression statement, StatementPosition position) {
      Add(_constructor, statement,  position);
    }

    public void Member(CodeTypeMember typeMember) {
      _members.Add(typeMember);
    }

    public void ResetGraphics(CodeStatement statement)
    {
      Add(_reset, statement, StatementPosition.First);
    }

    public void ResetGraphics(CodeStatement statement, StatementPosition position)
    {
      Add(_reset, statement, position);
    }

    public void Draw(CodeStatement statement)
    {
      Add(_draw, statement, StatementPosition.First);
    }

    public void Draw(CodeStatement statement, StatementPosition position)
    {
      Add(_draw, statement, position);
    }

    public void Draw(CodeExpression statement)
    {
      Add(_draw, statement, StatementPosition.Last);
    }

    public void Draw(CodeExpression statement, StatementPosition position)
    {
      Add(_draw, statement, position);
    }

    private static void Add(IDictionary<StatementPosition, CodeStatementCollection> collection, CodeStatement statement, StatementPosition position)
    {
      collection[position].Add(statement);
    }

    private static void Add(IDictionary<StatementPosition, CodeStatementCollection> collection, CodeExpression statement, StatementPosition position)
    {
      collection[position].Add(statement);
    }

    internal void GenerateCode(CodeTypeDeclaration typeDeclaration)
    {

      CodeTypeMemberCollection methods = new CodeTypeMemberCollection();
      typeDeclaration.Members.AddRange(_members);
      

      var addOffset = new CodeMemberMethod { Attributes = MemberAttributes.Override | MemberAttributes.Family, Name = "AddOffset" };
      addOffset.Parameters.Add(new CodeParameterDeclarationExpression(typeof(int), "xOffset"));
      addOffset.Parameters.Add(new CodeParameterDeclarationExpression(typeof(int), "yOffset"));
      addOffset.Statements.AddRange(GetAddOffsetStatements());

      var resetStatements = GetResetGraphicsStatements();
      if (resetStatements.Count > 0)
      {
        var resetGraphics = new CodeMemberMethod {Attributes = MemberAttributes.Public, Name = "ResetGraphics"};
        resetGraphics.Statements.AddRange(resetStatements);
        Constructor(ThisInvoke("ResetGraphics"), StatementPosition.Last);
        methods.Add(resetGraphics);
      }
      var drawStatements = GetDrawStatements();

      if (drawStatements.Count > 0)
      {
        var draw = new CodeMemberMethod
                     {Attributes = MemberAttributes.Override | MemberAttributes.Family, Name = "Draw"};
        draw.Parameters.Add(new CodeParameterDeclarationExpression(typeof (Graphics2D), "graphics2D"));
        draw.Statements.AddRange(drawStatements);
        methods.Add(draw);
      }

      
      var constructor = new CodeConstructor { Attributes = MemberAttributes.Public };
      constructor.Parameters.Add(new CodeParameterDeclarationExpression(typeof(Widget), "parent"));
      constructor.BaseConstructorArgs.Add(new CodeVariableReferenceExpression("parent"));
      constructor.Statements.AddRange(GetConstructorStatements());

      methods.Add(constructor);
      typeDeclaration.Members.AddRange(methods);
    }

  }
}