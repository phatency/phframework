﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using SlimDX.Direct3D9;
using System.Linq;

namespace UI
{
  enum StatementPosition
  {
    First,
    Last
  }

  interface IXmlAttributeReader
  {
    void Read(XmlReader reader);
  }

  abstract class UIAttribute : CodeDomHelper, IXmlAttributeReader
  {
    protected UIComponent _parent;

    public virtual bool Empty
    {
      get { return true; }
    }

    public abstract string Type
    {
      get;
    }

    public UIAttribute(UIComponent parent)
    {
      _parent = parent;
      _parent.Add(this);
    }

    public abstract void Read(XmlReader reader);
    public virtual void GenerateCode(SortedStatementCache code)
    {
    }

    public virtual void ToString(StringBuilder sb){}

    public virtual void CopyFrom(UIAttribute attribute)
    {
      return;
    }
  }

  interface ITypeGenerator
  {
    CodeTypeDeclaration GenerateType();
  }

  class UITopWidget : UIWidget
  {
    public UITopWidget(UIComponent parent) : base(parent)
    {
    }

    public override void GenerateCode(SortedStatementCache code)
    {
      AddChildrenInitializers(code);
    }
  }

  class UIGraphicComponent : UIComponent
  {
    public UIGraphicComponent(UIComponent parent) : base(parent) 
    {
      var parentWidget = parent as UIWidget;
      if (parentWidget != null)
        parentWidget.Add(this);
    }

    public override void Read(XmlReader reader)
    {
      base.Read(reader);
    }
  }

  class UIGraphicVertexComponent : UIGraphicComponent
  {

    public UIGraphicVertexComponent(UIComponent parent) : base(parent)
    {
    }



  }

  class UIRectangle : UIGraphicVertexComponent
  {
    static int[] rectangleIndices = new[] { 0, 1, 1, 2, 2, 3, 3, 0 };
    static string[] _variableNames = new[] { "x", "y", "xw", "yh" };
    public UIRectangle(UIComponent parent) : base(parent)
    {

    }

    public override void GenerateCode(SortedStatementCache code)
    {
      var parent = _parent as UIGraphicCollection;
      if (parent == null) throw new Exception();

      //string[] variables = _variableNames.Select((x) => GetVariableName(x)).ToArray();
      var rect = Size.GetExpressionRectangle(ThisField("_rectangle"));
      CodeExpression[] dim = {rect.X, rect.Y, rect.Width, rect.Height};
      CodeExpression[] vars = dim;
      string colorName = GetVariableName("_rectangleColor");
      code.ResetGraphics(Color.GenerateColorVariable(colorName, ThisField("_opacity")), StatementPosition.First);
      CodeExpression color = Variable(colorName);
      //for (int i = 0; i < 4; i++)
      //{

      //}
      parent.AddVertices(PrimitiveType.LineList, rectangleIndices, new[] { vars[0], vars[1], color }, new[] { vars[2], vars[1], color },
        new[] { vars[2], vars[3], color }, new[] { vars[0], vars[3], color });


      //        var dim = MakeExpression(new[] { rect.X, rect.Y, rect.Width, rect.Height });
      //string[] variableNames = new [] {"x", "y", "xw", "yh"};
      //CodeExpression[] vars = new CodeExpression[4];

      //for (int i = 0; i < 4; i++)
      //{
      //  string variableName = GetVariableName(variableNames[i]);
      //  var variableDeclaration = new CodeVariableDeclarationStatement(typeof(float), variableName, dim[i]);
      //  vars[i] = Variable(variableName);
      //  widget.ResetGraphics.Statements.Add(variableDeclaration);
      //}

      //if (style == RectangleStyle.Border)
      //AddLines(widget, true, new[] { vars[0], vars[1], color }, new[] { vars[2], vars[1], color },
      //  new[] { vars[2], vars[3], color }, new[] { vars[0], vars[3], color });
    }

  }


  class UIGraphicCollection : UIComponent
  {
    struct ExpressionVertex
    {
      public CodeExpression X;
      public CodeExpression Y;
      public CodeExpression Color;
    }

    class IndexedVertexList
    {
      public List<ExpressionVertex> Vertices = new List<ExpressionVertex>();
      public List<int> Indices = new List<int>();

    }

    private List<UIGraphicComponent> _graphics = new List<UIGraphicComponent>();
    Dictionary<PrimitiveType, IndexedVertexList> _vertices = new Dictionary<PrimitiveType, IndexedVertexList>();

    public UIGraphicCollection(UIWidget parent) : base(parent) { }

    public void Add(UIGraphicComponent component)
    {
      component.Parent = this;
      _graphics.Add(component);
    }

    public override void GenerateCode(SortedStatementCache code)
    {
      base.GenerateCode(code);
      foreach (var graphic in _graphics)
        graphic.GenerateCode(code);
    }

    public void AddVertices(PrimitiveType type, int[] indices, params CodeExpression[][] vertices)
    {
      IndexedVertexList vertexList;
      if (!_vertices.TryGetValue(type, out vertexList))
      {
        vertexList = new IndexedVertexList();
        _vertices.Add(type, vertexList);
      }
      int baseIndex = vertexList.Indices.Count;

      for (int i = 0; i < vertices.Length; ++i)
        vertexList.Vertices.Add(new ExpressionVertex { X = MakeExpression(vertices[i][0]), Y = MakeExpression(vertices[i][1]), Color = MakeExpression(vertices[i][2]) });
      Array.ForEach(indices, x => vertexList.Indices.Add(x + baseIndex));

    }
    private CodeObjectCreateExpression CreateVertex(object x, object y, object z, object color)
    {
      return New(typeof(Vertex), x, y, z, color);
    }
  }




  public class WidgetGenerationHelper : CodeDomHelper
  {
    private string _name;
    private CodeConstructor _constructor = null;
    public string Name
    {
      get { return _name; }
      set
      {
        Widget.Name = value;
        _name = value;
        Constructor.Name = value;
      }
    }

    public CodeConstructor Constructor
    {
      get { return _constructor; }

    }

    public CodeTypeDeclaration Widget;
    public CodeFieldReferenceExpression Parent;
    public CodeMemberMethod AddOffset;
    public CodeFieldReferenceExpression Rectangle;
    public CodePropertyReferenceExpression Width;
    public CodePropertyReferenceExpression Height;
    public CodeThisReferenceExpression Self;
    public CodeMemberMethod Draw;
    public CodeMemberMethod ResetGraphics;
    public CodeFieldReferenceExpression Opacity;
    public List<CodeObjectCreateExpression> LineList = new List<CodeObjectCreateExpression>();
    public List<CodeObjectCreateExpression> TriangleList = new List<CodeObjectCreateExpression>();
    public Dictionary<string, string> Code = new Dictionary<string, string>();
    //public List<UIText> TextList = new List<UIText>();

    public Dictionary<string, CodeMemberMethod> EventSignatures;

    public WidgetGenerationHelper()
    {
      Self = new CodeThisReferenceExpression();
      Widget = new CodeTypeDeclaration {IsClass = true, Attributes = MemberAttributes.Public};
      Opacity = ThisField("_opacity");
      Parent = ThisField("_parent");
      _constructor = new CodeConstructor { Attributes = MemberAttributes.Public };
      Constructor.Parameters.Add(new CodeParameterDeclarationExpression(typeof(Widget), "parent"));
      Constructor.BaseConstructorArgs.Add(new CodeVariableReferenceExpression("parent"));
      Widget.Members.Add(Constructor);

      AddOffset = new CodeMemberMethod { Attributes = MemberAttributes.Override | MemberAttributes.Family, Name = "AddOffset" };
      AddOffset.Parameters.Add(new CodeParameterDeclarationExpression(typeof(int), "xOffset"));
      AddOffset.Parameters.Add(new CodeParameterDeclarationExpression(typeof(int), "yOffset"));
      ResetGraphics = new CodeMemberMethod { Attributes = MemberAttributes.Public, Name = "ResetGraphics" };

      Draw = new CodeMemberMethod { Attributes = MemberAttributes.Override | MemberAttributes.Family, Name = "Draw" };
      Draw.Parameters.Add(new CodeParameterDeclarationExpression(typeof(Graphics2D), "graphics2D"));

      Rectangle = ThisField("_rectangle");
      Height = Property(Rectangle, "Height");
      Width = Property(Rectangle, "Width");

      EventSignatures = new Dictionary<string, CodeMemberMethod> 
                                                    {

                                                    };
    }
  }

  //public class UIText
  //{
  //  private Font _font = null;
  //  public string Text;
  //  public CodeExpression X, Y;
  //  public CodeExpression Color;
  //  public UIText(string text, CodeExpression x, CodeExpression y, CodeExpression color)
  //  {
  //    X = x;
  //    Y = y;
  //    Text = text;
  //    Color = color;
  //  }

  //}
}
