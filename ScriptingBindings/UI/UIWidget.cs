using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using Ph.Extensions;

namespace UI
{
  class UIWidget : UIComponent, ITypeGenerator
  {
    public List<UIWidget> Widgets = new List<UIWidget>();
    protected UIGraphicCollection _graphics;
    public SortedStatementCache Statements = new SortedStatementCache();
    public CodeFieldReferenceExpression RectangleField;
    public CodePropertyReferenceExpression RectangleProperty;
    public UIWidget ParentWidget;
    private bool? _visible = null;
    private float _opacity = 1.0f;
    
    public UIWidget(UIComponent parent) : base(parent) 
    {
      _graphics = new UIGraphicCollection(this);
      RectangleField = ThisField("_rectangle");
      RectangleProperty = Property(Self, "Rectangle");
      if (parent == null)
        return;
      UIWidget parentWidget = parent as UIWidget;
      
      if (parentWidget == null)
        throw new Exception();
      ParentWidget = parentWidget;
      parentWidget.Add(this);
    }

    public void Add(UIWidget widget)
    {
      Widgets.Add(widget);
    }

    public void Add(UIGraphicComponent graphic)
    {
      _graphics.Add(graphic);
    }

    public override void Read(XmlReader reader)
    {
      base.Read(reader);
      bool visible;
      
      if (reader.TryGetAttributeAsBoolean("visible", out visible))
        _visible = visible;
      //
      string opacityString = reader["opacity"];

      float opacity = 1.0f;
      if (opacityString != null)
        opacity = 0.01f * UIHelper.GetSpatialValue(opacityString, 100);
      _opacity = opacity;
    }

    public void GenerateWidget(CodeNamespace ns)
    {
      foreach (UIWidget widget in Widgets) // lapset ensin
        widget.GenerateWidget(ns);
      GenerateAttributes(Statements);
      var typeDeclaration = new CodeTypeDeclaration { IsClass = true, Attributes = MemberAttributes.Public, Name = Name };
      GenerateCode(Statements);
      _graphics.GenerateCode(Statements);
      
      Statements.GenerateCode(typeDeclaration);
      if (typeDeclaration.BaseTypes.Count == 0)
        typeDeclaration.BaseTypes.Add(typeof(Widget));
      ns.Types.Add(typeDeclaration);
      return;
    }

    public override void GenerateCode(SortedStatementCache code)
    {
      if (_visible != null)
        code.Constructor(  AssignToField("_visible", _visible.Value), StatementPosition.First);
      
      var setOpacity = new CodeAssignStatement();
      var parentOpacity = Property(new CodeVariableReferenceExpression("parent"), "Opacity");
      setOpacity.Left = ThisField("_opacity");
      setOpacity.Right = Multiply(parentOpacity, _opacity);
      code.Constructor( setOpacity, StatementPosition.First);
      AddChildrenInitializers(code);
    }

    protected void AddChildrenInitializers(SortedStatementCache code)
    {
      foreach (UIWidget widget in Widgets)
      {
        
        CodeObjectCreateExpression childObjectCreate = New(widget.Name, Self);
        CodeExpression childObject = childObjectCreate;
        if (widget._hasName)
        {
          string fieldName = GetPrivateFieldName(widget.Name);
          code.Member(NewField(widget.Name, fieldName, Null));
          code.Constructor(AssignToField(fieldName, childObjectCreate), StatementPosition.Last);
          childObject = ThisField(fieldName);
          var property = new CodeMemberProperty{ Name = widget.Name, HasGet = true, HasSet = false, Attributes = MemberAttributes.Public | MemberAttributes.Final};
          
          property.GetStatements.Add(Return(childObject));
          property.Type = new CodeTypeReference(widget.Name);
          code.Member(property);
        }
         
        var addChild = ThisInvoke("Add", childObject);
        code.Constructor(addChild, StatementPosition.Last);
      }
    }


    protected override void ToString(StringBuilder sb)
    {
      sb.AppendFormat("<{0} type=\"{1}\">\n", this.Name, this.Type);
      foreach (UIAttribute attr in _attributes.Values)
        attr.ToString(sb);
      foreach (UIWidget widget in Widgets)
        widget.ToString(sb);
      
      sb.AppendFormat("</{0}>\n", this.Name);
    }

    #region ITypeGenerator Members

    public CodeTypeDeclaration GenerateType()
    {
      throw new NotImplementedException();
    }

    #endregion
  }
}