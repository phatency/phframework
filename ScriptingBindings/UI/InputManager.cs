﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using Ph;

namespace UI
{
  enum BindingCallbackType
  {
    ConsoleCommand,
  }

  enum KeyType
  {
    KeyDown,
    KeyUp
  }

  class BindingCallback
  {
    public BindingCallbackType Type;
    public string Param;
    public bool Toggleable;

    public BindingCallback(BindingCallbackType type, bool toggleable, string param)
    {
      Toggleable = toggleable;
      Type = type;
      Param = param;
    }
  }

  struct InputBinding : IComparable<InputBinding>, IEqualityComparer<InputBinding>
  {
    readonly bool _ctrl, _alt, _shift;
    readonly Keys _key;

    public InputBinding(KeyEventArgs keyArgs)
    {
      _ctrl = keyArgs.Control;
      _alt = keyArgs.Alt;
      _shift = keyArgs.Shift;
      _key = keyArgs.KeyCode;
      //Console.WriteLine("{3} s:{0} c:{1} a:{2}, d:{4}, mod:{5}", _shift, _ctrl, _alt, keyArgs.KeyCode, (int)keyArgs.KeyData, keyArgs.Modifiers);
    }

    public int CompareTo(InputBinding other)
    {
      int result = _key.CompareTo(other._key);
      result += _ctrl.CompareTo(other._ctrl) << 4;
      result += _alt.CompareTo(other._alt) << 5;
      result += _shift.CompareTo(other._shift) << 6;
      return result;
    }

    #region IEqualityComparer<InputBinding> Members

    public bool Equals(InputBinding x, InputBinding y)
    {
      return (x._shift == y._shift && x._alt == y._alt && x._ctrl == y._ctrl && x._key == y._key);
    }

    public int GetHashCode(InputBinding obj)
    {
      int result = 0;
      result += Convert.ToInt32(_ctrl) << 0;
      result += Convert.ToInt32(_alt) << 1;
      result += Convert.ToInt32(_shift) << 2;
      result += (int)obj._key << 4;
      return result;
    }

    #endregion
  }



  [Flags]
  public enum MouseEvent
  {
    MouseDown  = 0x00000001,
    MouseUp    = 0x00000002,
    MouseClick = 0x00000004,
    MouseMove  = 0x00000010,
    MouseEnter = 0x00000020,
    MouseLeave = 0x00000040,

    MouseEnterLeave = MouseEnter | MouseLeave,

    HasEntered = 0x00000100,
    HasLeft    = 0x00000200,
  }

  public static class MouseEventExtensions
  {
    public static bool IsSet(this MouseEvent mouseEvent, MouseEvent flag)
    {
      return (mouseEvent & flag) == flag;
    }

    public static void Toggle(this MouseEvent mouseEvent, MouseEvent flag)
    {
      throw new NotImplementedException();
    }

    public static MouseEvent Set(this MouseEvent mouseEvent, MouseEvent flag, bool value)
    {
      if (value)
        mouseEvent |= flag;
      else
        mouseEvent &= ~flag;
      return mouseEvent;
    }

  }

  public class DragInformation
  {
    private Nullable<Point> _cursorLocation;
    private Nullable<Point> _lastCursorLocation;
    public Point DragBeginPoint;

    public Point CursorLocation
    {
      get
      {
        if (_cursorLocation.HasValue)
          return _cursorLocation.Value;
        throw new Exception();
      }
      set
      {
        if (_cursorLocation.HasValue)
          _lastCursorLocation = _cursorLocation.Value;
        _cursorLocation = value;

      }
    }

    public Point Offset
    {
      get
      {
        return new Point(CursorLocation.X - LastCursorLocation.X, CursorLocation.Y - LastCursorLocation.Y);
      }
    }

    public Point LastCursorLocation
    {
      get
      {
        if (_lastCursorLocation.HasValue)
          return _lastCursorLocation.Value;
        throw new Exception();
      }
    }
    public InputManager.DragHandler DragHandler;
    public bool Ended;
    public object Tag;
    public DragInformation(Point beginLocation)
    {
      CursorLocation = DragBeginPoint = beginLocation;
    }
  }

  public class InputManager
  {
    public delegate void UnicodeInputHandler(char input);
    private UnicodeInputHandler unicodeInputTHING = null;

    private Dictionary<InputBinding, BindingCallback> bindings;
    private Dictionary<Keys, BindingCallback> toggledKeys;

    PhForm form;
    bool unicodeInput;

    public delegate bool MouseInputHandler(InputManager caller, MouseEvent type, MouseEventArgs e);

    private List<MouseInputHandler> _mouse = new List<MouseInputHandler>();
    private List<KeyValuePair<MouseInputHandler, ClipRectangle>> _leaveCallbacks = new List<KeyValuePair<MouseInputHandler, ClipRectangle>>();
    private MouseEventArgs _mouseArgs;

    private DragInformation _dragInformation;
    public delegate void DragHandler(InputManager sender, DragInformation dragInformation);

    public InputManager(PhForm form)
    {
      this.form = form;
      form.KeyPreview = true;
      form.MouseDown += (sender, e) => DistributeMouseEvents(MouseEvent.MouseDown, e);
      form.MouseMove += (sender, e) =>
                          {
                            _mouseArgs = e;
                            UpdateDrag(e);
                            for (int i = 0; i < _leaveCallbacks.Count; i++)
                              if(!_leaveCallbacks[i].Value.Contains(e.Location))
                              {
                                _leaveCallbacks[i].Key(this, MouseEvent.MouseLeave, e);
                                _leaveCallbacks.RemoveAt(i);
                              }
                            DistributeMouseEvents(MouseEvent.MouseMove, e);
                          };
      form.MouseClick += (sender, e) => DistributeMouseEvents(MouseEvent.MouseClick, e);
      form.MouseUp += (sender, e) => DistributeMouseEvents(MouseEvent.MouseUp, e);
      form.MouseLeave += (sender, e) =>
                           {
                             foreach (var cb in _leaveCallbacks)
                               cb.Key(this, MouseEvent.MouseLeave, null);
                             
                             _leaveCallbacks.Clear();
                           };
                                          
      form.KeyDown += form_KeyDown;
      form.KeyUp += form_KeyUp;
      form.KeyPress += form_KeyPress;
      form.Select();

      bindings = new Dictionary<InputBinding, BindingCallback>();
      bindings.Add(new InputBinding(new KeyEventArgs(Keys.A | Keys.Control)), new BindingCallback(BindingCallbackType.ConsoleCommand, true, "hello"));

      toggledKeys = new Dictionary<Keys, BindingCallback>();

    }

    public void RegisterMouseLeaveEvent(MouseInputHandler handler, ClipRectangle rectangle)
    {
      _leaveCallbacks.Add(Utility.MakePair(handler, rectangle));
    }

    public void BeginDrag(DragHandler callback)
    {
      _dragInformation = new DragInformation(_mouseArgs.Location) { DragHandler = callback };
    }

    private void UpdateDrag(MouseEventArgs mouseArgs)
    {
      if (_dragInformation == null || _dragInformation.Ended)
        return;
      _dragInformation.CursorLocation = mouseArgs.Location;
      if ((mouseArgs.Button & MouseButtons.Left) != MouseButtons.Left)
        _dragInformation.Ended = true;
      _dragInformation.DragHandler(this, _dragInformation);

    }

    public void RegisterMouseHandler(MouseInputHandler handler, bool first)
    {
      if (first)
        _mouse.Insert(0, handler);
      else
        _mouse.Add(handler);
    }

    void DistributeMouseEvents(MouseEvent type, MouseEventArgs e)
    {
      foreach (MouseInputHandler handler in _mouse)
        if (handler(this, type, e))
          continue;
        else
          return;
    }

    void form_KeyPress(object sender, KeyPressEventArgs e)
    {

      if (!unicodeInput)
        return;
      if (unicodeInputTHING == null)
        Console.WriteLine(e.KeyChar);
      else
        unicodeInputTHING(e.KeyChar);

      e.Handled = true;
    }

    void form_KeyUp(object sender, KeyEventArgs e)
    {
      if (unicodeInput)
        return;
      new InputBinding(e);
      BindingCallback callback;
      if (toggledKeys.TryGetValue(e.KeyCode, out callback))
      {
        Call(callback, KeyType.KeyUp);
        toggledKeys.Remove(e.KeyCode);
      }
      e.Handled = true;
    }

    void form_KeyDown(object sender, KeyEventArgs e)
    {
      if (unicodeInput)
        return;

      var input = new InputBinding(e);
      BindingCallback callback;
      if (bindings.TryGetValue(input, out callback))
      {
        Call(callback, KeyType.KeyDown);
        if (callback.Toggleable)
          toggledKeys.Add(e.KeyCode, callback);
      }

      e.Handled = true;
    }

    private void Call(BindingCallback callback, KeyType keyType)
    {
      if (keyType == KeyType.KeyDown)
      {
        if (callback.Toggleable == true)
          Console.WriteLine("Calling +{0}", callback.Param);
        else
          Console.WriteLine("Calling {0}", callback.Param);
      }
      else if (keyType == KeyType.KeyUp && callback.Toggleable == true)
        Console.WriteLine("Calling -{0}", callback.Param);
      else
        Console.WriteLine("Calling nothing");
    }

    public void CaptureUnicodeInput(UnicodeInputHandler handler)
    {
      unicodeInputTHING = handler;
      unicodeInput = true;
      form.KeyRepeat = false;
    }

    public void ReleaseUnicodeInput(UnicodeInputHandler handler)
    {
      if (handler != unicodeInputTHING)
        throw new Exception("Tried to release a unicode input handler which, frankly, doesn't exist");

      unicodeInputTHING = null;
      unicodeInput = false;

    }

  }
}