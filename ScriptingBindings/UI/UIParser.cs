﻿#define DEBUG
using System;
using System.CodeDom;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Ph;
using System.Collections.Generic;
using Ph.Extensions;
using SlimDX;
using SlimDX.Direct3D9;

namespace UI
{
  public static class UIManager
  {
    public static Widget Top = new Widget(null) { Rectangle = new ClipRectangle(new Rectangle(0, 0, 800, 600), 0, 0)};
  }

  enum RectangleStyle
  {
    Border,
    Solid
  };



  public class UIParser : CodeDomHelper
  {
    private string _scriptLanguage = "csharp";
    private CodeNamespace _ns = new CodeNamespace("UserUI");

    public string ScriptLanguage
    { get { return _scriptLanguage; } set { _scriptLanguage = value ?? _scriptLanguage; }}
    public List<string> Widgets { get { return _children; }
    }
    public string FileName;
    private UITopWidget _top = new UITopWidget(null);
    public CodeNamespace RootNamespace { get { return _ns; } }

    List<string> _children = new List<string>();


    public UIParser()
    {
      _ns.Imports.Add(new CodeNamespaceImport("System"));
      _ns.Imports.Add(new CodeNamespaceImport("UI"));
    }

    public void GenerateCodeDomGraph(FileInfo file)
    {
      var xmlSettings = new XmlReaderSettings { IgnoreWhitespace = true, IgnoreComments = true };

      using (var reader = XmlReader.Create(file.FullName, xmlSettings))
      {
        if (!reader.ReadToFollowing("UI"))
          throw new Exception(); // todo: better exception
        ScriptLanguage = reader["language"];

        while (!reader.EOF)
        {
          Read(reader, _top);
        }

        _top.GenerateWidget(_ns);
        Log.Debug(_top.ToString());
       // Log.Debug("{2}: {0} = {1}", reader.Name, reader.Value, reader.NodeType.ToString());
      }
    }

    private void Read(XmlReader reader, UIComponent parent)
    {
      IXmlAttributeReader component = null;
      string parentName = reader.Name;
      while (reader.Read() && (reader.Name != parentName || reader.NodeType != XmlNodeType.EndElement))
      {
        if (reader.NodeType != XmlNodeType.Element)
          continue;
        string childName = reader.Name;
        switch (childName)
        {
          case "Frame":
          case "Window":
            component = new UIWidget(parent);
            break;
          case "UITop":
            component = _top;
            break;
          case "Size":
            component = new UISize(parent);
            break;
          case "Text":
            component = new UIText(parent);
            break;

          default:
            component = new UIComponent(parent);
            break;
        }
        component.Read(reader);

        if (!reader.IsEmptyElement && (reader.NodeType != XmlNodeType.EndElement || reader.Name != childName) && component is UIComponent)
          Read(reader, component as UIComponent);
        
      }
      
    }

#if false

    
    public void GenerateCodeDomGraph(FileInfo file)
    {
      var xmlSettings = new XmlReaderSettings { IgnoreWhitespace = true, IgnoreComments = true };

      using (var reader = XmlReader.Create(file.FullName, xmlSettings))
      {
        if (!reader.ReadToFollowing("UI"))
          throw new Exception(); // todo: better exception
        ScriptLanguage = reader["language"];

        while (reader.Read())
          ReadTree(reader, _ns, null);
        Log.Debug("{2}: {0} = {1}", reader.Name, reader.Value, reader.NodeType.ToString());
      }
    }

    private WidgetGenerationHelper ReadTree(XmlReader reader, CodeNamespace nameSpace, WidgetGenerationHelper parent)
    {
      if (reader.NodeType != XmlNodeType.Element)
        return null;
      WidgetGenerationHelper widget = new WidgetGenerationHelper();
      
      ReadProperties(reader, widget);
      string widgetType = reader.Name;

      if (parent == null)
        _children.Add(widget.Name);

      if (widgetType == "Frame")
        widget.Widget.BaseTypes.Add(new CodeTypeReference(typeof(Widget)));
      else if (widgetType == "Window")
        widget.Widget.BaseTypes.Add(new CodeTypeReference(typeof (Window)));
      else
        Log.Debug("Unknown element {0}", reader.Name);


      while (reader.Read() && (reader.Name != widgetType || reader.NodeType != XmlNodeType.EndElement))
      {
        
        switch (reader.Name)
        {
          case "Size":
            AddSize(reader, widget);
            break;
          case "Frame":
            WidgetGenerationHelper child = ReadTree(reader, nameSpace, widget);
            AddChildWidget(child.Name, widget);
            break;
          case "Border":
            AddBorder(reader, widget);
            break;
          case "Rectangle":
            AddRectangle(reader, widget);
            break;
          case "Text":
            AddText(reader, widget);
            break;
          case "Scripts":
            AddScripts(reader, widget);
            break;
        }
      }
      AddGraphics(widget);
      if (widget.ResetGraphics.Statements.Count > 0)
      {
        widget.Widget.Members.Add(widget.AddOffset);
        widget.Constructor.Statements.Add(ThisInvoke("ResetGraphics"));
        widget.Widget.Members.Add(widget.ResetGraphics);
      }
      if (widget.Draw.Statements.Count > 0)
      {
        widget.Widget.Members.Add(widget.Draw);
      }
      AddCode(widget);
      nameSpace.Types.Add(widget.Widget);
      return widget;
    }

    private void AddCode(WidgetGenerationHelper widget)
    {
      foreach (var code in widget.Code)
      {
        CodeExpression method;
        switch(code.Key)
        {
          case "MouseEnter":
          case "MouseLeave":
          case "MouseMove":
          case "MouseClick":
            widget.Constructor.Statements.Add(ThisInvoke("RegisterMouseEvent", Enum(typeof (MouseEvent), code.Key)));
            var mouseCb = new CodeMemberMethod
                            {Attributes = MemberAttributes.Family | MemberAttributes.Override, Name = code.Key, ReturnType = new CodeTypeReference(typeof(bool))};
            mouseCb.Parameters.Add(new CodeParameterDeclarationExpression(typeof (MouseEventArgs), "e"));
            mouseCb.Statements.Add(new CodeSnippetExpression(code.Value));
            widget.Widget.Members.Add(mouseCb);
            break;
        }
      }
    }

    private void AddScripts(XmlReader reader, WidgetGenerationHelper widget)
    {
      if (reader.IsEmptyElement) return;
      while (reader.Read() && (reader.Name != "Scripts" || reader.NodeType != XmlNodeType.EndElement))
      {
        string eventName = reader.Name;
        string code = reader.ReadString() ?? "";
        widget.Code.Add(eventName, code);
      }
    }

    private void AddText(XmlReader reader, WidgetGenerationHelper widget)
    {
      if (reader.IsEmptyElement)
        return;
      var x = GetRelativeCoordinate(reader["x"], widget.Width);
      var y = GetRelativeCoordinate(reader["y"], widget.Height);
      CodeExpression createColor4 = GetColorExpression(reader["color"], reader["opacity"], widget.Opacity);
      string variableName = GetVariableName("_textColor");
      widget.Widget.Members.Add(new CodeMemberField(typeof (int), variableName));
      widget.ResetGraphics.Statements.Add(AssignToField(variableName, Invoke(createColor4, "ToArgb")));
      widget.TextList.Add(new UIText(reader.ReadString(), x
        , y, Field(widget.Self, variableName)
        ));
    }
    private void AddGraphics(WidgetGenerationHelper widget)
    {

      var graphics2D = new CodeVariableReferenceExpression("graphics2D");
      var device = Property(graphics2D, "Device");

      Func<string,CodeExpression, CodeExpression, CodeIterationStatement> AddOffsets = (fieldName, offsetX, offsetY) =>
                        {
                          var field = ThisField(fieldName);

                          var loop = LoopArray(field);
                          var vertex = new CodeArrayIndexerExpression(field, MakeExpression(Variable("i")));
                          var position = Property(vertex, "PositionRhw");
                          var x = Property(position, "X");
                          var y = Property(position, "Y");
                          loop.Statements.Add(new CodeAssignStatement(x, Add(x, offsetX)));
                          loop.Statements.Add(new CodeAssignStatement(y, Add(y, offsetY)));
                          return loop;
                        };
      Action<List<CodeObjectCreateExpression>, string, string, int> AddDrawingAndResetCode =
        (vertexList, fieldName, enumName, verticesPerPrimitive) =>
        {
          var arrayDeclaration = new CodeMemberField(typeof(Vertex[]), fieldName);
          widget.Widget.Members.Add(arrayDeclaration);
          widget.ResetGraphics.Statements.Add(AssignToField(fieldName, new CodeArrayCreateExpression(typeof(Vertex), vertexList.ToArray())));
          var enumReference = Enum(typeof(PrimitiveType), enumName);
          widget.Draw.Statements.Add(Invoke(device, "DrawUserPrimitives", enumReference, vertexList.Count / verticesPerPrimitive, ThisField(fieldName)));

          widget.ResetGraphics.Statements.Add(AddOffsets(fieldName, Property(widget.Rectangle, "XOffset"), Property(widget.Rectangle, "YOffset")));
          widget.AddOffset.Statements.Add(AddOffsets(fieldName, Variable("xOffset"), Variable("yOffset")));
        };
      if (widget.TriangleList.Count != 0)
        AddDrawingAndResetCode(widget.TriangleList, "_triangleList", "TriangleList", 3);
      if (widget.LineList.Count != 0)
        AddDrawingAndResetCode(widget.LineList, "_lineList", "LineList", 2);

      foreach (UIText text in widget.TextList)
        widget.Draw.Statements.Add(Invoke(graphics2D, "DrawString", null, text.Text, Add(text.X, Property(widget.Rectangle, "XOffset")),
          Add(text.Y, Property(widget.Rectangle, "YOffset")), text.Color));
    }

    private void AddRectangle(XmlReader reader, WidgetGenerationHelper widget)
    {
      var rect = new ExpressionRectangle(reader, widget.Rectangle);
      if (!reader.IsEmptyElement)
        while ((reader.NodeType != XmlNodeType.EndElement || reader.Name != "Rectangle") && reader.Read())
        {
          if (reader.Name == "Border")
            AddRectangle(rect, RectangleStyle.Border, reader, widget);
          else if (reader.Name == "Background")
            AddRectangle(rect, RectangleStyle.Solid, reader, widget);
        }
      else
        AddRectangle(rect, RectangleStyle.Solid, reader, widget);
    }

    private void AddRectangle(ExpressionRectangle rect, CodeExpression color, RectangleStyle style, WidgetGenerationHelper widget)
    {
      var dim = MakeExpression(new[] { rect.X, rect.Y, rect.Width, rect.Height });
      string[] variableNames = new [] {"x", "y", "xw", "yh"};
      CodeExpression[] vars = new CodeExpression[4];

      for (int i = 0; i < 4; i++)
      {
        string variableName = GetVariableName(variableNames[i]);
        var variableDeclaration = new CodeVariableDeclarationStatement(typeof(float), variableName, dim[i]);
        vars[i] = Variable(variableName);
        widget.ResetGraphics.Statements.Add(variableDeclaration);
      }

      if (style == RectangleStyle.Border)
      AddLines(widget, true, new[] { vars[0], vars[1], color }, new[] { vars[2], vars[1], color },
        new[] { vars[2], vars[3], color }, new[] { vars[0], vars[3], color });
      if (style == RectangleStyle.Solid)
      {
        AddTriangles(widget, new int[] {0, 1, 3, 1, 2, 3}, new[]{ new[] {vars[0], vars[1], color}, new[] {vars[2], vars[1], color},
                     new[] {vars[2], vars[3], color}, new[] {vars[0], vars[3], color}});
      }
    }

    private void AddRectangle(ExpressionRectangle rect, RectangleStyle style, XmlReader reader, WidgetGenerationHelper widget)
    {
      CodeExpression createColor4 = GetColorExpression(reader["color"], reader["opacity"], widget.Opacity);
      string colorName = GetVariableName("color");
      var colorVariable = new CodeVariableDeclarationStatement(typeof(int), colorName, Invoke(createColor4, "ToArgb"));

      widget.ResetGraphics.Statements.Add(colorVariable);

      AddRectangle(rect, Variable(colorName), style, widget);
    }

    private void AddLines(WidgetGenerationHelper widget, bool connectHeads, params CodeExpression[][] lines)
    {
      for (int i = 1; i < lines.Length; ++i)
      {
        object x1 = lines[i - 1][0], y1 = lines[i - 1][1], x2 = lines[i][0], y2 = lines[i][1];
        object c1 = lines[i - 1][2], c2 = lines[i][2];
        widget.LineList.Add(CreateVertex(x1, y1, 0.0f, c1));
        widget.LineList.Add(CreateVertex(x2, y2, 0.0f, c2));
      }
      if (connectHeads)
      {
        object x1 = lines[0][0], y1 = lines[0][1], x2 = lines[lines.Length - 1][0], y2 = lines[lines.Length - 1][1];
        object c1 = lines[0][2], c2 = lines[lines.Length - 1][2];
        widget.LineList.Add(CreateVertex(x1, y1, 0.0f, c1));
        widget.LineList.Add(CreateVertex(x2, y2, 0.0f, c2));
      }
    }

    private void AddTriangles(WidgetGenerationHelper widget, int[] indices,  CodeExpression[][] points)
    {
      Log.Warn("Trying to add triangles");
      for (int iter = 0; iter < indices.Length; ++iter)
      {
        int i = indices[iter];
        object x = points[i][0], y = points[i][1], c = points[i][2];
        widget.TriangleList.Add(CreateVertex(x, y, 0.0f, c));
      }
    }

    private void AddBorder(XmlReader reader, WidgetGenerationHelper widget)
    {
      string style = reader["style"];
      if (style == null)
        return; // todo: xml-warning'

      AddRectangle(new ExpressionRectangle(0F,0F, widget.Width, widget.Height), RectangleStyle.Border, reader, widget);
    }

    private CodeObjectCreateExpression CreateVertex(object x, object y, object z, object color)
    {
      return New(typeof (Vertex), x, y, z, color);
    }

    private void ReadProperties(XmlReader reader, WidgetGenerationHelper widget)
    {
      widget.Name = reader["name"]; // todo: xsd makes sure name-attribute is always available.
   
      bool visible;
      if (reader.TryGetAttributeAsBoolean("visible", out visible))
        widget.Constructor.Statements.Add(AssignToField("_visible", visible));
      string opacityString = reader["opacity"];
      float opacity = 1.0f;
      if (opacityString != null)
        opacity = 0.01f * UIHelper.GetSpatialValue(opacityString, 100);
      var setOpacity = new CodeAssignStatement();
      var parentOpacity = Property(new CodeVariableReferenceExpression("parent"), "Opacity");
      setOpacity.Left = widget.Opacity;
      setOpacity.Right = Multiply(parentOpacity, opacity);
      widget.Constructor.Statements.Add(setOpacity);
    }



    private void AddChildWidget(string childName, WidgetGenerationHelper widget)
    {
      var childObject = New(childName,widget.Self);
      var addChild = ThisInvoke("Add", childObject);
      widget.Constructor.Statements.Add(addChild);
    }

    private void AddSize(XmlReader reader, WidgetGenerationHelper widget)
    {
      var setDimensions = new CodeAssignStatement {Left = widget.Rectangle};
    
      setDimensions.Right = new ExpressionRectangle(reader, Property(widget.Parent, "Rectangle")).GetClipRectangle();
      widget.Constructor.Statements.Add(setDimensions);
    }

    internal CodeExpression GetColorExpression(string colorString, string opacityString, CodeFieldReferenceExpression opacityField)
    {
      Color4 color4 = new Color4(UIHelper.GetColor(colorString));
      float opacity = opacityString != null ? UIHelper.GetSpatialValue(opacityString, 100) * 0.01f : 1.0f;
      var opacityExpression = Multiply(opacity,opacityField);
      return New(typeof(Color4), opacityExpression, color4.Red, color4.Green, color4.Blue);
    }
#endif
  }

}