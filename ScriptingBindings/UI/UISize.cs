using System.CodeDom;
using System.Text;
using System.Xml;

namespace UI
{
  class UISize : UIAttribute
  {
    private string _x, _y, _w, _h;
    public string X
    {
      set { _x = value ?? _x; }
      get { return _x; }
    }
    public string Y
    {
      set { _y = value ?? _y; }
      get { return _y; }
    }
    public string Width
    {
      set { _w = value ?? _w; }
      get { return _w; }
    }
    public string Height
    {
      set { _h = value ?? _h; }
      get { return _h; }
    }

      public override bool Empty
    {
      get { return (X ?? Y ?? Width ?? Height) == null; }
    }

    public override string Type
    {
      get { return "Size";  }
    }

    public UISize(UIComponent parent) : base(parent)
    {

    }

    public override void CopyFrom(UIAttribute attribute)
    {
      base.CopyFrom(attribute);
      UISize asSize = attribute as UISize;
      if (asSize == null)
        return;
      X = asSize.X;
      Y = asSize.Y;
      Width = asSize.Width;
      Height = asSize.Height;
    }

    public override void GenerateCode(SortedStatementCache code)
    {
      if (_parent.Name == "Top")
      {
        var assignment = AssignToField("_rectangle", New(typeof(ClipRectangle), 0, 0, int.Parse(Width), int.Parse(Height), 0, 0));
        code.Constructor(assignment, StatementPosition.First);
        return;
      }

      var parent = _parent as UIWidget;
      if (parent != null)
      {
        var rect = new ExpressionRectangle(X, Y, Width, Height, Property(Variable("parent"), "Rectangle"));
        var assignment = AssignToField("_rectangle", rect.GetClipRectangle());
        code.Constructor(assignment, StatementPosition.First);
      }
    }

    public ExpressionRectangle GetExpressionRectangle(CodeExpression relativeClipRectangle)
    {
      return new ExpressionRectangle(X, Y, Width, Height, relativeClipRectangle);
    }

    public override void Read(XmlReader reader)
    {
      X = reader["x"];
      Y = reader["y"];
      Width = reader["width"] ?? reader["w"];
      Height = reader["height"] ?? reader["h"];
    }

    public override void ToString(StringBuilder sb)
    {
      sb.AppendFormat("<Size x=\"{0}\" y=\"{1}\" w=\"{2}\" h=\"{3}\" />\n", X, Y, Width, Height);
    }
  }
}