﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ph;
namespace Testing
{
  using NUnit.Framework;
  using NUnit.Framework.Constraints;
  using NUnit.Framework.SyntaxHelpers;
  using NUnit.Framework.Extensions;
  using System.IO;

  [TestFixture]
  public class SettingLoadingTest : AssertionHelper
  {

    
    [TestFixtureSetUp]
    public void ConstructSettings()
    {

    }
    


    [SetUp]
    [SetCulture("en-US")]
    public void SetUpByLoadingFile()
    {
      LoadSettingsFromFile("settingsTestData.txt");
    }

    [TearDown]
    public void UnloadSettings()
    {
      Settings.Unload();
    }

    public void LoadSettingsFromFile(string fileName)
    {

      Settings.Unload();
      Assert.That(Settings.Loaded, Is.False);
      Settings.Load(Path.Combine("testData", fileName));
      Assert.That(Settings.Loaded, Is.True);
    }

    [SetCulture("en-US")]
    [Test]
    public void TestFloatValLoading()
    {
      for (int i = 0; i <= 4; i++)
      {
        float val = Settings.GetFloat("float.float" + i.ToString());
        Assert.That(val.Equals((float)i));
      }
    }
    [SetCulture("en-US")]
    [Test]
    public void TestStringValLoading()
    {
      for (int i = 0; i <= 4; i++)
      {
        string name = "stringVal" + i.ToString();
        string val = Settings.String["string." + name];
        Assert.That(name.Equals(val));
      }
    }
    [SetCulture("en-US")]
    [Test]
    public void TestIntValLoading()
    {
      for (int i = -1; i <= 4; i++)
      {
        int val = Settings.GetInt("int.int" + i.ToString());
        Assert.That(val.Equals(i));
      }
    }

    [Test]
    [SetCulture("en-US")]
    public void TestNesting()
    {
      string val = Settings.String["nested.a.b.c.d.e.deeplyNestedString"];
      Assert.That(val.Equals("Hello, deep world!"));
    }

    [Test]
    [SetCulture("en-US")]
    public void TestReloadingSettings()
    {
      string val = Settings.String["string.stringVal0"];
      Settings.Reload();
      string val2 = Settings.String["string.stringVal0"];
      Assert.That(val.Equals(val2));
    }

    [Test]
    [SetCulture("en-US")]
    public void TestStringNode()
    {
      SettingNode node = Settings.GetNode("string");
      for (int i = 0; i <= 4; i++)
      {
        string name = "stringVal" + i.ToString();
        string val = node[name];
        Assert.That(name.Equals(val));
      }
    }

    [Test]
    [SetCulture("en-US")]
    public void TestIntNode()
    {
      SettingNodeInt node = Settings.GetIntNode("int");
      for (int i = -1; i <= 4; i++)
      {
        string name = "int" + i.ToString();
        int val = node[name];
        Assert.That(val.Equals(i));
      }
    }

    [Test]
    [SetCulture("en-US")]
    public void Test65000Lines()
    {
      //LoadSettingsFromFile("settingsStressTest.txt");
    }
  }
}
