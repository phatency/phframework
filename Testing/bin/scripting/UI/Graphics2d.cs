﻿using System;
using System.Collections.Generic;
using System.Drawing;
using SlimDX;
using SlimDX.Direct3D9;
using Ph.Extensions.ICollection;

namespace UI
{
  public class Graphics2D
  {
    private readonly Stack<ClipRectangle> _clipStack = new Stack<ClipRectangle>();

    private readonly Device _device;
    private Rectangle _target;

    public Device Device { get { return _device; } }

    public Graphics2D(Device device, Rectangle target)
    {
      _device = device;
      _target = target;
     
    }

    internal bool PushClipArea(ClipRectangle area)
    {
      return true;
      //if (area.IsNegative()) // if area is negative, push an empty rectangle instead //todo: z
      //{
      //  _clipStack.Push(new ClipRectangle(Rectangle.Empty, 0, 0));
      //  return true;
      //}
      //if (_clipStack.IsEmpty())
      //{
        
      //  _clipStack.Push(new ClipRectangle(area, area.X, area.Y));
      //  return true;
      //}
      ClipRectangle top = _clipStack.Peek();


      ClipRectangle carea = new ClipRectangle
        {
          Rectangle = new Rectangle
           {
             Width = area.Width,
             Height = area.Height,
             X = area.X + top.XOffset,
             Y = area.Y + top.YOffset
           },
          XOffset = top.XOffset + area.X,
          YOffset = top.YOffset + area.Y
        };
      

      carea.Intersect(top);
      _device.ScissorRect = carea.Rectangle;
      _clipStack.Push(carea);
      return !carea.Rectangle.IsEmpty;
    }

    internal void PopClipArea()
    {
      //if (_clipStack.Count == 0) //todo: z
      //  throw new Exception();
      //_clipStack.Pop();
    }

    internal ClipRectangle GetCurrentClipRectangle()
    {
      if (_clipStack.Count == 0)
        throw new Exception();
      return _clipStack.Peek();
    }

    internal void BeginDraw()
    {
      //PushClipArea(new Rectangle(0, 0, _target.Width, _target.Height));  //todo: z
      Matrix ortho = Matrix.OrthoLH(_target.Width, _target.Height, 0.0f, 1.0f);
      _device.SetTransform(TransformState.View, Matrix.Identity);
      _device.SetTransform(TransformState.World, Matrix.Identity);
      _device.SetTransform(TransformState.Projection, ortho);
      _device.SetRenderState(RenderState.DitherEnable, true);
      _device.SetRenderState(RenderState.ZEnable, false);
      _device.SetRenderState(RenderState.Lighting, false);
      _device.SetRenderState(RenderState.AlphaBlendEnable, true);
      _device.SetRenderState(RenderState.SourceBlendAlpha, Blend.One);
      _device.SetRenderState(RenderState.SourceBlend, Blend.One);
      
      //_device.SetRenderState(RenderState.ScissorTestEnable, true); //todo: z
      _device.VertexFormat = VertexFormat.PositionRhw | VertexFormat.Diffuse;
   
      _device.SetSamplerState(0, SamplerState.MagFilter, TextureFilter.Point);
      _device.SetSamplerState(0, SamplerState.MinFilter, TextureFilter.Point);
      _device.SetTexture(0, null);
    }


    internal void ClipStackDebug()
    {
      //PushClipArea(new Rectangle(10, 10, _target.Width - 20, _target.Height - 20));
      //PushClipArea(new Rectangle(2, 1, _target.Width - 23, _target.Height - 22));
      //PushClipArea(new Rectangle(5, 5, 20, 10));

      Stack<ClipRectangle> copyStack = new Stack<ClipRectangle>();
      while (_clipStack.Count > 0)
      {
        ClipRectangle rect = _clipStack.Peek();
        DrawRectangle(new Rectangle(0, 0, rect.Rectangle.Width, rect.Rectangle.Height));
        copyStack.Push(_clipStack.Pop());

      }
      while (copyStack.Count > 0)
      {
        _clipStack.Push(copyStack.Pop());
      }
      //PopClipArea();
      //PopClipArea();
      //PopClipArea();
    }



    internal void DrawRectangle(Rectangle rect)
    {
      
      _device.VertexFormat = VertexFormat.PositionRhw | VertexFormat.Diffuse;
      int color = Color.Firebrick.ToArgb();
      //ClipRectangle top = _clipStack.Peek();
      //Vertex[] vertices = new[] {
      //  new Vertex(rect.X + top.XOffset, rect.Y + top.YOffset, 0.0f, color),
      //  new Vertex(rect.X + rect.Width - 1 + top.XOffset, rect.Y + top.YOffset, 0.0f, color),

      //   new Vertex(rect.X + rect.Width - 1 + top.XOffset, 
      //      (float)rect.Y + top.YOffset, 0.0f,  color),
      //      new Vertex(rect.X + rect.Width - 1 + top.XOffset,
      //      (float)rect.Y + rect.Height - 1 + top.YOffset, 0.0f,  color),

      //      new Vertex(rect.X + rect.Width - 1 + top.XOffset, 
      //      (float)rect.Y + rect.Height - 1 + top.YOffset, 0.0f,  color),
      //      new Vertex(rect.X + top.XOffset,
      //      (float)rect.Y + rect.Height - 1 + top.YOffset, 0.0f,  color),

      //      new Vertex(rect.X + top.XOffset, 
      //      (float)rect.Y + rect.Height - 1 + top.YOffset, 0.0f,  color),
      //      new Vertex(rect.X + top.XOffset, 
      //      (float)rect.Y + top.YOffset, 0.0f,  color)
      //};
      Vertex[] vertices2 = new[] {
        new Vertex(rect.X, rect.Y, 0.0f, color),
        new Vertex(rect.X + rect.Width-1, rect.Y, 0.0f, color),

        new Vertex(rect.X + rect.Width - 1, 
            rect.Y, 0.0f,  color),
            new Vertex(rect.X + rect.Width - 1 ,
            (float)rect.Y + rect.Height - 1 , 0.0f,  color),

            new Vertex(rect.X + rect.Width - 1 , 
            (float)rect.Y + rect.Height - 1 , 0.0f,  color),
            new Vertex(rect.X ,
            (float)rect.Y + rect.Height - 1 , 0.0f,  color),

            new Vertex(rect.X , 
            (float)rect.Y + rect.Height - 1 , 0.0f,  color),
            new Vertex(rect.X , 
            rect.Y , 0.0f,  color)
      };

      //_device.DrawPrimitives(PrimitiveType.LineList, 0, 4);
      _device.DrawUserPrimitives(PrimitiveType.LineList, 4, vertices2);
      
    }


    internal void EndDraw()
    {
      PopClipArea();
    }
  }
}