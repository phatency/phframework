﻿using System;
using SlimDX.Direct3D9;
namespace UI
{
  class GraphicsManager : IDisposable
  {
    int _screenHeight = 600, _screenWidth = 800; bool _windowed = true;
    Direct3D _direct3D;
    Device _device;
    PhForm _renderForm;
    PresentParameters _presentParams;

    public PhForm Form
    {
      get { return _renderForm; }
    }

    public Device Device
    {
      get { return _device; }
    }

    /// <summary>
    /// Gets a value indicating whether the application is windowed.
    /// </summary>
    /// <value>
    /// <c>true</c> if the application is windowed; otherwise, <c>false</c>.
    /// </value>
    public bool Windowed
    {
      get { return _windowed; }
    }

    /// <summary>
    /// Gets the width of the screen.
    /// </summary>
    /// <value>The width of the screen.</value>
    public int ScreenWidth
    {
      get { return _screenWidth; }
    }

    /// <summary>
    /// Gets the height of the screen.
    /// </summary>
    /// <value>The height of the screen.</value>
    public int ScreenHeight
    {
      get { return _screenHeight; }
    }

    /// <summary>
    /// Gets the size of the screen.
    /// </summary>
    /// <value>The size of the screen.</value>
    public System.Drawing.Size ScreenSize
    {
      get { return new System.Drawing.Size(_screenWidth, _screenHeight); }
    }

    public float AspectRatio
    {
      get { return _screenWidth / (float)_screenHeight; }
    }


    public GraphicsManager()
    {

      _renderForm = new PhForm
                      {
                        ClientSize = ScreenSize,
                        Text = "Ph.Framework.Gui"
                      };
      BuildPresentParameters();
      _direct3D = new Direct3D();
      _device = new Device(_direct3D, 0, DeviceType.Hardware, _renderForm.Handle, CreateFlags.HardwareVertexProcessing, _presentParams);

    }

    public PresentParameters BuildPresentParameters()
    {
      _presentParams = new PresentParameters
        {
          BackBufferHeight = _renderForm.ClientRectangle.Height,
          BackBufferWidth = _renderForm.ClientRectangle.Width,
          DeviceWindowHandle = _renderForm.Handle,
          Windowed = _windowed
        };
      return _presentParams;
    }

    #region IDisposable Members

    public void Dispose()
    {
      _device.Dispose();
      _direct3D.Dispose();
      _renderForm.Dispose();
    }

    #endregion
  }

}
