﻿#define DEBUG
using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Text;
using System.Xml;
using Microsoft.CSharp;
using Ph;
using System.Collections.Generic;
using Ph.Scripting;
using Ph.Extensions;
using SlimDX;
using SlimDX.Direct3D9;

namespace UI
{
  public static class UIManager
  {
    public static Widget Top = new Widget(null) { Rectangle = new ClipRectangle(new Rectangle(0, 0, 800, 600), 0, 0)};


  }

  public static class UIHelper
  {
    public static ClipRectangle CreateWidgetClipRectangle(string x, string y, string width, string height, ClipRectangle parent)
    {
      int _x = GetSpatialValue(x, parent.Width);
      int _y = GetSpatialValue(y, parent.Height);
      int _w = GetSpatialValue(width, parent.Width);
      int _h = GetSpatialValue(height, parent.Height);
      ClipRectangle rect = new ClipRectangle(new Rectangle(_x, _y, _w, _h), _x + parent.XOffset, _y + parent.YOffset);
      Log.Debug("Rect:{0}", rect);
      return  rect;
      
    }

    public static int GetSpatialValue(string value, int relativeValue)
    {
      if (value == null)
        return 0;
      int percentIndex = value.IndexOf('%');
      if (percentIndex != -1)
        value = value.Substring(0, percentIndex);
      if (percentIndex != -1 || value.Contains("."))
      {
        float f;
        if (float.TryParse(value, out f))
          if (percentIndex != -1)
            return (int)(f * relativeValue * 0.01);
          else
            return (int)(f * relativeValue);
        Log.Error("Error parsing {0}", value);
      }
       
      int i;
      if (!int.TryParse(value, out i))
        return 0;
      return i;
    }

    public static bool TryGetPercentValue(string value, out float result)
    {
      result = 0.0f;
      if (value == null)
        return false;
      int percentIndex = value.IndexOf('%');
      if (percentIndex != -1)
        value = value.Substring(0, percentIndex);
      if (percentIndex == -1 && !value.Contains("."))
        return false;
      float f;
      if (float.TryParse(value, out f))
        if (percentIndex != -1)
          result = f * 0.01f;
        else
          result = f;
      else
        Log.Error("Error parsing {0}", value);
      return true; 
    }

    public static Color GetColor(string colorString)
    {
      if (colorString == null)
        return Color.Violet;
      colorString = colorString.Trim();

      if (colorString.StartsWith("#") || Char.IsLetter(colorString[0]))
      {
        return ColorTranslator.FromHtml(colorString);
        
        
      }
      return Color.Violet;
    }

    public static Color4 GetColor4(Color color, float opacity)
    {
      var c = new Color4(color);
      c.Alpha = opacity;
      return c;
    }

    internal static int GetColorArgb(string colorString, float opacity, string opacityString)
    {
      var c = GetColor4(GetColor(colorString), opacity);
      float opacity2 = GetSpatialValue(opacityString, 100) * 0.01f;
      c.Alpha *= opacity2;
      return c.ToArgb();
    }


  }


  public class UIParser : CodeDomHelper
  {


    List<string> _children = new List<string>();

    public UIParser()
    {

      var settings = new XmlReaderSettings { IgnoreWhitespace = true, IgnoreComments = true };
      CodeDomProvider provider = new CSharpCodeProvider();
      var ns = new CodeNamespace("UserUI");
      ns.Imports.Add(new CodeNamespaceImport("System"));
      ns.Imports.Add(new CodeNamespaceImport("UI"));

      using (var reader = XmlReader.Create("scripting\\UI\\uiTest.ui", settings))
      {
        reader.ReadToFollowing("UI");

        while (reader.Read())
          ReadTree(reader, ns, null);
        Log.Debug("{2}: {0} = {1}", reader.Name, reader.Value, reader.NodeType.ToString());

      }
      StringBuilder sbCode = new StringBuilder();
      StringWriter sw = new StringWriter(sbCode);
      CodeGeneratorOptions options = new CodeGeneratorOptions();
     // provider.GenerateCodeFromNamespace(ns, sw, options);
      Ph.Dbg.GenerateCodeFromNamespace(ns, sw, options);
      Console.Write(sbCode.ToString());
      using (TextWriter writer = new StreamWriter(new[] { "cache", "gui", "uitest.ui.cs" }.RelativePath()))
      {
        writer.Write(sbCode.ToString());
      }

      ScriptCompiler compiler = Services.ScriptManager.Compiler;
      CompileParameters p = new CompileParameters
      {
        AssemblyName = "UI.dll",
        Debug = false,
        References = new[] { "mscorlib.dll", "System.Drawing.dll", "Script:Gui", @"C:\Program Files\SlimDX SDK (March 2009)\Bin\x86\SlimDX.dll"},
        RelativePath = new[] { "cache", "gui" }.RelativePath(),
        SourceCode = new[] { "uitest.ui.cs" }
      };
      Assembly assembly;
      compiler.Compile(p, out assembly);
      if (assembly != null)
        foreach (string widgetTypeName in _children)
        {
          Widget widget = (Widget)assembly.CreateInstance("UserUI." + widgetTypeName, false,
            BindingFlags.CreateInstance, null, new[] { UIManager.Top }, null, null);
          if (widget == null)
            Log.Error("UIParser: Something went wrong when constructing widget {0}", widgetTypeName);
          UIManager.Top.Add(widget);
        }
    }



    public WidgetGenerationHelper ReadTree(XmlReader reader, CodeNamespace nameSpace, WidgetGenerationHelper parent)
    {
      if (reader.NodeType != XmlNodeType.Element)
        return null;
      WidgetGenerationHelper widget = new WidgetGenerationHelper();

      
      ReadProperties(reader, widget);
      string widgetType = reader.Name;


      if (parent == null)
        _children.Add(widget.Name);
      
      if (widgetType == "Frame")
        widget.Widget.BaseTypes.Add(new CodeTypeReference(typeof(Widget)));
      else
        Log.Debug("Unknown element {0}", reader.Name);
      

      while (reader.Read() && (reader.Name != widgetType || reader.NodeType != XmlNodeType.EndElement))
        switch (reader.Name)
        {
          case "Size":
            AddSize(reader, widget);
            break;
          case "Frame":
            WidgetGenerationHelper child = ReadTree(reader, nameSpace, widget);
            AddChildWidget(child.Name, widget);
            break;
          case "Border":
            AddBorder(reader, widget);
            break;
          case "Rectangle":
            AddRectangle(reader, widget);
            break;
        }
      AddGraphics(widget);
      if (widget.ResetGraphics.Statements.Count > 0)
      {
        widget.Widget.Members.Add(widget.MoveGraphics);
        widget.Constructor.Statements.Add(ThisInvoke("ResetGraphics"));
        widget.Widget.Members.Add(widget.ResetGraphics);
      }
      if (widget.Draw.Statements.Count > 0)
      {
        widget.Widget.Members.Add(widget.Draw);
      }
      nameSpace.Types.Add(widget.Widget);
      return widget;
    }
    private void AddGraphics(WidgetGenerationHelper widget)
    {

      var graphics2D = new CodeVariableReferenceExpression("graphics2D");
      var device = Property(graphics2D, "Device");

      Func<string, CodeIterationStatement> AddOffsets = (fieldName) =>
                        {
                          var field = ThisField(fieldName);

                          var loop = LoopArray(field);
                          var vertex = new CodeArrayIndexerExpression(field, MakeExpression(Variable("i")));
                          var position = Property(vertex, "PositionRhw");
                          var x = Property(position, "X");
                          var y = Property(position, "Y");
                          loop.Statements.Add(new CodeAssignStatement(x, Add(x, Property(widget.Rectangle, "XOffset"))));
                          loop.Statements.Add(new CodeAssignStatement(y, Add(y, Property(widget.Rectangle, "YOffset"))));
                          return loop;
                        };
      Action<List<CodeObjectCreateExpression>, string, string, int> AddDrawingAndResetCode =
        (vertexList, fieldName, enumName, verticesPerPrimitive) =>
        {
          var arrayDeclaration = new CodeMemberField(typeof(Vertex[]), fieldName);
          widget.Widget.Members.Add(arrayDeclaration);
          widget.ResetGraphics.Statements.Add(AssignToField(fieldName, new CodeArrayCreateExpression(typeof(Vertex), vertexList.ToArray())));
          var enumReference = new CodeFieldReferenceExpression(new CodeTypeReferenceExpression(typeof(PrimitiveType)), enumName);
          widget.Draw.Statements.Add(Invoke(device, "DrawUserPrimitives", enumReference, vertexList.Count / verticesPerPrimitive, ThisField(fieldName)));
          widget.ResetGraphics.Statements.Add(AddOffsets(fieldName));
        };
      if (widget.LineList.Count != 0)
        AddDrawingAndResetCode(widget.LineList, "_lineList", "LineList", 2);
      if (widget.TriangleList.Count != 0)
        AddDrawingAndResetCode(widget.TriangleList, "_triangleList", "TriangleList", 3);
    }

    private void AddRectangle(XmlReader reader, WidgetGenerationHelper widget)
    {
      Func<string, CodePropertyReferenceExpression, object> ca = (valueName,  relativeTo) =>
      {
        string valueString = reader[valueName];
        float percentValue;
        if (valueString == null)
          return new CodePrimitiveExpression(0);
        if (UIHelper.TryGetPercentValue(valueString, out percentValue))
          return new CodeCastExpression(typeof(int), // (int) percentValue * relativeTo
            Multiply(percentValue, relativeTo));
        int absoluteValue;
        if(int.TryParse(valueString, out absoluteValue))
          return new CodePrimitiveExpression(absoluteValue);
        throw new Exception(); // todo: better exceptions

      };
       
      var x = ca("x", widget.Width);
      var y = ca("y", widget.Height);
      var w = ca("width", widget.Width);
      var h = ca("height", widget.Height);
      AddRectangle(x, y, w, h, RectangleStyle.Border, reader,  widget);
      
    }

    enum RectangleStyle
    {
      Border,
      Solid
    };

    private void AddRectangle(object x, object y, object w, object h, CodeExpression color, RectangleStyle style, WidgetGenerationHelper widget)
    {
      var dim = MakeExpression(new[] {x,y,w,h});
      string[] variableNames = new [] {"x", "y", "xw", "yh"};
      CodeExpression[] vars = new CodeExpression[4];

      for (int i = 0; i < 4; i++)
      {
        string variableName = GetVariableName(variableNames[i]);
        var variableDeclaration = new CodeVariableDeclarationStatement(typeof(float), variableName, dim[i]);
        vars[i] = Variable(variableName);
        widget.ResetGraphics.Statements.Add(variableDeclaration);
      }

      if (style == RectangleStyle.Border)
      AddLines(widget, true, new[] { vars[0], vars[1], color }, new[] { vars[2], vars[1], color },
        new[] { vars[2], vars[3], color }, new[] { vars[0], vars[3], color });
      if (style == RectangleStyle.Solid)
      {
        AddTriangles(widget, new int[] {0, 1, 3, 1, 2, 3}, new[]{ new[] {vars[0], vars[1], color}, new[] {vars[2], vars[1], color},
                     new[] {vars[2], vars[3], color}, new[] {vars[0], vars[3], color}});
      }
    }

    private void AddRectangle(object x, object y, object w, object h, RectangleStyle style, XmlReader reader, WidgetGenerationHelper widget)
    {
      CodeExpression createColor4 = GetColorExpression(reader["color"], reader["opacity"], widget.Opacity);
      string colorName = GetVariableName("color");
      var colorVariable = new CodeVariableDeclarationStatement(typeof(int), colorName, Invoke(createColor4, "ToArgb"));


      widget.ResetGraphics.Statements.Add(colorVariable);

      AddRectangle(x, y, w, h, Variable(colorName), style, widget);
    }

    private void AddLines(WidgetGenerationHelper widget, bool connectHeads, params CodeExpression[][] lines)
    {
      for (int i = 1; i < lines.Length; ++i)
      {
        object x1 = lines[i - 1][0], y1 = lines[i - 1][1], x2 = lines[i][0], y2 = lines[i][1];
        object c1 = lines[i - 1][2], c2 = lines[i][2];
        widget.LineList.Add(CreateVertex(x1, y1, 0.0f, c1));
        widget.LineList.Add(CreateVertex(x2, y2, 0.0f, c2));
      }
      if (connectHeads)
      {
        object x1 = lines[0][0], y1 = lines[0][1], x2 = lines[lines.Length - 1][0], y2 = lines[lines.Length - 1][1];
        object c1 = lines[0][2], c2 = lines[lines.Length - 1][2];
        widget.LineList.Add(CreateVertex(x1, y1, 0.0f, c1));
        widget.LineList.Add(CreateVertex(x2, y2, 0.0f, c2));
      }
    }

    private void AddTriangles(WidgetGenerationHelper widget, int[] indices,  CodeExpression[][] points)
    {
      //Log.Warn("Trying to add triangles");
      //for (int iter = 0; iter < indices.Length; ++iter)
      {
        //int i = indices[iter];
        //widget.Constructor.Statements.Add(Debug("i:{0}", MakeExpression(i)));
        //object x = points[i][0], y = points[i][1], c = points[i][2];
        //widget.TriangleList.Add(CreateVertex(x, y, 0.0f, c));
      }
    }

    private void AddBorder(XmlReader reader, WidgetGenerationHelper widget)
    {
      string style = reader["style"];
      if (style == null)
        return; // todo: xml-warning'

      AddRectangle(0F, 0F, widget.Width, widget.Height, RectangleStyle.Border, reader, widget);
    }

    private CodeObjectCreateExpression CreateVertex(object x, object y, object z, object color)
    {
      return New(typeof (Vertex), x, y, z, color);
    }

    private void ReadProperties(XmlReader reader, WidgetGenerationHelper widget)
    {
      widget.Name = reader["name"]; // todo: xsd makes sure name-attribute is always available.
   
      bool visible;
      if (reader.TryGetAttributeAsBoolean("visible", out visible))
        widget.Constructor.Statements.Add(AssignToField("_visible", visible));
      string opacityString = reader["opacity"];
      float opacity = 1.0f;
      if (opacityString != null)
        opacity = 0.01f * UIHelper.GetSpatialValue(opacityString, 100);
      var setOpacity = new CodeAssignStatement();
      var parentOpacity = Property(new CodeVariableReferenceExpression("parent"), "Opacity");
      setOpacity.Left = widget.Opacity;
      setOpacity.Right = Multiply(parentOpacity, opacity);
      widget.Constructor.Statements.Add(setOpacity);
    }



    private void AddChildWidget(string childName, WidgetGenerationHelper widget)
    {
      var childObject = New(childName,widget.Self);
      var addChild = ThisInvoke("Add", childObject);
      widget.Constructor.Statements.Add(addChild);
    }

    private void AddSize(XmlReader reader, WidgetGenerationHelper widget)
    {
       
      var setDimensions = new CodeAssignStatement {Left = widget.Rectangle};
      string xs = reader["x"];
      string ys = reader["y"];
      string widths = reader["width"];
      string heights = reader["height"];

      var parentRectangle = Property(widget.Parent, "Rectangle");
    
      setDimensions.Right = Invoke("UIHelper.CreateWidgetClipRectangle", xs, ys, widths, heights, parentRectangle);
      widget.Constructor.Statements.Add(setDimensions);
      

    }

    internal CodeExpression GetColorExpression(string colorString, string opacityString, CodeFieldReferenceExpression opacityField)
    {
      Color4 color4 = new Color4(UIHelper.GetColor(colorString));
      float opacity = opacityString != null ? UIHelper.GetSpatialValue(opacityString, 100) * 0.01f : 1.0f;
      var opacityExpression = Multiply(opacity,opacityField);
      return New(typeof(Color4), opacityExpression, color4.Red, color4.Green, color4.Blue);
    }

  }

}