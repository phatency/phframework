﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
namespace UI
{
  enum BindingCallbackType
  {
    ConsoleCommand
  }

  enum KeyType
  {
    KeyDown,
    KeyUp
  }

  class BindingCallback
  {
    public BindingCallbackType Type;
    public string Param;
    public bool Toggleable;

    public BindingCallback(BindingCallbackType type, bool toggleable, string param)
    {
      Toggleable = toggleable;
      Type = type;
      Param = param;
    }
  }

  struct InputBinding : IComparable<InputBinding>, IEqualityComparer<InputBinding>
  {
    readonly bool _ctrl, _alt, _shift;
    readonly Keys _key;

    public InputBinding(KeyEventArgs keyArgs)
    {
      _ctrl = keyArgs.Control;
      _alt = keyArgs.Alt;
      _shift = keyArgs.Shift;
      _key = keyArgs.KeyCode;
      //Console.WriteLine("{3} s:{0} c:{1} a:{2}, d:{4}, mod:{5}", _shift, _ctrl, _alt, keyArgs.KeyCode, (int)keyArgs.KeyData, keyArgs.Modifiers);
    }

    public int CompareTo(InputBinding other)
    {
      int result = _key.CompareTo(other._key);
      result += _ctrl.CompareTo(other._ctrl) << 4;
      result += _alt.CompareTo(other._alt) << 5;
      result += _shift.CompareTo(other._shift) << 6;
      return result;
    }

    #region IEqualityComparer<InputBinding> Members

    public bool Equals(InputBinding x, InputBinding y)
    {
      return (x._shift == y._shift && x._alt == y._alt && x._ctrl == y._ctrl && x._key == y._key);
    }

    public int GetHashCode(InputBinding obj)
    {
      int result = 0;
      result += Convert.ToInt32(_ctrl) << 0;
      result += Convert.ToInt32(_alt) << 1;
      result += Convert.ToInt32(_shift) << 2;
      result += (int)obj._key << 4;
      return result;
    }

    #endregion
  }

  public interface IScreenArea
  {
    bool IsInsideArea(SlimDX.Vector2 point);
  }

  public class ScreenRectangle : IScreenArea
  {
    private readonly float _x, _y, _x2, _y2;

    public ScreenRectangle(float x, float y, float x2, float y2)
    {
      _x = x;
      _y = y;
      _x2 = x2;
      _y2 = y2;
    }

    #region IScreenArea Members
    public bool IsInsideArea(SlimDX.Vector2 point)
    {
      return (point.X >= _x && point.X <= _x2 && point.Y >= _y && point.Y <= _y2);
    }
    #endregion
  }


  class MouseInputManager
  {
    class MouseInputProfile
    {
      List<KeyValuePair<IScreenArea, MouseBinding>> areas = new List<KeyValuePair<IScreenArea, MouseBinding>>();


      public bool IsBindedArea(SlimDX.Vector2 coord, out MouseBinding callback)
      {

        foreach (KeyValuePair<IScreenArea, MouseBinding> area in areas)
        {
          if (area.Key.IsInsideArea(coord))
          {
            callback = area.Value;
            return true;
          }
        }
        callback = null;
        return false;
      }

      public void RegisterCallback(IScreenArea area, MouseBinding binding)
      {
        areas.Add(new KeyValuePair<IScreenArea, MouseBinding>(area, binding));
      }

    }

    [Flags]
    public enum MouseCallbackType
    {
      LeftButton = 0x1,
      RightButton = 0x2,
      MouseOver = 0x4,
      MouseEnter = 0x8,
      MouseLeave = 0x10,
      MiddleButton = 0x20
    }

    class MouseBinding
    {
      public MouseCallbackType Type;
      public BindingCallback Callback;
      public MouseBinding(BindingCallback callback, MouseCallbackType type)
      {
        Type = type;
        Callback = callback;
      }
    }

    Dictionary<string, MouseInputProfile> profiles = new Dictionary<string, MouseInputProfile>();
    Form form;

    public void RegisterCallback(string profileName, IScreenArea area, BindingCallback callback, MouseCallbackType type)
    {
      MouseInputProfile profile;
      if (!profiles.TryGetValue(profileName, out profile))
      {
        profile = new MouseInputProfile();
        profiles.Add(profileName, profile);
      }
      profile.RegisterCallback(area, new MouseBinding(callback, type));
    }

    void form_MouseWheel(object sender, MouseEventArgs e)
    {

    }

    void form_MouseEnter(object sender, EventArgs e)
    {

    }

    void form_MouseUp(object sender, MouseEventArgs e)
    {

    }

    public MouseInputManager(PhForm form)
    {
      form.MouseWheel += new MouseEventHandler(form_MouseWheel);
      form.MouseEnter += new EventHandler(form_MouseEnter);
      form.MouseUp += new MouseEventHandler(form_MouseUp);
      form.MouseDown += new MouseEventHandler(form_MouseDown);
      RegisterCallback("main", new ScreenRectangle(0, 0, 10, 10), new BindingCallback(BindingCallbackType.ConsoleCommand, false, "mouse_top_Left"), MouseCallbackType.LeftButton);
      this.form = form;
    }

    void form_MouseDown(object sender, MouseEventArgs e)
    {
      MouseBinding callback;
      if (IsBindedArea(e.Location, out callback))
      {
        if (IsButtonPressed(callback.Type, e.Button))
          Console.WriteLine("Calling {0}", callback);
      }
    }

    private bool IsButtonPressed(MouseCallbackType mouseCallbackType, MouseButtons mouseButtons)
    {
      switch (mouseButtons)
      {
        case MouseButtons.Left:
          if ((mouseCallbackType & MouseCallbackType.LeftButton) == MouseCallbackType.LeftButton)
            return true;
          break;
        case MouseButtons.Right:
          if ((mouseCallbackType & MouseCallbackType.RightButton) == MouseCallbackType.RightButton)
            return true;
          break;
        case MouseButtons.Middle:
          if ((mouseCallbackType & MouseCallbackType.MiddleButton) == MouseCallbackType.MiddleButton)
            return true;
          break;
      }
      return false;
    }

    bool IsBindedArea(Point point, out MouseBinding callback)
    {
      SlimDX.Vector2 coord = ScreenToVirtualCoordinates(point);
      foreach (MouseInputProfile profile in profiles.Values)
        if (profile.IsBindedArea(coord, out callback))
          return true;
      callback = null;
      return false;
    }

    public SlimDX.Vector2 ScreenToVirtualCoordinates(Point coordinates)
    {
      float yFactor = form.Height / 100.0f; // TODO: Get these values from somewhere else
      float xFactor = form.Width / 100.0f;

      return new SlimDX.Vector2(coordinates.X / xFactor, coordinates.Y / yFactor);
    }
  }


  class InputManager
  {
    public delegate void UnicodeInputHandler(char input);
    private UnicodeInputHandler unicodeInputTHING = null;

    private Dictionary<InputBinding, BindingCallback> bindings;
    private Dictionary<Keys, BindingCallback> toggledKeys;

    List<KeyValuePair<IScreenArea, BindingCallback>> mouseAreas;
    private MouseInputManager mouse;
    PhForm form;
    bool unicodeInput;

    public InputManager(PhForm form)
    {
      this.form = form;
      form.KeyPreview = true;
      form.MouseDown += new MouseEventHandler(form_MouseDown);
      form.MouseMove += new MouseEventHandler(form_MouseMove);
      mouse = new MouseInputManager(form);
      form.KeyDown += new KeyEventHandler(form_KeyDown);
      form.KeyUp += new KeyEventHandler(form_KeyUp);
      form.KeyPress += new KeyPressEventHandler(form_KeyPress);
      form.Select();

      bindings = new Dictionary<InputBinding, BindingCallback>();
      bindings.Add(new InputBinding(new KeyEventArgs(Keys.A | Keys.Control)), new BindingCallback(BindingCallbackType.ConsoleCommand, true, "hello"));

      toggledKeys = new Dictionary<Keys, BindingCallback>();

      mouseAreas = new List<KeyValuePair<IScreenArea, BindingCallback>>();
      //mouseAreas.Add(MakePair(new ScreenRectangle(0.0f, 0.0f, 10.0f, 10.0f) as IScreenArea, new BindingCallback(BindingCallbackType.ConsoleCommand, false, "mouse_topleft_corner")));
    }

    void form_MouseMove(object sender, MouseEventArgs e)
    {
      form.Debug(e);
    }

    private KeyValuePair<T, U> MakePair<T, U>(T key, U val)
    {
      return new KeyValuePair<T, U>(key, val);
    }

    void form_KeyPress(object sender, KeyPressEventArgs e)
    {

      if (!unicodeInput)
        return;
      if (unicodeInputTHING == null)
        Console.WriteLine(e.KeyChar);
      else
        unicodeInputTHING(e.KeyChar);

      e.Handled = true;
    }

    void form_KeyUp(object sender, KeyEventArgs e)
    {
      if (unicodeInput)
        return;
      new InputBinding(e);
      BindingCallback callback;
      if (toggledKeys.TryGetValue(e.KeyCode, out callback))
      {
        Call(callback, KeyType.KeyUp);
        toggledKeys.Remove(e.KeyCode);
      }
      e.Handled = true;
    }

    void form_KeyDown(object sender, KeyEventArgs e)
    {
      if (unicodeInput)
        return;

      var input = new InputBinding(e);
      BindingCallback callback;
      if (bindings.TryGetValue(input, out callback))
      {
        Call(callback, KeyType.KeyDown);
        if (callback.Toggleable)
          toggledKeys.Add(e.KeyCode, callback);
      }

      e.Handled = true;
    }

    private void Call(BindingCallback callback, KeyType keyType)
    {
      if (keyType == KeyType.KeyDown)
      {
        if (callback.Toggleable == true)
          Console.WriteLine("Calling +{0}", callback.Param);
        else
          Console.WriteLine("Calling {0}", callback.Param);
      }
      else if (keyType == KeyType.KeyUp && callback.Toggleable == true)
        Console.WriteLine("Calling -{0}", callback.Param);
      else
        Console.WriteLine("Calling nothing");
    }



    void form_MouseDown(object sender, MouseEventArgs e)
    {
      BindingCallback callback;
      if (IsBindedArea(e.Location, out callback))
      {
        Console.WriteLine("Calling {0}", callback);
      }

    }

    public SlimDX.Vector2 ScreenToVirtualCoordinates(Point coordinates)
    {
      float yFactor = form.Height / 100.0f;
      float xFactor = form.Width / 100.0f;

      return new SlimDX.Vector2(coordinates.X / xFactor, coordinates.Y / yFactor);
    }

    private bool IsBindedArea(Point point, out BindingCallback callback)
    {
      SlimDX.Vector2 coord = ScreenToVirtualCoordinates(point);
      foreach (KeyValuePair<IScreenArea, BindingCallback> area in mouseAreas)
      {
        if (area.Key.IsInsideArea(coord))
        {
          callback = area.Value;
          return true;
        }
      }
      callback = null;
      return false;
    }

    public void CaptureUnicodeInput(UnicodeInputHandler handler)
    {
      unicodeInputTHING = handler;
      unicodeInput = true;
      form.KeyRepeat = false;
    }

    public void ReleaseUnicodeInput(UnicodeInputHandler handler)
    {
      if (handler != unicodeInputTHING)
        throw new Exception("Tried to release a unicode input handler which, frankly, doesn't exist");

      unicodeInputTHING = null;
      unicodeInput = false;

    }

  }
}