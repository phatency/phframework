﻿using System;
using System.Drawing;

/// <summary>
/// A rectangle used when dealing with clipping. 
/// A clip rectangle uses System.Drawing.Rectangle internally, so there is no conversion between the two involved. 
/// ClipRectangle just adds two offset variables, which are used for calculations from relative screen coordinates to actual screen coordinates.
/// </summary>
namespace UI
{
public struct ClipRectangle
{
  Rectangle _rectangle;
  public int XOffset, YOffset;

  public int X
  {
    get { return _rectangle.X; }
    set { _rectangle.X = value; }
  }

  public int Y
  {
    get { return _rectangle.Y; }
    set { _rectangle.Y = value; }
  }

  public int Width
  {
    get { return _rectangle.Width; }
    set { _rectangle.Width = value; }
  }

  public int Height
  {
    get { return _rectangle.Height; }
    set { _rectangle.Height = value; }
  }

  public Rectangle Rectangle
  {
    get { return _rectangle; }
    set { _rectangle = value;}
  }

  /// <summary>
  /// Construct a ClipRectangle specifying offsets that are <b>already applied to it</b>
  /// </summary>
  /// <param name="rectangle"></param>
  /// <param name="xOffset">The offset of the x coordinate. Used for calculating the actual screen coordinate from the relative screen coordinate.</param>
  /// <param name="yOffset">The offset of the y coordinate. Used for calculating the actual screen coordinate from the relative screen coordinate.</param>
  public ClipRectangle(Rectangle rectangle, int xOffset, int yOffset)
  {
    _rectangle = rectangle;
    XOffset = xOffset;
    YOffset = yOffset;
  }

  public void AddOffset(int xOffset, int yOffset)
  {
    XOffset += xOffset;
    YOffset += yOffset;
    _rectangle.X += xOffset;
    _rectangle.Y += yOffset;
  }

  internal void Intersect(ClipRectangle carea)
  {
    _rectangle.Intersect(carea._rectangle);
  }

  public static explicit operator ClipRectangle(Rectangle rectangle)
  {
    return new ClipRectangle(rectangle,0,0);
  }

  public static explicit operator Rectangle(ClipRectangle clipRectangle)
  {
    return clipRectangle.Rectangle;
  }

  public override string ToString()
  {
    return String.Format("x:{0}, y:{1}, h:{2}, w:{3} xO:{4}, yO:{5}", X, Y, Width, Height, XOffset, YOffset);
  }
}

static class RectangleExtensions
{
  public static bool IsNegative(this Rectangle rect)
  {
    if (rect.X < 0 || rect.Y < 0 || rect.Width < 0 || rect.Height < 0)
      return true;
    return false;
  }
}
}