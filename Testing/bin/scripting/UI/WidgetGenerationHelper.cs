﻿
using System;
using System.CodeDom;
using System.Collections.Generic;


namespace UI
{
  public class WidgetGenerationHelper : CodeDomHelper
  {
    private string _name;
    private CodeConstructor _constructor = null;
    public string Name
    {
      get { return _name; }
      set
      {
        Widget.Name = value;
        _name = value;
        Constructor.Name = value;
      }
    }

    public CodeConstructor Constructor
    {
      get { return _constructor; }

    }

    public CodeTypeDeclaration Widget;
    public CodeFieldReferenceExpression Parent;
    public CodeMemberMethod MoveGraphics;
    public CodeFieldReferenceExpression Rectangle;
    public CodePropertyReferenceExpression Width;
    public CodePropertyReferenceExpression Height;
    public CodeThisReferenceExpression Self;
    public CodeMemberMethod Draw;
    public CodeMemberMethod ResetGraphics;
    public CodeFieldReferenceExpression Opacity;
    public List<CodeObjectCreateExpression> LineList = new List<CodeObjectCreateExpression>();
    public List<CodeObjectCreateExpression> TriangleList = new List<CodeObjectCreateExpression>();

    public WidgetGenerationHelper()
    {
      Self = new CodeThisReferenceExpression();
      Widget = new CodeTypeDeclaration {IsClass = true, Attributes = MemberAttributes.Public};
      Opacity = ThisField("_opacity");
      Parent = ThisField("_parent");
      _constructor = new CodeConstructor { Attributes = MemberAttributes.Public };
      Constructor.Parameters.Add(new CodeParameterDeclarationExpression(typeof(Widget), "parent"));
      Constructor.BaseConstructorArgs.Add(new CodeVariableReferenceExpression("parent"));
      Widget.Members.Add(Constructor);

      MoveGraphics = new CodeMemberMethod { Attributes = MemberAttributes.Override | MemberAttributes.Family, Name = "MoveGraphics" };
      MoveGraphics.Parameters.Add(new CodeParameterDeclarationExpression(typeof(int), "xOffset"));
      MoveGraphics.Parameters.Add(new CodeParameterDeclarationExpression(typeof(int), "yOffset"));
      ResetGraphics = new CodeMemberMethod { Attributes = MemberAttributes.Public, Name = "ResetGraphics" };

      Draw = new CodeMemberMethod { Attributes = MemberAttributes.Override | MemberAttributes.Family, Name = "Draw" };
      Draw.Parameters.Add(new CodeParameterDeclarationExpression(typeof(Graphics2D), "graphics2D"));

      Rectangle = ThisField("_rectangle");
      Height = Property(Rectangle, "Height");
      Width = Property(Rectangle, "Width");
    }
  }
}
