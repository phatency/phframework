﻿using System;
using System.CodeDom;
using System.Collections.Generic;

namespace UI
{

  public class CodeDomHelper
  {
    protected CodeBinaryOperatorExpression Multiply(object left, object right)
    {
      return BinaryExpression(left, CodeBinaryOperatorType.Multiply, right);
    }

    protected CodeBinaryOperatorExpression Add(object left, object right)
    {
      return BinaryExpression(left, CodeBinaryOperatorType.Add, right);
    }

    protected CodeBinaryOperatorExpression LessThan(object left, object right)
    {
      return BinaryExpression(left, CodeBinaryOperatorType.LessThan, right);
    }

    protected CodeBinaryOperatorExpression BinaryExpression(object left, CodeBinaryOperatorType type, object right)
    {
      CodeExpression l = MakeExpression(left);
      CodeExpression r = MakeExpression(right);
      return new CodeBinaryOperatorExpression(l, type, r);
    }

    protected CodePropertyReferenceExpression Property(CodeExpression targetObject, string property)
    {
      return new CodePropertyReferenceExpression(targetObject, property);
    }

    protected CodeFieldReferenceExpression Field(CodeExpression targetObject, string field)
    {
      return new CodeFieldReferenceExpression(targetObject, field);
    }

    protected CodeFieldReferenceExpression ThisField(string field)
    {
      return new CodeFieldReferenceExpression(new CodeThisReferenceExpression(), field);
    }

    protected CodeExpression[] MakeExpression(object[] values)
    {
      CodeExpression[] expressions = new CodeExpression[values.Length];
      for (int i = 0; i < values.Length; i++)
        expressions[i] = MakeExpression(values[i]);
      return expressions;
    }

    protected CodeExpression MakeExpression(object value)
    {
      CodeExpression exp = value as CodeExpression;
      if (exp != null)
        return exp;
      return new CodePrimitiveExpression(value);
    }

    protected CodeObjectCreateExpression New(Type objectType, params object[] parameters)
    {
      return new CodeObjectCreateExpression(objectType, CreateParameters(parameters));
    }

    protected CodeObjectCreateExpression New(string typeName, params object[] parameters)
    {
      return new CodeObjectCreateExpression(typeName, CreateParameters(parameters));
    }

    protected CodeVariableReferenceExpression Variable(string variableName)
    {
      return new CodeVariableReferenceExpression(variableName);
    }

    protected CodeExpression[] CreateParameters(params object[] parameters)
    {
      CodeExpression[] primitiveExpressions = new CodeExpression[parameters.Length];
      for (int i = 0; i < parameters.Length; i++)
        primitiveExpressions[i] = MakeExpression(parameters[i]);
      return primitiveExpressions;
    }

    protected CodeMethodInvokeExpression ThisInvoke(string methodName, params object[] parameters)
    {
      return new CodeMethodInvokeExpression(new CodeThisReferenceExpression(), methodName, CreateParameters(parameters));
    }

    protected CodeMethodInvokeExpression Invoke(string methodName, params object[] parameters)
    {
      return new CodeMethodInvokeExpression(null, methodName, CreateParameters(parameters));
    }

    protected CodeMethodInvokeExpression Invoke(CodeExpression targetObject, string methodName, params object[] parameters)
    {
      return new CodeMethodInvokeExpression(targetObject, methodName, CreateParameters(parameters));
    }


    protected CodeExpression Debug(params object[] parameters)
    {
      return Invoke("System.Console.WriteLine", CreateParameters(parameters)); ;
    }

    protected CodeAssignStatement AssignToField(string field, object value)
    {
      var statement = new CodeAssignStatement();
      statement.Left = new CodeFieldReferenceExpression(null, field);
      statement.Right = MakeExpression(value);
      return statement;
    }


    protected CodeIterationStatement LoopArray(CodeExpression array)
    {
      var loop = new CodeIterationStatement();
      var i = Variable("i");
      loop.InitStatement = NewPrimitive("i", 0); // for (i = 0;
      loop.TestExpression = LessThan(i, Property(array, "Length"));
      loop.IncrementStatement = new CodeAssignStatement(i, Add(i, 1)); // i++
      return loop;

    }

    protected CodeVariableDeclarationStatement NewPrimitive(string variableName, object value)
    {
      var declaration = new CodeVariableDeclarationStatement(value.GetType(), variableName)
                          {
                            InitExpression = new CodePrimitiveExpression(value)
                          };
      return declaration;
    }


    static Dictionary<string, int> _usedNames = new Dictionary<string, int>();
    protected string GetVariableName(string prefix)
    {
      int num = 0;
      if (_usedNames.TryGetValue(prefix, out num))
        _usedNames[prefix] = ++num;
      else
        _usedNames.Add(prefix, 0);
      if (num == 0)
        return prefix;
      return prefix + num;
    }
  }
}
