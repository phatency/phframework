﻿using System.Collections.Generic;
using System.Drawing;
using Ph.Extensions.ICollection;

namespace UI
{
  public class Widget
  {
    protected ClipRectangle _rectangle;
    public ClipRectangle Rectangle { get { return _rectangle; } set { _rectangle = value; } }
    protected List<Widget> _children = null;
    protected Widget _parent = null;
    protected bool _visible = true;
    protected float _opacity = 1.0f;

    public float Opacity { get { return _opacity; } } 
      public bool Visible { get { return _visible; } } 

    public virtual bool Movable { get { return false; } }

      public bool HasChildren
    {
      get { return _children == null ? false : !_children.IsEmpty<Widget>(); }
    }

    public Widget(Widget parent)
    {
      _parent = parent;
      
    }  

    public void Add(Widget widget)
    {
      if (_children == null)
        _children = new List<Widget>();
      _children.Add(widget);
    } 


    public bool Remove(Widget widget)
    {
      return _children.Remove(widget);
    } 

    public void MoveToBottom(Widget widget)
    {
      _children.Remove(widget);
      _children.Insert(0, widget);
    }

    internal void _Draw(Graphics2D graphics2D)
    {
      graphics2D.PushClipArea(_rectangle);
      //graphics2D.ClipStackDebug();
      Draw(graphics2D);

      if (this.HasChildren)
        foreach (Widget child in _children)
          if (child._visible)
            child._Draw(graphics2D);
      graphics2D.PopClipArea();
    }

    protected virtual void MoveGraphics(int offsetX, int offsetY)
    {

    }

    protected virtual void Draw(Graphics2D graphics2D)
    {

      graphics2D.DrawRectangle(new Rectangle(0, 0, _rectangle.Width, _rectangle.Height));
    }
  }

  public abstract class Window : Widget, IMovable
  {
    protected Window(Widget parent)
      : base(parent)
    {
      ResetGraphics();
      
    }

    public void Move(int x, int y)
    {
      //if (this.HasChildren)
      //  foreach (Widget child in _children)
      //    if (child.Visible)
    }

    abstract protected void ResetGraphics();

    public override bool Movable { get { return true; } }
  }

  public interface IMovable
  {
    void Move(int x, int y);
    bool Movable { get; }
  }

}