﻿#define DEBUG
using System;
using System.Text;
using Ph.Collections;
using Ph.Extensions.String;
using Ph.Scripting;
using ScriptBindings;

public class CommandLine : ScriptEntryPoint
{
  public override void Init()
  {
    Ph.Log.Debug("EntryPoint.Init()");
    Ph.Services.ScriptManager.RegisterScriptTypeHandler(new ConsoleCommandHandler());
  }
}


class ConsoleCommandHandler : ScriptTypeFactoryHandler<SingleFactory<ConsoleCommand>, ConsoleCommand, ConsoleCommandDefinition>, ICommandLine
{
  public override string ScriptType
  {
    get { return "ConsoleCommand"; }
  }
  public void Execute(string commandName, string parameters)
  {
    if (commandName == "help")
    {
      HelpCommand(parameters);
      return;
    }
    else if (commandName == "commands")
    {
      CommandList();
      return;
    }
    ConsoleCommand cmd;
    if (!instanceFactory.TryCreate(commandName, out cmd))
    {
      Ph.Log.Error("Unknown command \"{0}\"", commandName);
      return;
    }
    if (parameters == null && (cmd.ExpectedParameterNum == ParameterCount.None || cmd.ExpectedParameterNum == ParameterCount.Variable))
    {
      cmd.Execute();
      return;
    }

    if (cmd.ExpectedParameterNum == ParameterCount.SingleFullString)
    {
      cmd.Execute(parameters);

      return;
    }

    if (parameters == null)
    {
      Console.WriteLine("This command requires {0} parameter(s).", cmd.ExpectedParameterNum);
      return;
    }
    string[] splittedParameters = parameters.Split(' ');
    if (cmd.ExpectedParameterNum != ParameterCount.Variable && cmd.ExpectedParameterNum != (ParameterCount)splittedParameters.Length)
    {
      Console.WriteLine("Invalid number of parameters. Got {0}, expected {1}", splittedParameters.Length, cmd.ExpectedParameterNum);
      return;
    }
    else
      cmd.Execute(splittedParameters);

  }

  private void HelpCommand(string parameters)
  {
    if (parameters == null)
      GetHelpFor("help");
    else
      GetHelpFor(parameters);
  }

  private void GetHelpFor(string command)
  {
    ConsoleCommandDefinition def;
    if (!definitions.TryGetValue(command, out def))
    {
      Console.WriteLine("help: Unknown command: {0}", command);
      return;
    }
    StringBuilder help = new StringBuilder();
    if (def.Synopsis != null)
    {
      help.Append("Synopsis: ");
      help.AppendLine(def.Synopsis);

    }
    if (def.Usage != null)
    {
      help.Append("usage: ");
      help.AppendLine(def.Usage);
    }
    if (def.Help != null)
      help.AppendLine(def.Help);

    Console.WriteLine(help.ToString());
  }

  private void CommandList()
  {
    StringBuilder commandList = new StringBuilder();
    int count = 0;
    foreach (string command in definitions.Keys)
    {
      commandList.Append(command);
      if (++count != definitions.Count)
        commandList.Append(", ");
      else
        commandList.Append('.');

    }
    Console.WriteLine(commandList.ToString());
  }

  public void Execute(string line)
  {
    if (line.IsEmptyOrNull())
      return;
    int spaceLoc = line.IndexOf(' ');
    if (spaceLoc == -1 || line.Length - 1 == spaceLoc)
    {
      Execute(line.Trim(), null);
      return;
    }
    string command = line.Substring(0, spaceLoc++);
    string parameters = line.Substring(spaceLoc, line.Length - spaceLoc);
    Execute(command, parameters);


  }

}