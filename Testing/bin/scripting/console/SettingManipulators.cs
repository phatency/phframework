using System;
using ScriptBindings;

class SetCommand : ConsoleCommand
{
	public override void Execute(params string[] parameters)
	{
		Ph.Settings.SetString(parameters[0], parameters[1]);
	}
	
	public override ParameterCount ExpectedParameterNum
	{
		get { return ParameterCount.Two; }
	}
}

class GetCommand : ConsoleCommand
{
	public override void Execute(params string[] parameters)
	{
		Console.WriteLine(Ph.Settings.GetString(parameters[0]));
	}
	
	public override ParameterCount ExpectedParameterNum
	{
		get { return ParameterCount.One; }
	}
}
