using System;
using ScriptBindings;

class PrintCommand : ConsoleCommand
{
	public override void Execute(params string[] parameters)
	{
		Console.WriteLine(parameters[0]);
	}
	
	public override ParameterCount ExpectedParameterNum
	{
		get { return ParameterCount.SingleFullString; }
	}
}

class RecompileCommand : ConsoleCommand
{
	public override void Execute(params string[] parameters)
	{
		
	}
}