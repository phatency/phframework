using Ph;
using ScriptBindings;
using System.Runtime.InteropServices;
using System;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Ph.SQLite;



class SQLCommand : ConsoleCommand
{
  SQLiteConnection connection = null;
  SQLiteStatement statement   = null;

  public override void Execute(params string[] parameters)
  {
    Log.Debug(parameters[0]);

    

    //List<string> results = statement.ExecuteStringQuery(parameters[0]);
    //foreach (string result in results)
    //  Log.Debug(result);
    DataTable results = statement.ExecuteQuery(parameters[0]);
    foreach (DataRow row in results.Rows)
      for (int i = 0; i < results.Columns.Count; i++)
        Log.Debug(row[i].ToString());
  }


  ~SQLCommand()
  {

  }

  public SQLCommand()
  {
    connection = new SQLiteConnection("test.s3db");
    if (connection.IsOpen)
      statement = new SQLiteStatement(connection);
    
  }




  public override ParameterCount ExpectedParameterNum
  {
    get { return ParameterCount.SingleFullString; }
  }
}