﻿#undef DEBUG
#define DEBUG
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using ScriptBindings;
using Ph;

  public class TCPCommand : ConsoleCommand
  {
    private TCPClient client = new TCPClient();
    private TCPServer server = new TCPServer();

    public override void Execute(params string[] parameters)
    {
      if (parameters[0] == "start")
      {
        server.Start();
        client.Start();
      }
      else if(parameters[0] == "stop")
      {
        client.Stop();
        server.Stop();

      }
      else
      {

        client.Message(parameters[0]);
      }

    }

    public override ParameterCount ExpectedParameterNum
    {
      get { return ParameterCount.SingleFullString; }
    }
  }

  class TCPClient
  {
    private static List<string> messages = new List<string>();
    private static bool running = false;

    public void Start()
    {
      Log.Debug("TCPClient connecting");
      TcpClient client = new TcpClient();

      IPEndPoint serverEndPoint = new IPEndPoint(IPAddress.Loopback, 3000);
      running = true;
      
        try
        {
          client.Connect(serverEndPoint);
          Log.Debug("TCPClient connected");
        }
        catch (Exception ex)
        {
          Log.Error(ex.Message);

        }
      
      Thread network = new Thread(
        delegate()
        {
          while (running)
          {
            if (messages.Count != 0)
              lock (messages)
              {
                Log.Debug("Message acquired, trying to send");
                NetworkStream clientStream = client.GetStream();
                byte[] buffer = Encoding.UTF8.GetBytes(messages[0]);
                clientStream.Write(buffer, 0, buffer.Length);
                clientStream.Flush();
                messages.RemoveAt(0);
              }
            System.Threading.Thread.Sleep(1);

          }
          Log.Debug("TCPClient shutting down");
        } 
      );

      network.Start();
      client.Close();

    }


    public void Stop()
    {
      running = false;
    }

    internal void Message(string p)
    {
      lock (messages)
      messages.Add(p);
    }
  }

  class TCPServer
  {
    private TcpListener tcpListener;
    private Thread listenThread;
    private bool running;
    private static ManualResetEvent tcpClientConnected =
    new ManualResetEvent(false);

    public TCPServer()
    {
   
    }

    public void Start()
    {
      tcpListener = new TcpListener(IPAddress.Any, 3000);
      listenThread = new Thread(ListenForClients);
      running = true;
      Log.Debug("TCPServer listening for connections");
      listenThread.Start();
    }

    public void Stop()
    {
      
      running = false;
    }

    private void ListenForClients()
    {
      this.tcpListener.Start();
      bool connected = true;
      while (running)
      {
        //blocks until a client has connected to the server
        //TcpClient client = this.tcpListener.AcceptTcpClient();
        if (connected)
          tcpListener.BeginAcceptTcpClient( 
             delegate(IAsyncResult ar)
              {
                Log.Debug("TCPServer acquired connection.");
                var listener = (TcpListener)ar.AsyncState;
                TcpClient client = listener.EndAcceptTcpClient(ar);
                HandleClientComm(client);
                tcpClientConnected.Set();
              } , tcpListener


            );
        connected = tcpClientConnected.WaitOne(100);
        //create a thread to handle communication
        //with connected client
        //Thread clientThread = new Thread(new ParameterizedThreadStart(HandleClientComm));
        //clientThread.Start(client);
        
      }
      tcpListener.Stop();
      Log.Debug("TCPServer shutting down");
    }

    private void HandleClientComm(TcpClient client)
    {
      Log.Debug("TCPServer handling connection.");
      TcpClient tcpClient = (TcpClient)client;
      NetworkStream clientStream = tcpClient.GetStream();

      byte[] message = new byte[4096];
      int bytesRead;

      while (true)
      {
        bytesRead = 0;

        try
        {
          //blocks until a client sends a message
          bytesRead = clientStream.Read(message, 0, 4096);
        }
        catch
        {
          //a socket error has occured
          break;
        }

        if (bytesRead == 0)
        {
          //the client has disconnected from the server
          break;
        }

        //message has successfully been received
        Console.WriteLine(Encoding.ASCII.GetString(message, 0, bytesRead));
      }

      tcpClient.Close();
    }
  }
