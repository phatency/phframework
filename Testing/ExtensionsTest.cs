﻿using System.Linq;
using NUnit.Framework;
using Ph.Extensions.String;
using Ph.Extensions.Char;
using NUnit.Framework.SyntaxHelpers;

namespace Testing
{
  [TestFixture]
  public class ExtensionTest : AssertionHelper
  {
    [Test]
    public void TestString_EmptyOrNull()
    {
      string a = null, b = "", c = "HAL";
      Assert.That(a.IsEmptyOrNull());
      Assert.That(b.IsEmptyOrNull());
      Assert.That(b.IsEmpty());

      Assert.That(c.IsEmpty(), Is.False);
      Assert.That(c.IsEmptyOrNull(), Is.False);
    }

    [Test]
    public void TestChar_IsNumeric()
    {
      const string numericValues = "+,-.0123456789";
      const string alphabeticalValues = "abcdefghijklmnopqrstuwvxyzåäö";
      for (int i = 0; i < numericValues.Length; i++)
        Assert.That(numericValues[i].IsNumeric(), Is.True);
      for (int i = 0; i < alphabeticalValues.Length; i++)
        Assert.That(alphabeticalValues[i].IsNumeric(), Is.False);
      for (char i = (char)0; i <= 255; i++)
        if (numericValues.Contains(i))
          Assert.That(i.IsNumeric(), Is.True);
        else
          Assert.That(i.IsNumeric(), Is.False);

    }
  }
}
