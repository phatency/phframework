﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using NUnit.Framework.SyntaxHelpers;
using NUnit.Framework.Extensions;
using Ph.SQLite;

namespace Testing
{
 [TestFixture]
  public class SQLiteConnectionTest : AssertionHelper
  {
   private const string dbFile = "unit_test.db3";
   public bool DatabaseExists
   {
     get
     {
       FileInfo fileInfo = new FileInfo(dbFile);
       return fileInfo.Exists;
     }
   }

   [SetUp]
   public void SetUpByDeletingExistingDatabase()
   {
     FileInfo fileInfo = new FileInfo(dbFile);
     if (fileInfo.Exists)
       fileInfo.Delete();
   }

   [Test]
   public void CreateDatabase()
   {
     SQLiteConnection connection = new SQLiteConnection();
     Assert.That(connection.IsOpen, Is.Not.True);
     connection.Open(dbFile, SQLFileOpen.Create);
     Assert.That(connection.IsOpen);
     Assert.That(DatabaseExists);
     connection.Close();
     Assert.That(connection.IsOpen, Is.Not.True);
   }

   [Test]
   public void OpenExistingDatabase()
   {
     CreateDatabase();
     SQLiteConnection connection = new SQLiteConnection(dbFile);
     Assert.That(connection.IsOpen);
     connection.Close();
     Assert.That(connection.IsOpen, Is.Not.True);
     
   }

   [Test]
   [ExpectedException(typeof(SQLiteException))]
   public void OpenNonexistingDatabase()
   {
     SQLiteConnection connection = new SQLiteConnection();
     Assert.That(connection.IsOpen, Is.Not.True);
     Assert.That(DatabaseExists, Is.Not.True);
     try
     {
       connection.Open(dbFile, SQLFileOpen.ReadWrite);
     }
     catch (SQLiteException ex)
     {
       Assert.That(ex.ErrorCode == SQLResult.CantOpen);
       throw;
     }
   } 

   [Test]
   [ExpectedException(typeof(SQLiteException))]
   public void TryToGetAPointerToADatabaseWhenConnectionIsNotOpen()
   {
     SQLiteConnection connection = new SQLiteConnection();
     IntPtr db = connection.Database;
   }
   [TearDown]
   public void CloseConnection()
   {
     
   }
  }
}
