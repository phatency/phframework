﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using NUnit.Framework;
using NUnit.Framework.Constraints;
using NUnit.Framework.SyntaxHelpers;
using NUnit.Framework.Extensions;
using Ph.SQLite;
namespace Testing
{
  [TestFixture]
  public class SQLiteStatementTest : AssertionHelper
  {
    private SQLiteConnection con = null;
    private SQLiteStatement st = null;
    private string dbFile = Path.Combine("testData", "unit_test.db3");

    public void DeleteExistingDatabase()
    {
      FileInfo fileInfo = new FileInfo(dbFile);
      if (fileInfo.Exists)
        fileInfo.Delete();
    }


    [TestFixtureSetUp]
    public void OpenConnectionToANewDatabase()
    {
      try
      {
        DeleteExistingDatabase();
      }
      catch (Exception)
      {
        dbFile = Path.Combine("testData", Path.GetRandomFileName() + ".db3");
      }
      
      con = new SQLiteConnection();
      con.Open(dbFile, SQLFileOpen.ReadWrite | SQLFileOpen.Create );
      Assert.That(con.IsOpen);
      
    }

    [TestFixtureTearDown]
    public void CloseConnectionToTheDatabase()
    {
      con.Close();
      try { DeleteExistingDatabase(); } catch {}
    }

    [SetUp]
    public void CreateNewSQLiteStatement()
    {
      st = new SQLiteStatement(con);
      FillDatabaseWithJunk();
    }
    
    
    public void FillDatabaseWithJunk()
    {
      st.ExecuteNonQuery("DROP TABLE IF EXISTS test");
      st.ExecuteNonQuery(
        @"CREATE TABLE test (
          id INTEGER PRIMARY KEY AUTOINCREMENT,
          textfield TEXT NOT NULL,
          realfield REAL NOT NULL)
          ");
      st.Prepare("INSERT INTO test (textfield,realfield) VALUES(?,?)");
      for (decimal d = new decimal(0.1); d < 10; d += new decimal(0.1)) // using decimal to avoid rounding errors
      {
        st.Reset();
        st.Bind(d.ToString(), Decimal.ToDouble(d));
        st.Step();
      }
      st.FinalizeStatement();
    }

    [Test]
    public void TestDataIntegrityWithDataTable()
    {
      DataTable data = st.ExecuteQuery("SELECT * FROM test");
      int lastID = 0;
      foreach (DataRow row in data.Rows)
      {
        int id = (int) row.ItemArray[0];
        
        string textfield = (string)row[1];
        double realfield = (double)row[2];
        Assert.That(lastID, Is.EqualTo(id - 1));
        Assert.That(Math.Round(id * 0.1, 2), Is.EqualTo(realfield));
        Assert.That(Double.Parse(textfield), Is.EqualTo(realfield));
        lastID = id;
      }
    }




  }
}
