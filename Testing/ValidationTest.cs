﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Testing 
{

  using NUnit.Framework;
  using NUnit.Framework.Constraints;
  using NUnit.Framework.SyntaxHelpers;
  using NUnit.Framework.Extensions;
  using Ph.Validation;




  [TestFixture]
  public class ValidationTest
  {
    [Test]
    public void TestValidatingNonNullValues()
    {
      object a = new object();
      string b = "";
      Validate.Begin()
              .IsNotNull(a, "object a")
              .IsNotNull(b, "string b")
              .Check();
        
    }

    [Test]
    [ExpectedException(typeof(ValidationException))]
    public void TestValidatingNullValues()
    {
      object a = null;
      string b = null;
      Validation val = Validate.Begin()
              .IsNotNull(a, "object a")
              .IsNotNull(b, "string b")
              .Check();
      Assert.AreEqual(2, val.Exceptions.Count);
    }

    [Test]
    public void TestValidatingRanges()
    {
      Validate.Begin()
              .IsInRange(5, 5, 10, "IsFiveBetweenFiveAndTen")
              .IsInRange(10, 5, 10, "IsTenBetweenFiveAndTen")
              .IsOver(0, -1, "isZeroOverMinusOne")
              .IsUnder(-1, 0, "IsMinusOneUnderZero")
              .IsNotNegative(0, "IsZeroNegative")
              .Check();

    }

    [Test]
    public void TestValidateCheck()
    {
      Validate.Begin().Check().Check();
      new Validation().Check(); // Not intended usage, but we will detect it and won't mind.
      Validation val = Validate.Begin().Check();
    }

    [Test]
    public void TestValidatingInvalidRanges()
    {
      try
      {
        Validate.Begin()
                  .IsInRange(4, 5, 10, "val")
                  .IsInRange(11, 5, 10, "val")
                  .IsOver(0, 0, "equal")
                  .IsUnder(0, 0, "equal")
                  .IsNotNegative(-1, "zero") // Remember to increase the count below if adding more tests!
                  .Check();
        Assert.Fail("Exception should have been thrown"); 
      }
      catch (ValidationException ex)
      {
        Assert.AreEqual(5, ex.Count); // We expect exactly 5 exceptions
      }
      
    }

  }
}
