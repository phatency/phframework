﻿using System;
using ScriptBindings;
using Ph.Extensions.String;
using Ph;
using UI;
using Services = Ph.Services;

namespace Testing
{



  class Program
  {
    static void Main(string[] args)
    {
      System.Globalization.CultureInfo culture = new System.Globalization.CultureInfo("en-US");
      System.Threading.Thread.CurrentThread.CurrentCulture = culture;

      Gui gui;
      try
      {
        var mngr = Services.ScriptManager;
        ICommandLine cmdline = null;
        //new UICompiler(mngr.Compiler).Compile();
        bool running = true;
        do
        {
          string line = Console.ReadLine();
          if (line == "exit")
            break;
          if (line.IsEmptyOrNull())
          {
            mngr.Load();

            cmdline = (ICommandLine) mngr.GetScriptTypeHandler("ConsoleCommand");
            //gui = new Gui();
            //gui.Init();
          }
          else if (cmdline != null) cmdline.Execute(line);
        } while (running);
      }
      catch (Ph.SQLite.SQLiteException ex)
      {
        Log.Error(ex.Message);
      }

        return;
      
    }
  }
}
