﻿
namespace Ph
{
  interface IService
  {
    string[] Depencies
    { 
      get;
    }

    bool Start();
    bool Stop();
    bool Restart();
  }
}
