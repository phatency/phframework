﻿using System.Collections.Generic;
using System.CodeDom.Compiler;
using System.Xml.Serialization;
using Ph.Extensions.String;
using System.IO;
using System.Reflection;
using System;

namespace Ph.Scripting
{

  public class ScriptLanguageManager
  {
    private Dictionary<ScriptLanguage, ScriptLanguageProvider> providers = new Dictionary<ScriptLanguage, ScriptLanguageProvider>();

    

    public ScriptLanguageProvider this[ScriptLanguage index]
    {
      get
      {
        return GetProvider(index);
      }
    }

    public ScriptLanguageManager(string settingsFile)
    {
      if (!settingsFile.IsEmptyOrNull())
        ReadSettings(settingsFile);
    }
    public ScriptLanguageManager() { }

    public void ReadSettings(string file)
    {
       XmlSerializer xml = new XmlSerializer(typeof(ScriptLanguageProviderList));
      //Microsoft.Xml.Serialization.GeneratedAssembly.ScriptLanguageProviderListSerializer xml = new Microsoft.Xml.Serialization.GeneratedAssembly.ScriptLanguageProviderListSerializer();
      try
      {
        using (StreamReader reader = new StreamReader(file))
        {
          ScriptLanguageProviderList list = (ScriptLanguageProviderList)xml.Deserialize(reader);
          foreach (ScriptLanguageProviderDefinition definition in list)
          {
            Assembly assembly = Assembly.LoadFile(Path.GetFullPath(definition.Assembly));
            Type type = assembly.GetType(definition.Name);
            if (type == null)
              throw new Exception("Type not found etc."); // TODO: New exceptions
            ScriptLanguageProvider provider = (ScriptLanguageProvider)assembly.CreateInstance(type.FullName);
            if (provider != null)
              providers.Add(definition.Language, provider);
          }
         
        }
      }
      catch (Exception ex)
      {
        Console.WriteLine(ex.Message);
      }
    }

    public ScriptLanguageProvider GetProvider(ScriptLanguage scriptLanguage)
    {
      ScriptLanguageProvider provider = null;
      if (providers.ContainsKey(scriptLanguage))
        provider = providers[scriptLanguage];
      else
      {
        switch (scriptLanguage)
        {
          case ScriptLanguage.CSharp:
            provider = new GenericNETLanguageProvider(ScriptLanguage.CSharp);
            break;
          case ScriptLanguage.VisualBasic:
            provider = new GenericNETLanguageProvider(ScriptLanguage.VisualBasic);
            break;
          default:
            throw new Exception("No provider for language " + scriptLanguage.ToString() + " is registered.");
        }
        providers.Add(scriptLanguage, provider);
      }
      return provider;
    }

     public ScriptLanguageProvider GetProvider(string extension)
    {
      return GetProvider(GetLanguage(extension));
    }

    public ScriptLanguage GetLanguage(string extension)
    {
      if (_languageExtensions.ContainsKey(extension))
        return _languageExtensions[extension];
        throw new FrameworkException("No language for the extension " + extension + " was found.");
    }

    public string GetExtension(ScriptLanguage language)
    {
      foreach (var pair in _languageExtensions)
        if (pair.Value == language)
          return pair.Key;
      throw new FrameworkException("No extension for the language " + language + " was found.");
    }

    private static Dictionary<string, ScriptLanguage> _languageExtensions = new Dictionary<string, ScriptLanguage>                                                                          
    {
       {".cs", ScriptLanguage.CSharp},
       {".vb", ScriptLanguage.VisualBasic},
       {".fs", ScriptLanguage.FSharp},
       {".js", ScriptLanguage.JScript}                                                    
    };
  }



  public enum AssemblyTargetType
  {
    Module,
    Library,
    Exe
  }

  public abstract class ScriptLanguageProvider
  {
    protected string outputAssembly;
    protected AssemblyTargetType target;
    abstract public void AddReferences(string[] references);
    abstract public bool Debug { get; set; }
    virtual public void AddModules(List<string> modules) { }
    abstract public bool GenerateInMemory { get; set; }
    protected CodeDomProvider provider;

    public CodeDomProvider CodeDomProvider
    { 
      get {  return provider; }
    }

    virtual public AssemblyTargetType Target
    {
      get { return target; }
      set { target = value; }
    }

    abstract public ScriptLanguage Language { get; }

    abstract public CompilerResults Compile(string[] sources);
    public string OutputAssembly { get { return outputAssembly; } set { outputAssembly = value; } }
  }

  class GenericNETLanguageProvider : ScriptLanguageProvider
  {
    protected CompilerParameters options = null;
    private ScriptLanguage language;

    public override ScriptLanguage Language
    {
      get { return language; ; }
    }

    public override bool Debug
    {
      get { return options.IncludeDebugInformation; }
      set { options.IncludeDebugInformation = value; }
    }

    public override bool GenerateInMemory
    {
      get { return options.GenerateInMemory; }
      set { options.GenerateInMemory = value; }
    }

    public GenericNETLanguageProvider(ScriptLanguage language)
    {
      this.language = language;

      var providerOptions = new Dictionary<string, string>();
      providerOptions.Add("CompilerVersion", "v3.5");

      if (language == ScriptLanguage.CSharp)
        provider = new Microsoft.CSharp.CSharpCodeProvider(providerOptions);
      else if (language == ScriptLanguage.VisualBasic)
        provider = new Microsoft.VisualBasic.VBCodeProvider(providerOptions);
      options = new CompilerParameters();
    }

    public override void AddReferences(string[] references)
    {
      options.ReferencedAssemblies.AddRange(references);
     
    }

    public override void AddModules(List<string> modules)
    {
      options.CompilerOptions += " /addmodule:";
      for (int i = 1; i <= modules.Count; i++)
      {
        options.CompilerOptions += modules[i - 1] + (i < modules.Count ? "," : "");
      }
    }

    public override CompilerResults Compile(string[] sources)
    {
      options.OutputAssembly = outputAssembly;
      options.CompilerOptions += " /lib:C:\\WINDOWS\\assembly ";
      options.CompilerOptions += " /target:" + target.ToString().ToLower();
 
      CompilerResults result = provider.CompileAssemblyFromFile(options, sources);
      options.CompilerOptions = "";
      options.ReferencedAssemblies.Clear();
      options.TempFiles.Delete();
      return result;
    }
  }
}
