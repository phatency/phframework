﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Xml.Serialization;
using System.IO;
using Ph.Collections;
using Ph.Validation;
using System.Reflection;
using Ph.SQLite;


namespace Ph.Scripting
{

  public class ScriptManager
  {
    private ScriptCompiler compiler;
    private Dictionary<string, ScriptTypeHandler> scriptTypeHandler = new Dictionary<string, ScriptTypeHandler>();
    private ScriptLanguageManager providers;
    //private SQLiteStatement cacheDB;
    private ScriptCache cache;
    private OverwritingDictionary<string, bool> loadedScripts = new OverwritingDictionary<string, bool>(); // bool = true if no errors were detected while loading.

    public ScriptCompiler Compiler
    {
      get { return compiler;}
    }

    public ScriptManager(string settingsFile = "languageproviders.xml")
    {
      providers = new ScriptLanguageManager(settingsFile);
      Initialize();
    }

    private void Initialize()
    {
      compiler = new ScriptCompiler(providers);
      RegisterScriptTypeHandler(new ScriptEntryPointHandler());
    }

    public void Load()
    {
      OpenCache();
      Load("*.xml");
      CloseCache();
    }

    private void CloseCache()
    {
      cache.Write();
      cache.PurgeDeprecated();
      //TODO: Is this needed?
      //cache.Clear();
    }
    private void OpenCache()
    {
      cache = new ScriptCache();

      string cacheLocation = new [] {"cache", "cache.xml"}.RelativePath();
      cache.Load(cacheLocation);
//      string cacheLocation = new [] {"cache", "cache.db3"}.RelativePath();
//      // TODO: Exception handling
//      SQLiteConnection connection = new SQLiteConnection();
//      if (!connection.TryOpen(cacheLocation))
//      {
//        connection.Open(cacheLocation, SQLFileOpen.ReadWrite | SQLFileOpen.Create);
//        connection.CreateStatement().ExecuteNonQuery(
//      @"CREATE TABLE scripts (
//        name TEXT PRIMARY KEY NOT NULL, 
//        version TEXT NOT NULL, 
//        checksums TEXT NOT NULL,
//        assembly TEXT NOT NULL)");
//      }


      //cacheDB = Database.Cache;
    }

    private void ComputeMD5(ScriptSource source)
    {
      StringBuilder curChecksums = new StringBuilder("PhCompiler 0.1\r\n");
      curChecksums.AppendLine(source.Assembly.Debug.ToString());
      foreach (string filePath in source.Sources)
        curChecksums.AppendLine(Utility.ComputeMD5FromFile(Path.Combine(source.RelativeFilePath, filePath)));
      foreach (string reference in source.Assembly.References)
        curChecksums.AppendLine(Utility.ComputeMD5(reference));
      
      source.Checksum = curChecksums.ToString();
    }


    public void Load(string searchString)
    {
      DirectoryInfo dir = new DirectoryInfo(Dir.Relative("scripts"));
      if (!searchString.EndsWith(".xml")) // allow only .xml-extension
        searchString += ".xml"; 
      if (!dir.Exists)
      {
        dir.Create();
        return;
      }
      FileInfo[] allFiles = dir.GetFiles(searchString, SearchOption.AllDirectories);
      Load(allFiles);
    }

    public void Load(FileInfo[] files)
    {
      if (files.Length == 0)
        return;
     
      XmlSerializer xml = new XmlSerializer(typeof(ScriptSource)); // todo: No auto-generated xml-serializers

      foreach (FileInfo file in files)
      {
        try
        {
          using (StreamReader reader = new StreamReader(file.FullName))
          {
            ScriptSource source = (ScriptSource) xml.Deserialize(reader);
   


            source.RelativeFilePath = Dir.MakeRelative(file.Directory.FullName);

            bool success = ValidateAndLoad(source);
            loadedScripts.Add(source.Name, success);


          }

        }
        catch (Exception ex)
        {
          Console.WriteLine(ex.Message);
        }
        
      }
    }

    private bool ValidateAndLoad(ScriptSource source)
    {
      Validate.Begin()
        .IsNotNull(source, "Script Source")
        .Check()
        .IsNotNullOrEmpty(source.Name, "Name")
        .Check();

      ComputeMD5(source);

      Log.Debug(String.Format("Loading script {0}", source.Name));
      Assembly output = null;
      
      //// if caching is enabled for this script and the cached file is set
      //if (source.Assembly.Cache == true && source.Assembly.File != null)
      //{
      //  // then we compare the old checksum of sources to the new ones.
      //  string oldChecksums = cacheDB.ExecuteStringQuery("SELECT checksums FROM scripts WHERE name='" + source.Name + "'");
      //  string cachedAssembly = cacheDB.ExecuteStringQuery("SELECT assembly FROM scripts WHERE name='" + source.Name + "'");
      //  source.Assembly.File = cachedAssembly;

      //  if (cachedAssembly != null && oldChecksums == source.Checksum)
      //  {
      //    if (loadedScripts.ContainsKey(source.Name))
      //      if (loadedScripts[source.Name] == true)
      //      {
      //        Log.Debug("Nothing has changed - not reloading.");
      //        return true;
      //      }
      //    Log.Debug("Checksum matches with the previously compiled version, trying to load from cache.");
      //    string assemblyFileName = cachedAssembly;
      //    FileInfo fileInfo = new FileInfo(assemblyFileName);
      //    if (fileInfo.Exists)
      //      try
      //      {
      //        output = Assembly.LoadFile(fileInfo.FullName);

      //        foreach (Type t in output.GetTypes())
      //          Debug("Type defined: " + t);

      //      }
      //      catch (Exception ex)
      //      {
      //        Log.Error(ex.Message);
      //        output = null;
      //      }
      //  }
      //}
      
       // if the output is null, assembly was not loaded from cache and should be compiled.
      if (output == null && !compiler.Compile(source, out output))
        return false;

      //cacheDB.ExecuteNonQuery("DELETE FROM scripts WHERE name=?", source.Name);

      //cacheDB.ExecuteNonQuery("INSERT INTO scripts VALUES (?,?,?,?)", source.Name, source.Version, source.Checksum, output.Location);

      cache.Add(new CachedScript(source.Name, source.Checksum, source.Version, output.Location));
      return RegisterDefinitions(source, output);
    }



    public bool RegisterDefinitions(ScriptSource scriptSource, Assembly assembly)
    {
      var failures = new List<string>();
      foreach (KeyValuePair<string, ScriptDefinition> definition in scriptSource.Definitions)
        if (scriptTypeHandler.ContainsKey(definition.Key))
          try
          {
            scriptTypeHandler[definition.Key].Register(assembly, definition.Value);
          }
          catch (Exception ex)
          {
            failures.Add(ex.Message);
            failures.Add(ex.StackTrace);
          }
        else
          failures.Add("No handler has been registered for definition type: " + definition.Key);
      if (failures.Count > 0)
      {
        foreach (string failure in failures)
          Log.Debug(failure);
        return false;
      }
      return true;
    }

    public void RegisterScriptTypeHandler(ScriptTypeHandler handler)
    {
      Log.Debug("New scripthandler registered: {0}", handler.ScriptType);
      scriptTypeHandler.Add(handler.ScriptType, handler);
    }

    public ScriptTypeHandler GetScriptTypeHandler(string typeName)
    {
      ScriptTypeHandler handler;
      scriptTypeHandler.TryGetValue(typeName, out handler);
      return handler;
    }

    [Conditional("COMPILER_DEBUG")]
    private void Debug(string format, params object[] parameters)
    {
      Log.Debug(format, parameters);
    }
  }
}
