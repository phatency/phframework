﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.IO;


namespace Ph.Scripting
{
  /// <summary>
  /// TODO: Nothing stops this class from deleting essential system files.
  /// Maintains a list of cached scripts and information about them.
  /// </summary>
  public class ScriptCache
  {
    Dictionary<string, CachedScript> _scripts = new Dictionary<string, CachedScript>();
    List<string> _deprecatedAssemblies = new List<string>();

    public void Add(CachedScript script)
    {
      if (_scripts.ContainsKey(script.Name))
      {
        AddDeprecated(_scripts[script.Name].Assembly);
        _scripts.Remove(script.Name);
      }
      _scripts.Add(script.Name, script);
    }

    private void AddDeprecated(string assembly)
    {
      _deprecatedAssemblies.Add(assembly);
    }

    public void PurgeDeprecated()
    {
      List<string> _inUse = new List<string>();

      foreach (string assembly in _deprecatedAssemblies)
        try
        {
          File.Delete(assembly);
        }
        catch (IOException)
        {
          _inUse.Add(assembly);
        }
      _deprecatedAssemblies = _inUse;
    }

    public void Load(string filename)
    {
      _scripts.Clear();
      
      try
      {
        using (XmlTextReader reader = new XmlTextReader(filename))
        {
          reader.ReadToFollowing("Current");
          bool reading = reader.ReadToDescendant("Script");
          while (reading)
          {
            CachedScript script = new CachedScript();
            script.Name = reader["Name"];
            script.Version = reader["Version"];
            script.Assembly = reader["Assembly"];
            script.Checksum = reader.ReadElementContentAsString();
            reading = reader.ReadToNextSibling("Script");
            _scripts.Add(script.Name, script);
          }
          reader.ReadToFollowing("Deprecated");
          reading = reader.ReadToDescendant("Script");
          while (reading)
          {
            string assembly = reader[0];
            _deprecatedAssemblies.Add(assembly);
            reading = reader.ReadToNextSibling("Script");
          }
        }
      }
      catch (IOException)
      {
        Log.Warn("Could not load: " + filename);
      }
      catch (XmlException)
      {
        throw new NotImplementedException();
      }

    }
    public void Write()
    {

      _scripts.Add("kekaka", new CachedScript("kekaka", "af0jf0afs9jasj09jv9v9ja0sjv9j0a0svv9s00s9va", "1.2.3", "C:\\d.exe"));
      _deprecatedAssemblies.AddRange(new[] { "hahahi", "huhaho", "kekakakako" });
      string filename = "test.xml";

      using (FileStream fileStream = new FileStream(filename, FileMode.Create))
      using (StreamWriter sw = new StreamWriter(fileStream))
      using (XmlTextWriter writer = new XmlTextWriter(sw))
      {
        writer.Indentation = 4;
        writer.Formatting = Formatting.Indented;

        writer.WriteStartDocument();
        writer.WriteStartElement("ScriptCache");

        writer.WriteStartElement("Current");
        foreach (var script in _scripts)
        {
          writer.WriteStartElement("Script");
          writer.WriteAttributeString("Name", script.Key);
          writer.WriteAttributeString("Version", script.Value.Version);
          writer.WriteAttributeString("Assembly", script.Value.Assembly);
          writer.WriteString(script.Value.Checksum);
          writer.WriteEndElement();
        }
        writer.WriteEndElement();

        writer.WriteStartElement("Deprecated");
        foreach (string assembly in _deprecatedAssemblies)
        {
          writer.WriteStartElement("Script");
          writer.WriteAttributeString("Assembly", assembly);
          writer.WriteEndElement();
        }
        writer.WriteEndElement();

        writer.WriteEndElement();
      }

    }

  }

public class CachedScript
  {
    public CachedScript(string name, string checksum, string version, string assembly)
    {
        Name = name; Checksum = checksum; Version = version;Assembly=assembly;
    }
    public CachedScript() { }

    public string Name;
    public string Version;
    public string Checksum;
    public string Assembly;
  }
}
