﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using System.Xml;

namespace Ph.Scripting
{
  public class ScriptLanguageProviderDefinition
  {
    [XmlAttribute]
    public ScriptLanguage Language;

    [XmlAttribute]
    public string Assembly;

    [XmlAttribute]
    public string Name;
  }

  [XmlRoot("ScriptLanguageProviders")]
  public class ScriptLanguageProviderList
  {
    [XmlElement(ElementName = "Provider")]
    public ScriptLanguageProviderDefinition[] Providers;

    public System.Collections.IEnumerator GetEnumerator()
    {
      return Providers.GetEnumerator();
    }
  }

  public enum ScriptLanguage
  {
    CSharp,
    VisualBasic,
    JScript,
    FSharp
  }
  /// <summary>
  /// Todo: expected amount of parameters for derived class
  /// </summary>
  public class ScriptDefinition
  {
    protected List<KeyValuePair<string, string>> parameters = null;
    public string Location;

    public string Name;

    /// <summary>
    /// Gets the value of the given parameter name. Returns null if not found. Do note that custom parameters are optimized for fast insertion and slow access, as
    /// scriptdefinitions are not intended to be longliving data-objects.
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public string this[string index]
    {
      get
      {
        if (parameters == null)
          return null;
        foreach (KeyValuePair<string, string> stringPair in parameters)
          if (stringPair.Key == index)
            return stringPair.Value;
        return null;
      }
    }

    /// <exception cref="XmlException" />
    internal void ReadXml(XmlReader reader)
    {
      string definitionName = reader.Name;
      bool emptyElement = reader.IsEmptyElement;
      for (int i = 0; i < reader.AttributeCount; i++)
      {
        reader.MoveToAttribute(i);
        if (reader.Name == "Location")
          Location = reader.Value;
        else if (reader.Name == "Name")
          Name = reader.Value;
        else
          AddParameter(reader.Name, reader.Value);
      }
      if ((Location ?? Name) == null)
        throw new XmlException("Incorrect xml, Location and Name are required attributes for ScriptDefinition");

      if (!emptyElement)
        while (reader.NodeType != XmlNodeType.EndElement || reader.Name != definitionName)
        {
          reader.Read();
          if (reader.NodeType == XmlNodeType.EndElement)
            continue;
          AddParameter(reader.Name, reader.ReadString());
        }
    }

    internal void WriteXml(XmlWriter writer)
    {
      writer.WriteAttributeString("Name", Name);
      writer.WriteAttributeString("Location", Location);
      IEnumerable<KeyValuePair<string, string>> custParams = GetParameters();
      if (custParams != null)
        foreach (KeyValuePair<string, string> param in custParams)
          writer.WriteElementString(param.Key, param.Value);
    }

    protected virtual void AddParameter(string name, string value)
    {
      if (parameters == null)
        parameters = new List<KeyValuePair<string, string>>();
      parameters.Add(new KeyValuePair<string, string>(name, value));
    }

    protected virtual IEnumerable<KeyValuePair<string, string>> GetParameters()
    {
      return parameters;
    }


  }



  public class ScriptDefinitions : IXmlSerializable
  {
    /// <summary>
    /// List of definitions for scripts. These types are user-defined, and don't have to be derived from any particular base-class.
    /// </summary>
    private List<KeyValuePair<string, ScriptDefinition>> definitions = new List<KeyValuePair<string, ScriptDefinition>>();

    public void Add(string typeName, ScriptDefinition def)
    {
      definitions.Add(Utility.MakePair(typeName, def));
    }

    public IEnumerator<KeyValuePair<string, ScriptDefinition>> GetEnumerator()
    {
      return definitions.GetEnumerator();
    }

    #region IXmlSerializable Members

    public System.Xml.Schema.XmlSchema GetSchema()
    {
      return null;
    }

    public void ReadXml(System.Xml.XmlReader reader)
    {

      if (!reader.IsEmptyElement)
      {
        reader.Read();
        while (reader.NodeType != XmlNodeType.EndElement || reader.Name != "Definitions")
        {

          if (reader.NodeType != XmlNodeType.Element)
          {
            reader.Read();
            continue;
          }
          string fullTypeName;
          string typeName = reader.Name.Replace("Definition", "");

          if (reader.Name.Contains('.'))
            fullTypeName = reader.Name;
          else if (reader.Name == "EntryPoint")
            fullTypeName = "Ph.Scripting.ScriptEntryPointDefinition, PhFramework";
          else
          {
            fullTypeName = "ScriptBindings." + reader.Name;
            if (!fullTypeName.EndsWith("Definition"))
              fullTypeName += "Definition";
            fullTypeName += ", ScriptingBindings";
          }
          Type type = Type.GetType(fullTypeName);

          if (type == null)
            throw new XmlException("Could not resolve type: " + reader.Name);

          ScriptDefinition def = (ScriptDefinition)Activator.CreateInstance(type);

          def.ReadXml(reader);

          // object def = (object)new XmlSerializer(type).Deserialize(reader);

          if (def != null)
            definitions.Add(Utility.MakePair(typeName, def));

        }
      }
      reader.Read();
    }



    public void WriteXml(System.Xml.XmlWriter writer)
    {
      foreach (KeyValuePair<string, ScriptDefinition> def in definitions)
      {
        writer.WriteStartElement(def.Key);
        def.Value.WriteXml(writer);
        writer.WriteEndElement();
        //Type type = def.Value.GetType();
        //new XmlSerializer(type).Serialize(writer, def.Value);
      }

    }

    #endregion
  }



  public class ScriptSource
  {
    private string assemblyName;
    private string version;
    private string filePath;
    private string[] files;
    private string checksum;
    private ScriptDefinitions definitions;
    private ScriptAssembly assembly;

    //[XmlArray("")]
    //[XmlArrayItem(Type=typeof(Ph.Xml.AbstractXmlSerializer<ScriptParameters>))]
    // [XmlElement(ElementName="Parameters", Type=typeof(Ph.Xml.AbstractXmlSerializer<ScriptParameters>))]

    [XmlElement(ElementName = "Name")]
    public string Name
    {
      get { return assemblyName; }
      set { assemblyName = value; }
    }

    public string Version
    {
      get { return version; }
      set { version = value; }
    }

    public ScriptDefinitions Definitions
    {
      get { return definitions; }
      set { definitions = value; }
    }

    public ScriptAssembly Assembly
    {
      get { return assembly; }
      set { assembly = value; }
    }


    //public ScriptSourceList Sources;

    [XmlArray("Sources")]
    [XmlArrayItem("File")]
    public string[] Sources
    {
      get { return files; }
      set { files = value; }
    }

    [XmlIgnore]
    public string RelativeFilePath
    {
      get { return filePath; }
      set { filePath = value; }
    }

    [XmlIgnore]
    public string Checksum
    {
      get { return checksum; }
      set { checksum = value; }
    }
  }

  public class ScriptAssembly
  {

    [XmlAttribute]
    public bool Cache = false;

    [XmlAttribute]
    public bool Debug = true;


    [XmlAttribute]
    public bool Compile = true;

    public string File; 

    [XmlElement("Module")]
    public string[] Modules;


    [XmlElement(ElementName = "Reference")]
    public string[] References = { "mscorlib.dll", "ScriptingBindings.dll" };



    //internal void AddCachedAssembly(string fileName)
    //{
    //  if (!Assemblies.Contains(fileName))
    //    Assemblies.Add(fileName);
    //}
  }
  [XmlRoot(ElementName = "Sources")]
  public class ScriptSourceList
  {
    public string this[int index]
    {
      get { return Files[index]; }
      set { Files[index] = value; }
    }

    [XmlAttribute]
    public ScriptLanguage Language = ScriptLanguage.CSharp;

    [XmlElement(ElementName = "File")]
    public string[] Files;
  }
}
