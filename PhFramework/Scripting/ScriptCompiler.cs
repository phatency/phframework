﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.CodeDom.Compiler;

namespace Ph.Scripting
{
  public class CompileParameters
  {
    public string[] SourceCode;
    public string[] References;
    public bool Debug;
    public string AssemblyName;
    public string RelativePath = "";
  }

  public class ScriptCompiler
  {

    private readonly string cacheDirectory = Dir.Relative("cache");
    private readonly ScriptLanguageManager providers;

    public ScriptLanguageManager Providers
    {
      get { return providers;}
    }
    
    public ScriptCompiler(ScriptLanguageManager languages)
    {
      providers = languages;
    }
    
    public bool Compile(ScriptSource scriptSource, out Assembly output)
    {
      var parameters = new CompileParameters
      {
        SourceCode = scriptSource.Sources,
        AssemblyName = scriptSource.Name,
        References = scriptSource.Assembly.References,
        Debug = scriptSource.Assembly.Debug,
        RelativePath = scriptSource.RelativeFilePath
      };
      return Compile(parameters, out output);
    }
    /// <summary>
    /// So how  do we compile multiple languages into one assembly? Easy, we just compile all except the last language into .netmodules. 
    /// </summary>
    public bool Compile(CompileParameters parameters , out Assembly output)
    {
      ResolveReferences(parameters.References);
      var separatedFiles = new Dictionary<ScriptLanguage, List<string>>();
      
      foreach (string file in parameters.SourceCode)
      {
        string ext = Path.GetExtension(file);
        ScriptLanguage lang = providers.GetLanguage(ext);
        if (!separatedFiles.ContainsKey(lang))
          separatedFiles.Add(lang, new List<string>());

        separatedFiles[lang].Add(Path.Combine(parameters.RelativePath, file));
      }

      bool multipleLanguages = separatedFiles.Count > 1 ? true : false;
      if (multipleLanguages && separatedFiles.ContainsKey(ScriptLanguage.JScript) && separatedFiles.ContainsKey(ScriptLanguage.FSharp))
        throw new Exception("JScript and FSharp scripts are not supported in the same assembly due to compiler limitations.");
      IEnumerator<KeyValuePair<ScriptLanguage, List<string>>> sourceEnumerator = separatedFiles.GetEnumerator();

      List<string> tempModules = multipleLanguages ? new List<string>() : null;
      for (int i = 1; i <= separatedFiles.Count; i++)
      {
        sourceEnumerator.MoveNext();
        KeyValuePair<ScriptLanguage, List<string>> sources = sourceEnumerator.Current;

        ScriptLanguageProvider provider = providers.GetProvider(sources.Key);

        provider.AddReferences(parameters.References);

        provider.Debug = parameters.Debug;
        provider.GenerateInMemory = false;

        if (multipleLanguages)
          provider.AddModules(tempModules);
        if (multipleLanguages && i != separatedFiles.Count) // Target is a module
        {
          string moduleName = Path.Combine(Path.Combine(cacheDirectory, parameters.RelativePath), Path.GetRandomFileName()) + ".netmodule";

          provider.OutputAssembly = moduleName;

          tempModules.Add(moduleName);
          provider.Target = AssemblyTargetType.Module;
        }
        else // target is a library
        {
          provider.Target = AssemblyTargetType.Library;
          provider.OutputAssembly = GetCacheFile(parameters.AssemblyName, parameters.RelativePath, true);

        }
        CompilerResults results;

        try
        {
          results = provider.Compile(sources.Value.ToArray());
        }
        catch (FileNotFoundException ex)
        {
          Console.WriteLine("File not found: {0}", Dir.MakeRelative(ex.FileName));
          output = null;
          return false;
        }

        if (results.Errors.HasErrors)
        {
          foreach (CompilerError err in results.Errors)
          {
            Log.Error(err.ToString());
          }
          output = null;
          return false;
        }
        if (!multipleLanguages || i == separatedFiles.Count)
        {
          output = results.CompiledAssembly;
          //source.Assembly.File = Dir.MakeRelative(provider.OutputAssembly);
          //if (tempModules != null)
          //  source.Assembly.Modules = tempModules.ToArray();
          //TODO: Save modules in cache.db instead
          results.TempFiles.Delete();
          foreach (Type t in output.GetTypes())
            Log.Debug("Type defined: " + t);
         
          return true;
        }
      }

      output = null;
      return false;
    }

    public void ResolveReferences(string[] references)
    {
      for(int i = 0; i < references.Length; i++)
      {
        if (references[i].StartsWith("Script:"))
        {
          string scriptName = references[i].Substring(7);
          string assembly = Database.Cache.ExecuteStringQuery("SELECT assembly FROM scripts WHERE name=?", scriptName);
          references[i] = Path.Combine(Dir.Root, assembly);
        }
      }
    }

    public string GetCacheFile(string fileName, string relativePath, bool deleteOld)
    {
      // TODO: move somewhere elsewhere not herewhere
      DirectoryInfo cacheDirInfo = new DirectoryInfo(Path.Combine(Dir.Absolute("cache"), relativePath));


      if (!cacheDirInfo.Exists)
        cacheDirInfo.Create();
      string filePath = Path.Combine(cacheDirInfo.FullName, fileName);
      if (deleteOld)
      {
        FileInfo file = new FileInfo(filePath + ".dll");
        if (file.Exists)
          try
          {
            file.Delete();
          }
          catch
          {
            filePath += Path.GetRandomFileName();
          }
      }
      return filePath + ".dll";
    }
  }
}
