﻿using Ph.Collections;
using Ph.Validation;
using System.Reflection;

namespace Ph.Scripting
{

  public abstract class ScriptObject
  {
    public abstract void Init();
  }



  public abstract class ScriptTypeHandler
  {

    /// <summary>
    /// Provides the unique name of the ScriptType this handler.. handles
    /// </summary>
    public abstract string ScriptType
    {
      get;
    }
    public abstract void Register(Assembly script, ScriptDefinition definition);
  }

  public abstract class ScriptTypeFactoryHandler<FactoryType, InstanceType, DefinitionType> 
  : ScriptTypeHandler 
    where FactoryType : Factory<InstanceType>, new()
    where InstanceType : ScriptObject //new()
    where DefinitionType : ScriptDefinition
  {
    protected FactoryType instanceFactory = new FactoryType();
    protected OverwritingDictionary<string, DefinitionType> definitions;

    public override void Register(Assembly script, ScriptDefinition definition)
    {
      DefinitionType def = definition as DefinitionType;
      Ph.Validation.Validation validation = Validate.Begin()
        .IsNotNull(def, "ConsoleCommandDefinition")
        .Check()
        .IsNotNullOrEmpty(def.Location, "ConsoleCommand.Location") // Note: Also checked while parsing the xml in ScriptDefinition.ReadXML()
      ;
      ValidateDefinition(validation).Check();
      AddDefinition(def.Name, def);

      if (def.Location == "Internal")
        return;
      InstanceType instance = instanceFactory.Add(def.Name, def.Location, script);
      Initialize(instance);
      
    }

    protected virtual void Initialize(ScriptObject instance)
    {
      if (instance != null)
        instance.Init();
    }

    protected virtual void AddDefinition(string name, DefinitionType definition)
    {
      if (definitions == null)
        definitions = new OverwritingDictionary<string,DefinitionType>();
      definitions.Add(name, definition);
    }

    public virtual Validation.Validation ValidateDefinition(Validation.Validation validation)
    {
      return validation;
    }
  }

  

  //public class ScriptInstanceDefinitionHandler<DefinitionType, Factory<ScriptInstanceType>>
  //  : ScriptTypeHandler                      // derives from
  //  where DefinitionType : ScriptDefinition  // first generic parameter
  //  where ScriptInstanceType : ScriptObject          // second generic parameter
  //{
  //  protected OverwritingDictionary<string, ScriptInstanceType> instances = new OverwritingDictionary<string, ScriptInstanceType>();
  //  protected OverwritingDictionary<string, DefinitionType> definitions   = new OverwritingDictionary<string, DefinitionType>();

  //  private string typeName;

  //  public ScriptInstanceDefinitionHandler(string typeName)
  //  {
  //    this.typeName = typeName;
  //  }

  //  public override string ScriptType
  //  {
  //    get { return typeName; }
  //  }

  //  public override void Register(System.Reflection.Assembly script, object definition)
  //  {
  //    DefinitionType def = definition as DefinitionType;
  //    Ph.Validation.Validation validation = Validate.Begin()
  //      .IsNotNull(def, "ConsoleCommandDefinition")
  //      .Check()
  //      .IsNotNullOrEmpty(def.Location, "ConsoleCommand.Location") // Note: Also checked while parsing the xml in ScriptDefinition.ReadXML()
  //      ;
  //    ValidateDefinition(validation).Check();
  //    definitions.Add(def.Name, def);

  //    if (def.Location == "Internal")
  //      return;

  //    Type t = script.GetType(def.Location);

  //    if (t == null)
  //      return; //TODO: Throw an exception

  //    ScriptInstanceType instance = ConstructInstance(t, script);

  //    AddInstance(def.Name, instance);
  //  }

  //  protected virtual void AddDefinition(string name, DefinitionType definition)
  //  {
  //    definitions.Add(name, definition);
  //  }

  //  protected virtual void AddInstance(string name, ScriptInstanceType instance)
  //  {
  //    instances.Add(name, instance);
  //  }

  //  protected virtual ScriptInstanceType ConstructInstance(Type type, Assembly script)
  //  {
  //    ScriptInstanceType instance = (ScriptInstanceType)Activator.CreateInstance(type);
  //    instance.Init();
  //    return instance;
  //  }



  //  protected virtual Ph.Validation.Validation ValidateDefinition(Ph.Validation.Validation validation)
  //  {
  //    return validation;
  //  }
  //}
}
