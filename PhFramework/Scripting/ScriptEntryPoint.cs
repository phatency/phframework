﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;
using Ph.Collections;

namespace Ph.Scripting
{
  public abstract class ScriptEntryPoint : ScriptObject
  {

  }

  [XmlRoot("EntryPoint")]
  public class ScriptEntryPointDefinition : ScriptDefinition
  {
    
  }

  internal class ScriptEntryPointHandler : ScriptTypeFactoryHandler<SingleFactory<ScriptEntryPoint>, ScriptEntryPoint, ScriptEntryPointDefinition>
  {

    public override string ScriptType
    {
      get { return "EntryPoint"; }
    }
  }
}
