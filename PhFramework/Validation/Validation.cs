﻿using System;
using System.Collections.Generic;
using System.Text;
using Ph.Extensions.String;
namespace Ph.Validation // Parameter validation system, based on Pain.NET blog-entry a-fluent-approach-to-c-parameter-validation
{

  public static class Validate
  {
    public static Validation Begin()
    {
      return null;
    }
  }

  public sealed class Validation
  {
    private List<Exception> exceptions;

    public List<Exception> Exceptions
    {
      get { return this.exceptions; }
    }

    public Validation AddException(Exception ex)
    {
      lock (this.exceptions)
      {
        this.exceptions.Add(ex);
      }
      return this;
    }

    public Validation()
    {
      this.exceptions = new List<Exception>(1); // optimize for only having 1 exception
    }
  }

  public static class ValidationExtensions
  {
    public static Validation IsNotNull<T>(this Validation validation, T testSubject, string paramName)
      where T : class
    {
      if (testSubject == null)
        return (validation ?? new Validation()).AddException(
          new ArgumentNullException(paramName));
      else
        return validation;
    }

    public static Validation IsNotNegative(this Validation validation, long testSubject, string paramName)
    {
      if (testSubject < 0)
        return (validation ?? new Validation()).AddException(
          new ArgumentOutOfRangeException(paramName, "Value must be non-negative, but was " + testSubject.ToString()));
      else
        return validation;
    }

    public static Validation IsOver(this Validation validation, long testSubject, long min, string paramName)
    {
      if (testSubject <= min)
        return (validation ?? new Validation()).AddException(
          new ArgumentOutOfRangeException(paramName, "Value must be over " + min.ToString() + " but was " + testSubject.ToString()));
      else
        return validation;
    }

    public static Validation IsUnder(this Validation validation, long testSubject, long max, string paramName)
    {
      if (testSubject >= max)
        return (validation ?? new Validation()).AddException(
          new ArgumentOutOfRangeException(paramName, "Value must be under " + max.ToString() + " but was " + testSubject.ToString()));
      else
        return validation;
    }

    public static Validation IsInRange(this Validation validation, long testsubject, long min, long max, string paramName)
    {
      return validation.IsOver(testsubject, min - 1, paramName)
                       .IsUnder(testsubject, max + 1, paramName);
    }

    public static Validation IsNotEqualTo(this Validation validation, IComparable testSubject, IComparable other, string paramName)
    {
      if (testSubject.CompareTo(other) == 0)
        return (validation ?? new Validation()).AddException(
          new ArgumentException("Values can not be equal.", paramName));
      else
        return validation;
    }

    public static Validation IsEqualTo(this Validation validation, IComparable testSubject, IComparable other, string paramName)
    {
      if (testSubject.CompareTo(other) != 0)
        return (validation ?? new Validation()).AddException(
          new ArgumentException("Values must be equal.", paramName));
      else
        return validation;
    }

    public static Validation IsTrue(this Validation validation, bool testSubject, string paramName)
    {
      if (testSubject != true)
        return (validation ?? new Validation()).AddException(
          new ArgumentException("Assertion failed, value was not true: ", paramName));
      else
        return validation;
    }

    public static Validation IsNotNullOrEmpty(this Validation validation, string testSubject, string paramName)
    {
      if (testSubject.IsEmptyOrNull())
        return (validation ?? new Validation()).AddException(
          new ArgumentException("Parameter was null or empty.", paramName));
      else
        return validation;
    }

    public static Validation IsLessThan(this Validation validation, IComparable testSubject, IComparable other, string paramName)
    {
      //TODO: you know what to do here..
      if (testSubject.CompareTo(other) >= 0)
        return (validation ?? new Validation()).AddException(
          new ArgumentOutOfRangeException(paramName, "Value must be less than than the other argument. Someone write a better description for this exception."));
      else
        return validation;
    }

    public static Validation Check(this Validation validation)
    {
      if (validation == null || validation.Exceptions.Count == 0)
        return validation;
       

      StringBuilder message = new StringBuilder("Validation failed for a function. Failures(");
      message.AppendFormat("{0}) are: \n", validation.Exceptions.Count);
      
      foreach (Exception ex in validation.Exceptions)
      {
        message.AppendLine(ex.Message);
        message.AppendLine("--------------");
      }

      throw new ValidationException(message.ToString(), validation.Exceptions[0], validation.Exceptions.Count);
      
    }

  }




  [global::System.Serializable]
  public class ValidationException : Exception
  {
    //
    // For guidelines regarding the creation of new exception types, see
    //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
    // and
    //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
    //
    private int count;
    public int Count { get { return count; } }
    public ValidationException() { }
    public ValidationException(string message) : base(message) { }
    public ValidationException(string message, Exception inner, int exceptionCount) : base(message, inner) { count = exceptionCount; }
    protected ValidationException(
    System.Runtime.Serialization.SerializationInfo info,
    System.Runtime.Serialization.StreamingContext context)
      : base(info, context) { }
  }
}
