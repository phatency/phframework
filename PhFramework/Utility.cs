﻿using System;
using System.CodeDom;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using Microsoft.CSharp;
using Ph.Scripting;
using Ph.SQLite;

namespace Ph
{

  static public class Utility
  {
    public static KeyValuePair<T, U> MakePair<T, U>(T key, U value)
    {
      return new KeyValuePair<T, U>(key, value);
    }

    public static string ComputeMD5FromFile(string fileName)
    {
      using (FileStream file = File.OpenRead(fileName))
      {
        MD5 md5 = new MD5CryptoServiceProvider();
        byte[] retVal = md5.ComputeHash(file);
        return Convert.ToBase64String(retVal);
      }
    }

    public static string ComputeMD5(string value)
    {
      MD5 md5 = new MD5CryptoServiceProvider();
      byte[] data = Encoding.UTF8.GetBytes(value);
      data = md5.ComputeHash(data);
      return Convert.ToBase64String(data);
    }
  }

  public static class Services
  {
    private static ScriptManager scriptManager = null;

    public static ScriptManager ScriptManager
    {
      get
      {
        if (scriptManager == null)
          scriptManager = new ScriptManager();
        return scriptManager;
      }
    }
  }

  public static class Database
  {
    private static SQLiteConnection _cacheDB;

    public static SQLiteStatement Cache
    {
      get { return new SQLiteStatement(_cacheDB); }
    }

    static Database()
    {
      string cacheLocation = new[] { "cache", "cache.db3" }.RelativePath();
      
      _cacheDB = new SQLiteConnection();
      if (!_cacheDB.TryOpen(cacheLocation))
      {
        _cacheDB.Open(cacheLocation, SQLFileOpen.ReadWrite | SQLFileOpen.Create);
        _cacheDB.CreateStatement().ExecuteNonQuery(
      @"CREATE TABLE scripts (
        name TEXT PRIMARY KEY NOT NULL, 
        version TEXT NOT NULL, 
        checksums TEXT NOT NULL,
        assembly TEXT NOT NULL)");
      }
    }
  }



  [global::System.Serializable]
  public class FrameworkException : Exception
  {
    //
    // For guidelines regarding the creation of new exception types, see
    //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
    // and
    //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
    //

    public FrameworkException() { }
    public FrameworkException(string message) : base(message) { }
    public FrameworkException(string message, Exception inner) : base(message, inner) { }
    protected FrameworkException(
    System.Runtime.Serialization.SerializationInfo info,
    System.Runtime.Serialization.StreamingContext context)
      : base(info, context) { }
  }

}
