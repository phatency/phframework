﻿using System;
using System.Collections.Generic;
using System.Text;
using Ph.Extensions.Char;
using System.IO;

namespace Ph
{
  public class SettingNode
  {
    protected string node;

    public SettingNode(string node)
    {
      this.node = node;
    }

    public string Node
    {
      get { return node; }
      set { node = value; }
    }

    public string this[string index]
    {
      get { return Settings.String[node + index]; }
      set { Settings.String[node + index] = value; }
    }

    public int GetInt(string index)
    {
      return Settings.GetInt(node + index);
    }

    public float GetFloat(string index)
    {
      return Settings.GetFloat(node + index);
    }

    public string GetString(string index)
    {
      return Settings.GetString(node + index);
    }

    public void SetInt(string index, int value)
    {
      Settings.SetInt(index, value);
    }

    public void SetFloat(string index, float value)
    {
      Settings.SetFloat(index, value);
    }

    public void SetString(string index, string value)
    {
      Settings.SetString(index, value);
    }


  }

  public class SettingNodeInt : SettingNode
  {

    public SettingNodeInt(string node)
      : base(node)
    {

    }

    public new int this[string index]
    {
      get { return Settings.Int[node + index]; }
      set { Settings.Int[node + index] = value; }
    }
  }



  public class Settings
  {    
    static SortedDictionary<string, string> stringData = new SortedDictionary<string, string>();
    static SortedDictionary<string, int> intData = new SortedDictionary<string, int>();
    static SortedDictionary<string, float> floatData = new SortedDictionary<string, float>();

    static string fileName;
    

    static bool loaded = false;
    static bool _strict = true;
    public bool Strict
    {
      get { return _strict; }
      set { _strict = value; }
    }

    public static bool Loaded
    {
      get { return loaded; }
    }

    public static SettingNode GetNode(string name)
    {
      return new SettingNode(name + '.');
    }

    public static SettingNodeInt GetIntNode(string name)
    {
      return new SettingNodeInt(name + '.');
    }

    public static SortedDictionary<string, int> Int
    {
      get { return intData; }
    }

    public static SortedDictionary<string, string> String
    {
      get { return stringData; }
    }

    public static SortedDictionary<string, float> Float
    {
      get { return floatData; }
    }

    public static int GetInt(string name)
    {
      return intData[name];
    }

    public static void SetInt(string name, int value)
    {
      intData[name] = value;
    }

    public static float GetFloat(string name)
    {
      return floatData[name];
    }

    public static void SetFloat(string name, float value)
    {
      floatData[name] = value;
    }

    public static string GetString(string name)
    {
      return stringData[name];
    }

    public static void SetString(string name, string value)
    {
      stringData[name] = value;
    }


    public static void Load(TextReader reader)
    {
      int line = 1;
      try
      {
      // Behold: below lies probably the most incomphensible piece of code I've ever written. You have been warned.
      string buffer = reader.ReadToEnd();
      StringBuilder curNode = new StringBuilder();

      int offset = 0;
      int a, b, c, d, e;
      b = c = e = 0;

      while (offset < buffer.Length - 3) // minimum amount of characters required for a full line
      {
        a = -1 + offset; d = offset;
        while (buffer[++a] == ' ' || buffer[a] == '\t') ; // Iterate untill non-whitespace char appears
        if (buffer[a] == '<') // <*foo>
        {
          if (buffer[a + 1] == '/') // </foo>
            ++a;                       // iterate over the slash
          b = ++a;                     // let b and a be the first char of the foo
          while (buffer[++b] != '>') ; // iterate b to the terminating >

          if (buffer[a - 1] == '/')    // so if it was a </foo> we'll remove one node from curNode 
            curNode.Remove(curNode.Length - (b - a) - 1, b - a + 1);
          else                         // Else it is a new node which we'll add
            curNode.Append(buffer.Substring(a, b - a)).Append('.');
          d = b;                       // Let d be the terminating >
          while (++d < buffer.Length && buffer[d] != '\r') ; // iterate untill end of line
        }
        else if (buffer[a] != '\r')   // if the line is not empty
        {

          b = a;                      // a and b are both at the first non-whitespace
          while (buffer[++b] != ' ' && buffer[b] != '\t' && buffer[b] != '=') ;
          c = b;
          while (buffer[c] != '=') ++c; // first make sure we get to the equal sign
          while (buffer[c] == '=' || buffer[c] == ' ' || buffer[c] == '\t') ++c; // then skip it and skip whitespace too
          
          d = c;
          while (++d < buffer.Length && buffer[d] != '\r') ; // and d will be the end of line
          e = d;
          while (buffer[--e] == ' ' || buffer[e] == '\t') ; // remove trailing whitespace
          ++e;
          string name = curNode.Length != 0
                          ? curNode.ToString() + buffer.Substring(a, b - a)
                          : buffer.Substring(a, b - a);
          string value = buffer.Substring(c, e - c);

          if (value[0].IsNumeric())
            if (value[value.Length - 1] == 'f') 
              floatData.Add(name, float.Parse(value.Remove(value.Length - 1)));
            else
              intData.Add(name, int.Parse(value));
          else
            stringData.Add(name, value);
        }
        else
        {
          d = a;
        }

        offset = d + 2;
        ++line;
        continue;

      }
      loaded = true;

      }
      catch (Exception ex)
      {
        Log.Error("{0}:{2} {1}", fileName, ex.Message, line);
        throw;
      }
    }

    public static void Load(string fileName)
    {
      Settings.fileName = fileName;
      TextReader reader = null;
      try
      {
        reader = new StreamReader(Settings.fileName);
         Load(reader);
      }
      catch (FileNotFoundException ex)
      {
        Log.Error(ex.Message);
      }
      finally
      {
        if (reader != null)
          reader.Dispose();
      }
    }

    public static void Save(string fileName)
    {
      var settings = new SortedDictionary<string, object>();
      AddToDictionary(settings, stringData);
      AddToDictionary(settings, intData);
      AddToDictionary(settings, floatData);

      var settingStack = new Stack<SortedDictionary<string, object>>();
      try
      {
        using (TextWriter file = new StreamWriter(fileName, false, Encoding.Unicode))
            WriteConfigDictionaryToStream(settings, file, 0);
      }
      catch (Exception ex)
      {
        throw new FrameworkException("Exception occured while saving " + fileName, ex);
      }
      
    }

    public static void Save()
    {
      Save(fileName);
    }


    private static void WriteConfigDictionaryToStream(SortedDictionary<string, object> dict, TextWriter file, int indentCount)
    {
      string indent = new string('\t', indentCount); 
      foreach (KeyValuePair<string, object> pair in dict)
      {
        var subTree = pair.Value as SortedDictionary<string, object>;
        if (subTree != null)
        {
          file.WriteLine("{1}<{0}>", pair.Key, indent);   // <foo>
          WriteConfigDictionaryToStream(subTree, file, indentCount + 1); // Recursion
          file.WriteLine("{1}</{0}>", pair.Key, indent); // </foo>
        }
        else if (pair.Value is string || pair.Value is int)
          file.WriteLine("{2}{0} = {1}", pair.Key, pair.Value, indent);
        else if(pair.Value is float)
          file.WriteLine("{2}{0} = {1}f", pair.Key, pair.Value, indent);
      }
    }

    private static void AddToDictionary<T>(SortedDictionary<string, object> master, SortedDictionary<string,T> source)
    {
      foreach (KeyValuePair<string, T> pair in source)
      {
        var curDict = master;
        string[] nameSplitted = pair.Key.Split('.');
        for (int i = 0; i < nameSplitted.Length; i++)
          if (i == (nameSplitted.Length - 1)) // this is the actual name of the setting
            curDict.Add(nameSplitted[i], pair.Value);
          else
          {
            if (!curDict.ContainsKey(nameSplitted[i])) // we'll create a new sub-dictionary
            {
              var newDict = new SortedDictionary<string, object>();
              curDict.Add(nameSplitted[i], newDict);
              curDict = newDict;
            }
            else // we'll use an existing dictionary
              curDict = curDict[nameSplitted[i]] as SortedDictionary<string, object>;
          }
      }
    }



    public static void Reload()
    {
      Unload();
      Load(fileName);
    }

    public static void Unload()
    {
      loaded = false;
      stringData.Clear();
      intData.Clear();
      floatData.Clear();
    }

    static Settings()
    {
      FileInfo config = new FileInfo("config.cfg");
      if (config.Exists)
        Load(config.FullName);
    }
  }
}
