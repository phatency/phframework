﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Data;

namespace Ph.SQLite
{

  public class SQLiteStatement
  {
    private IntPtr db = IntPtr.Zero;
    private SQLiteConnection connection;
    private IntPtr stmHandle = IntPtr.Zero;

    /// <param name="connection">Database connection to use. Note: it must be open.
    /// </param>
    public SQLiteStatement(SQLiteConnection connection)
    {
      db = connection.Database;
      this.connection = connection;
    }

    public void ExecuteNonQuery(string query)
    {
      Prepare(query);
      Step();
      FinalizeStatement();
    }

    public List<string> ExecuteStringArrayQuery(string query)
    {
      Prepare(query);
      List<string> results = new List<string>();


      int numColumns = sqlite_column_count(stmHandle);

      if (numColumns > 1 && sqlite_step(stmHandle) == SQLResult.Row)
        for (int i = 0; i < numColumns; ++i)
          results.Add(sqlite_column_text(stmHandle, i));
      else
        while (sqlite_step(stmHandle) == SQLResult.Row)
          results.Add(sqlite_column_text(stmHandle, 0));

      FinalizeStatement();
      return results;
    }
    
    public string ExecuteStringQuery(string query, params object[] parameters)
    {
      Debug("SQLITE> Executing {0}", query);

      Prepare(query);
      Bind(parameters);
      string result = null;
      if (sqlite_step(stmHandle) == SQLResult.Row)
        result = sqlite_column_text(stmHandle, 0);

      FinalizeStatement();
      return result;
    }

    public void BindInt(int index, int parameter)
    {
      SQLResult result = sqlite_bind_int(stmHandle, index, parameter);
      CheckResultExpectOk(result);
    }

    public void BindDouble(int index, double parameter)
    {
      SQLResult result = sqlite_bind_double(stmHandle, index, parameter);
      CheckResultExpectOk(result);
    }

    public void BindText(int index, string parameter)
    {
      SQLResult result = sqlite_bind_text(stmHandle, index, parameter, parameter.Length * 2, IntPtr.Zero);
      CheckResultExpectOk(result);
    }
    public void BindNull(int index)
    {
      SQLResult result = sqlite_bind_null(stmHandle, index);
      CheckResultExpectOk(result);
    }
    public void Bind(int index, object parameter)
    {

      if (parameter is int)
        BindInt(index, (int)parameter);
      else if (parameter is double)
        BindDouble(index, (double)parameter);
      else if (parameter is string)
        BindText(index, (string)parameter);
      else if (parameter == null)
        BindNull(index);
    }

    public SQLResult Step()
    {
      Debug("SQLITE> Stepping {0}", stmHandle);
      SQLResult result = sqlite_step(stmHandle);
      if (result < SQLResult.Row) // That only leaves us .Row and .Done
        throw new SQLiteException(db);
      return result;
    }

    public SQLResult TryStep()
    {
      return sqlite_step(stmHandle);
    }

    public void Prepare(string query)
    {
      Debug("SQLITE> Preparing {0} {1}",stmHandle, query);
      SQLResult result = sqlite_prepare(db, query, query.Length * 2, out stmHandle, IntPtr.Zero);
      CheckResultExpectOk(result);
    }

    public SQLResult TryPrepare(string query)
    {
      return sqlite_prepare(db, query, query.Length, out stmHandle, IntPtr.Zero);
    }

    public SQLResult FinalizeStatement()
    {
      Debug("SQLITE> Finalizing {0}", stmHandle);
      SQLResult result = sqlite_finalize(stmHandle);
      CheckResultExpectOk(result);
      return result;
    }

    public DataTable ExecuteQuery(string query)
    {
      Prepare(query);

      DataTable result = Execute();
      FinalizeStatement();
      return result;
    }

    public int ColumnCount
    {
      get { return sqlite_column_count(stmHandle); }
    }

    public string GetColumnName(int index)
    {
      return sqlite_column_origin_name(stmHandle, index);
    }

    public DataTable Execute()
    {
      DataTable dTable = new DataTable();
      int numColumns = ColumnCount;

      sqlite_step(stmHandle);

      for (int i = 0; i < numColumns; i++)
        dTable.Columns.Add(GetColumnName(i), ToNETType(sqlite_column_type(stmHandle, i)));

      do
      {
        object[] row = new object[numColumns];

        for (int i = 0; i < numColumns; i++)
          switch (sqlite_column_type(stmHandle, i))
          {
            case SQLType.Integer:
              row[i] = sqlite_column_int(stmHandle, i);
              break;
            case SQLType.Text:
              row[i] = sqlite_column_text(stmHandle, i);
              break;
            case SQLType.Float:
              row[i] = sqlite_column_double(stmHandle, i);
              break;
            case SQLType.Null:
              row[i] = null;
              break;
            case SQLType.Blob:
              //TODO: support for blobs
              break;

          } 
        dTable.Rows.Add(row);
      } while (sqlite_step(stmHandle) == SQLResult.Row);
      return dTable;
    }

    private Type ToNETType(SQLType SQLType)
    {
       switch (SQLType)
       {
         case SQLType.Integer:
           return typeof (int);
         case SQLType.Text:
           return typeof (string);
         case SQLType.Float:
           return typeof (double);
         //TODO: Add missing types
         default:
           throw new NotImplementedException("SQLiteStatement.ToNETType(" + SQLType + ")");
       }
    }

    public string GetString(int index)
    {
      return sqlite_column_text(stmHandle, index);
    }

    /// <summary>
    /// Called to reset a prepared statement object back to its initial state, ready to be re-executed.
    /// </summary>
    /// <returns>SQLResult.Ok, if the most recent call to Step didn't throw an exception.</returns>
    public SQLResult Reset()
    {
      return sqlite_reset(stmHandle);
    }

    /// <summary>
    /// Contrary to the intuition of many, Reset() does not reset the bindings on a prepared statement. Use this routine to reset all host parameters to NULL.
    /// <remarks>Does the SQLResult have any real value? Should we even marshal it?</remarks>
    /// </summary>
    public SQLResult ClearBindings()
    {
      return sqlite_clear_bindings(stmHandle);
    }

    public void BindText( params string[] parameters)
    {
      for (int i = 1; i <= parameters.Length; i++)
        BindText(i, parameters[i - 1]);
    }

    public void Bind(params object[] parameters)
    {
      for (int i = 1; i <= parameters.Length; i++)
        Bind(i, parameters[i - 1]);
    }

    private void CheckResultExpectOk(SQLResult result)
    {
      if (result != SQLResult.Ok)
        throw new SQLiteException(db);
    }
    [Conditional("SQLITE_DEBUG")]
    private void Debug(string format, params object[] parameters)
    {
      Log.Debug(format, parameters);
    }

    #region sqlite_marshals
    //[DllImport("sqlite3", EntryPoint = "sqlite3_prepare_v2")]
    //static extern SQLResult sqlite_prepare(IntPtr database, string query, int nBytes, out IntPtr stHandle, IntPtr tail);

    [DllImport("sqlite3", EntryPoint = "sqlite3_prepare16_v2", CharSet = CharSet.Unicode)]
    static extern SQLResult sqlite_prepare(IntPtr database, string query, int nBytes, out IntPtr stHandle, IntPtr tail);

    [DllImport("sqlite3", EntryPoint = "sqlite3_step")]
    static extern SQLResult sqlite_step(IntPtr stmHandle);

    [DllImport("sqlite3", EntryPoint = "sqlite3_finalize")]
    static extern SQLResult sqlite_finalize(IntPtr stmHandle);

    [DllImport("sqlite3", EntryPoint = "sqlite3_reset")]
    static extern SQLResult sqlite_reset(IntPtr stmHandle);

    [DllImport("sqlite3", EntryPoint = "sqlite3_clear_bindings")]
    static extern SQLResult sqlite_clear_bindings(IntPtr stmHandle);

    [DllImport("sqlite3", EntryPoint = "sqlite3_column_count")]
    static extern int sqlite_column_count(IntPtr stmHandle);

    [DllImport("sqlite3", EntryPoint = "sqlite3_column_origin_name16", CharSet = CharSet.Unicode)]
    static extern string sqlite_column_origin_name(IntPtr stmHandle, int columnNum);

    [DllImport("sqlite3", EntryPoint = "sqlite3_column_type")]
    static extern SQLType sqlite_column_type(IntPtr stmHandle, int iCol);

    [DllImport("sqlite3", EntryPoint = "sqlite3_column_int")]
    static extern int sqlite_column_int(IntPtr stmHandle, int columnNum);

    [DllImport("sqlite3", EntryPoint = "sqlite3_column_text16", CharSet = CharSet.Unicode)]
    static extern string sqlite_column_text(IntPtr stmHandle, int columnNum);

    [DllImport("sqlite3", EntryPoint = "sqlite3_column_double")]
    static extern double sqlite_column_double(IntPtr stmHandle, int columnNum);

    [DllImport("sqlite3", EntryPoint = "sqlite3_bind_int")]
    static extern SQLResult sqlite_bind_int(IntPtr stmHandle, int index, int value);

    [DllImport("sqlite3", EntryPoint = "sqlite3_bind_double")]
    static extern SQLResult sqlite_bind_double(IntPtr stmHandle, int index, double value);

    [DllImport("sqlite3", EntryPoint = "sqlite3_bind_text16", CharSet = CharSet.Unicode)]
    private static extern SQLResult sqlite_bind_text(IntPtr stmHandle, int index, string value, int nBytes, IntPtr destructor);

    [DllImport("sqlite3", EntryPoint = "sqlite3_bind_null")]
    static extern SQLResult sqlite_bind_null(IntPtr stmHandle, int index);
    // TODO: Bind a busy handler
    #endregion
  }

  public static class SQLiteExtensions
  {

    public static void BindText(this SQLiteStatement st, params string[] parameters)
    {
      for (int i = 1; i <= parameters.Length; i++)
        st.BindText(i, parameters[i-1]);
    }

    public static void Bind(this SQLiteStatement st, params object[] parameters)
    {
      for (int i = 1; i <= parameters.Length; i++)
        st.Bind(i, parameters[i-1]);
    }

    public static void ExecuteNonQuery(this SQLiteStatement st, string query, params object[] parameters)
    {
      Prepare(st, query, parameters);
      st.Step();
      st.FinalizeStatement();
    }

    public static void ExecuteNonQuery(this SQLiteStatement st, string query, params string[] parameters)
    {
      st.Prepare(query);
      BindText(st, parameters);
      st.Step();
      st.FinalizeStatement();
    }

    public static DataTable ExecuteQuery(this SQLiteStatement st, string query, params object[] parameters)
    {
      Prepare(st, query, parameters);

      DataTable result = st.Execute();
      st.FinalizeStatement();
      return result;
    }

    public static void Prepare(this SQLiteStatement st, string query, params object[] parameters)
    {
      st.Prepare(query);
      Bind(st, parameters);
    }
  }

  [Serializable]
  public class SQLiteException : FrameworkException
  {
    //
    // For guidelines regarding the creation of new exception types, see
    //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/cpgenref/html/cpconerrorraisinghandlingguidelines.asp
    // and
    //    http://msdn.microsoft.com/library/default.asp?url=/library/en-us/dncscol/html/csharp07192001.asp
    //
    private string message;

    public new string Message
    {
      get { return message; }
    }

    public SQLResult ErrorCode = SQLResult.Ok;

    internal SQLiteException(IntPtr db)
    {
      ErrorCode = sqlite_errcode(db);
      message = String.Format("SQLiteException: {0}\n{1}", ErrorCode, sqlite_errmsg(db));
    }

    internal SQLiteException(string message)
    {
      this.message = message;
    }

    protected SQLiteException(
    System.Runtime.Serialization.SerializationInfo info,
    System.Runtime.Serialization.StreamingContext context)
      : base(info, context) { }

    [DllImport("sqlite3", EntryPoint = "sqlite3_errmsg16", CharSet = CharSet.Unicode)]
    private static extern string sqlite_errmsg(IntPtr database);

    [DllImport("sqlite3", EntryPoint = "sqlite3_errcode")]
    private static extern SQLResult sqlite_errcode(IntPtr database);
  }
}
