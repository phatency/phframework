﻿using System;
using System.Runtime.InteropServices;

namespace Ph.SQLite
{
  public class SQLiteConnection
  {
    private bool isOpen = false;
    private IntPtr db = IntPtr.Zero;
    private string path = "";


    /// <summary>
    /// Returns a pointer to the active connection.
    /// </summary>
    /// <exception cref="SQLiteException">Thrown when the connection is not open.</exception>
    public IntPtr Database
    {
      get
      {
        if (isOpen)
          return db;
        throw new SQLiteException("SQLiteConnection: database is not open, yet access to it was requested.");
      }
    }

    /// <summary>
    /// Path to the current or last database connection.
    /// </summary>
    public string Path
    {
      get { return path; }
    }

    public bool IsOpen
    {
      get { return isOpen; }
    }

    public SQLiteConnection(string path)
    {
      Open(path);
    }

    public SQLiteConnection(string path, SQLFileOpen flags)
    {
      Open(path, flags);
    }

    public SQLiteConnection() {}


    /// <summary>
    /// Closes the active connection, if it's open.
    /// </summary>
    public void Close()
    {
      if (isOpen)
      {
        SQLResult result = sqlite_close(db);
        if (result == SQLResult.Ok)
          Log.Debug("Closed database connection {0}", path);
        else // Is this even possible?
          throw new SQLiteException(db);
        isOpen = false;
        path = "";
      }
    }

    /// <summary>
    /// Opens a database connection to path, using default flags: SQLFileOpen.ReadWrite
    /// </summary>
    /// <param name="path">path to the database. Use :memory for an in-memory database</param>
    public void Open(string path)
    {
      Open(path, SQLFileOpen.ReadWrite);
    }

    /// <summary>
    /// Opens a database connection.
    /// </summary>
    /// <param name="path">Path to the database. Use ":memory" for an in-memory database.</param>
    /// <param name="flags">File operation flags to open the database with.</param>
    /// <exception cref="SQLiteException">Thrown if no connection can be made.</exception>
    public void Open(string path, SQLFileOpen flags)
    {
      if (isOpen)
        Close();
      SQLResult result = sqlite_open_v2(path, out db, flags, null);
      if (result == SQLResult.Ok)
        ConnectionOpened(path);
      else
        throw new SQLiteException(db);

    }

    public bool TryOpen(string path, SQLFileOpen flags)
    {
      SQLResult result = sqlite_open_v2(path, out db, flags, null);
      if (result == SQLResult.Ok)
      {
        ConnectionOpened(path);
        return true;
      }
      return false;
    }

    public bool TryOpen(string path)
    {
      return TryOpen(path, SQLFileOpen.ReadWrite);
    }

    private void ConnectionOpened(string path)
    {
      this.path = path;
      isOpen = true;
      Log.Debug("Opened database {0}", path);
    }

    public SQLiteStatement CreateStatement()
    {
      if (isOpen)
        return new SQLiteStatement(this);
      throw new SQLiteException("SQLite: Can't create an SQLiteStatement, connection is not open.");
    }

    ~SQLiteConnection()
    {
      Close();
    }

    #region sqlite_marshals
    [DllImport("sqlite3", EntryPoint = "sqlite3_open_v2")]
    static extern SQLResult sqlite_open_v2(string filename, out IntPtr database, SQLFileOpen flags, string vfsModule);

    [DllImport("sqlite3", EntryPoint = "sqlite3_close")]
    static extern SQLResult sqlite_close(IntPtr database);
    #endregion
  }
}
