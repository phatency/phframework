﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ph.SQLite
{

  /// <summary>SQLite result constants </summary>
  ///     <see>http://www.sqlite.org/c3ref/c_abort.html</see>
  public enum SQLResult
  {
    Ok = 0,           /// Successful result

    Error,            /// SQL error or missing database
    Internal,         /// Internal logic error in SQLite
    PermissionDenied, /// Access permission denied
    Abort,            /// Callback routine requested abort
    Busy,             /// The database file is locked 
    Locked,           /// A table in the database is locked
    NoMemory,         /// A malloc() failed
    ReadOnly,         /// Attempt to write a readonly database
    Interrupt,        /// Operation terminated by sqlite3_interrupt()
    IOError,          /// Some kind of disk I/O error occurred
    Corrupt,          /// The database disk image is malformed
    Full = 13,        /// Insertion failed because database is full
    CantOpen = 14,    /// Unable to open the database file.
    Empty = 16,       /// Database is empty
    Schema,           /// The database schema changed
    TooBig,           /// String or BLOB exceeds size limit
    Constraint,       /// Abort due to constraint violation
    Mismatch,         /// Data type mismatch
    Misuse,           /// Library used incorrectly
    NoLFS,            /// Uses OS features not supported on host
    Auth,             /// Authorization denied
    Format,           /// Auxiliary database format error
    Range,            /// 2nd parameter to sqlite3_bind out of range
    NotDatabase = 26, /// File opened that is not a database file
                      /// 
    Row = 100,        /// sqlite_step() has another row ready
    Done = 101        /// sqlite_step() has finished executing
  }

  enum SQLType
  {
    Integer = 1,
    Float,
    Text,
    Blob,
    Null
  }

  enum SQLTextEncoding
  {
    UTF8 = 1,
    UTF16LE,
    UTF16BE,
    UTF16,
    ANY = 5,
    UTF16_ALIGNED = 8
  }

  [Flags]
  public enum SQLFileOpen
  {
    ReadOnly = 0x00000001,
    ReadWrite = 0x00000002,
    Create = 0x00000004,
    NoMutex = 0x00008000,
    FullMutex = 0x00010000
  }

}
