﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Ph
{
  public static class Dir
  {
    static SettingNode dirs;
    static bool initialized = false;
    static string rootDir;


    public static string Relative(string identifier)
    {
      return dirs[identifier];
    }

    public static string Absolute(string identifier)
    {
      return rootDir +  dirs[identifier];
    }

    public static string MakeRelative(string absolutePath)
    {
      return absolutePath.Remove(0, rootDir.Length);
    }

    private static void Init()
    {
      if (initialized)
        throw new Exception(""); //TODO: Proper exception
      dirs = Settings.GetNode("dir");
      initialized = true;
      rootDir = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location) + Path.DirectorySeparatorChar;

    }

    public static string PathCombine(this IEnumerable<string> self)
    {
      string fullPath = "";
      foreach (string pathPart in self)
      {
        fullPath = Path.Combine(fullPath, pathPart);
      }
      return fullPath;
    }

    public static string RelativePath(this IEnumerable<string> self)
    {
      var i = self.GetEnumerator();
      i.MoveNext();
      string fullPath = Relative(i.Current);
      
      while (i.MoveNext())
      {
        fullPath = Path.Combine(fullPath, i.Current);
      }
      return fullPath;
    }

    public static string AbsolutePath(this IEnumerable<string> self)
    {
      var i = self.GetEnumerator();
      i.MoveNext();
      string fullPath = Absolute(i.Current);

      while (i.MoveNext())
      {
        fullPath = Path.Combine(fullPath, i.Current);
      }
      return fullPath;
    }



    internal static bool Initialized
    {
      get { return initialized; }
    }

    public static string Root
    {
      get { return rootDir; }
    }

    static Dir()
    {
      Init();
    }

  }
}
