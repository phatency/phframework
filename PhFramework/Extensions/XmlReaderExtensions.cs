﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace Ph.Extensions
{
  public static class XmlReaderExtensions
  {
    public static bool TryGetAttributeAsBoolean(this XmlReader reader, string name, out bool value)
    {
      string valueStr = reader[name];
      if (valueStr == null)
      {
        value = false;
        return false;
      }
      return bool.TryParse(valueStr, out value);
    }
  }
}
