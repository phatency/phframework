﻿using System;

namespace Ph.Extensions.Int
{
  static class IntExtensions
  {
    public static int Digit(this int value, int digit)
    {
      
      if (digit == 0)
        return value % 10;
      return (int)(Math.Pow(10, -digit) * value) % 10;
    }

    //public static int Digit(this double value, int digit)
    //{
    //  if (digit == 0)
    //    return (int)value % 10;
    //}
  }

}
