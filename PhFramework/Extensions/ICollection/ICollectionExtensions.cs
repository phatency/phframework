﻿using System.Collections.Generic;

namespace Ph.Extensions.ICollection
{
  public static class ICollectionExtensions
  {
    public static bool IsEmpty<T>(this ICollection<T> collection)
    {
      return collection.Count == 0;
    }

    public static bool IsEmpty(this System.Collections.ICollection collection)
    {
      return collection.Count == 0;
    }
  }
}
