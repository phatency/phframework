﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ph.Extensions.Time
{
  public static class DateTimeExtensions
  {
    static public DateTime Rounded(this DateTime t)
    {
      t = t.AddSeconds(0.5);
      DateTime rounded = new DateTime(t.Year, t.Month, t.Day, t.Hour, t.Minute, t.Second);
      return rounded;
    }
  }
}
