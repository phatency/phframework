﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Ph.Extensions
{
  public static class DirectoryInfoExtensions
  {
    public static List<FileInfo> GetFilesRegex(this DirectoryInfo dir, string pattern, SearchOption searchOption)
    {
      var matches = new List<FileInfo>();
      var regex = new Regex(pattern);
      var files = dir.GetFiles("*", searchOption);
      foreach (var file in files)
        if (regex.IsMatch(file.Name))
          matches.Add(file);
      return matches;
    }
  }
}
