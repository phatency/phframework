﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ph.Extensions.Char
{
  public static class CharExtensions
  {
    public static bool IsNumeric(this char val)
    {
      if (val > 47 && val < 58)
        return true;
      else if (val > 42 && val < 47)
        return true;

      return false;
    }
  }
}
