﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ph.Extensions.String
{
  public static class StringExtensions
  {
    public static bool IsEmpty(this string str)
    {
      return str.Length == 0;
    }

    public static bool IsEmptyOrNull(this string str)
    {
      return str == null || str.Length == 0;
    }
  }
}
