﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using Ph.Extensions.String;
using System.IO;
namespace Ph
{
  
  public class Log
  {
    static Logger defLogger = null;
    public enum LogType
    {
        Debug,
        Warning,
        Error,
        LastElement
    }

    class Logger
    {
        bool[] _printSourceInformation = new bool[(int)LogType.LastElement] {true,true,true};
        private const int NumStackFramesToSkip = 3;
        public void Print(LogType type, string format, params object[] parameters)
        {
            DoPrint(type, FormatString(type, format, parameters));
        }

        protected virtual void DoPrint(LogType type, string formattedString)
        {
            Console.WriteLine(formattedString);
        }

        protected string FormatString(LogType type, string format, params object[] parameters)
        {
            string outputString = String.Format(format, parameters);
#if DEBUG
            if (_printSourceInformation[(int)type])
            {
                StackFrame stackFrame = new StackFrame(NumStackFramesToSkip, true);
                string fileName = Path.GetFileName(stackFrame.GetFileName());
                string method = stackFrame.GetMethod().ToString();
                int line = stackFrame.GetFileLineNumber();

                outputString = String.Format("{0} ({1} {2}:{3}", outputString, method, fileName, line);
            }
#endif
            return outputString;
        }
    }

    static public void Warn(string format, params object[] parameters)
    {
      (defLogger ?? CreateLogger()).Print(LogType.Warning, format, parameters);
    }

    static public void Error(string format, params object[] parameters)
    {
      (defLogger ?? CreateLogger()).Print(LogType.Error, format, parameters);
    }

    [Conditional("DEBUG")]
    static public void Debug(string format, params object[] parameters)
    {
      (defLogger ?? CreateLogger()).Print(LogType.Debug, format, parameters);
    }

    static private Logger CreateLogger()
    {
      defLogger = new Logger();
      return defLogger;
    }
  }
}
