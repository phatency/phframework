﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace Ph.Collections
{
  public abstract class Factory<BaseType> where BaseType : class //new()
  {
    public abstract BaseType Add(string name, Type type);

    public virtual BaseType Add(string name, string typeLocation, Assembly asm)
    {
      Type t = asm.GetType(typeLocation);
      if (t == null)
      {
        Log.Error("Type {0} could not be found in the assembly {1}. It will not be available.", typeLocation, asm.FullName);
        //throw new Exception(); // TODO: guess
        return null;
      }
        
      return Add(name, t);
    }

    public abstract BaseType Create(string name);
    public abstract void Clear();
  }

  public class SingleFactory<BaseType> : Factory<BaseType>, IEnumerable<KeyValuePair<string, BaseType>> where BaseType  : class
  {
    protected OverwritingDictionary<string, BaseType> instances = new OverwritingDictionary<string, BaseType>();

    public override BaseType Add(string name, Type type)
    {
      BaseType instance = (BaseType)Activator.CreateInstance(type);
      if (instance == null)
        throw new Exception(); //TODO: EXCEPTION
      instances.Add(name, instance);
      return instance;
    }

    public override BaseType Create(string name)
    {
      return instances[name];
    }

    public bool TryCreate(string name, out BaseType value)
    {
      return instances.TryGetValue(name, out value);
    }

    public override void Clear()
    {
      instances.Clear();
    }

    public IEnumerator<KeyValuePair<string, BaseType>> GetEnumerator()
    {
      return instances.GetEnumerator();
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return instances.GetEnumerator();
    }
  }

  public class DynamicFactory<BaseType> 
    : Factory<BaseType>, 
      IEnumerable<string> 
      where BaseType : class
  {
    protected OverwritingDictionary<string, Type> types = new OverwritingDictionary<string, Type>();
    public DynamicFactory()
    { }

    public override BaseType Add(string name, Type type)
    {
      types.Add(name, type);
      return null;
    }

    public override BaseType Create(string name)
    {
      return (BaseType)Activator.CreateInstance(types[name]);
    }

    public override void Clear()
    {
      types.Clear();
    }

    public IEnumerator<string> GetEnumerator()
    {
      return types.Keys.GetEnumerator();
    }

    System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
    {
      return types.GetEnumerator();
    }
  }

  public class CloneFactory<BaseType> : SingleFactory<BaseType> where BaseType : class, ICloneable  //new()
  {
    public CloneFactory()
    { }

    public override BaseType Create(string name)
    {
      return (BaseType)instances[name].Clone();
    }

    public BaseType GetOriginal(string name)
    {
      return instances[name];
    }
  }
}
