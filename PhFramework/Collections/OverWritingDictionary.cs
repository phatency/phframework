﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Ph.Collections
{

  /// <summary>
  /// OverwritingDictionary is a simple dictionary, which doesn't throw an exception on duplicate key additions, but silently replaces them.
  /// </summary>
  /// <typeparam name="TKey">KeyType</typeparam>
  /// <typeparam name="TValue">ValueType</typeparam>
  public class OverwritingDictionary<TKey, TValue> : Dictionary<TKey, TValue>
  {
    /// <summary>
    /// Adds an item to the dictionary, overwriting the existing one if it exists.
    /// </summary>
    /// <param name="key">TKey key</param>
    /// <param name="value">TValue value</param>
    /// <returns>true if value was overwritten, false otherwise.</returns>
    public new bool Add(TKey key, TValue value)
    {
      if (base.ContainsKey(key))
      {
        base[key] = value;
        return true;
      }
      base.Add(key, value);
      return false;
    }
  }
}
